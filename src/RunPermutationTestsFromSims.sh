#!/bin/bash
numSims=500
effectSizesT=(0.2 0.5 0.8);
effectSizesB=(0.1 0.3 0.5);

ipwConfigFile=$1;
pythonScriptDirectory="/home/arafferty/banditAlgPermTest/src/louie_experiments"


# binary, non-zero es - 9 sims
cd /disk2/banditSimulations/repeatSimsPriorDifferencesTestIPW

arrayLength=${#effectSizesB[@]};
for ((i=0; i<$arrayLength; i++)); do
    curEffectSize=${effectSizesB[$i]}
    directoryName="bbHighArms"$curEffectSize;
    echo $directoryName
    mkdir -p $directoryName
    python3 $pythonScriptDirectory/run_effect_size_simulations_beta.py $curEffectSize $numSims $directoryName "Thompson" "armsHigh" $ipwConfigFile 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
    echo $!
done

arrayLength=${#effectSizesB[@]};
for ((i=0; i<$arrayLength; i++)); do
    curEffectSize=${effectSizesB[$i]}
    directoryName="bbLowArms"$curEffectSize;
    echo $directoryName
    mkdir -p $directoryName
    python3 $pythonScriptDirectory/run_effect_size_simulations_beta.py $curEffectSize $numSims $directoryName "Thompson" "armsLow" $ipwConfigFile 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
    echo $!
done

arrayLength=${#effectSizesB[@]};
for ((i=0; i<$arrayLength; i++)); do
    curEffectSize=${effectSizesB[$i]}
    directoryName="bb"$curEffectSize;
    echo $directoryName
    mkdir -p $directoryName
    python3 $pythonScriptDirectory/run_effect_size_simulations_beta.py $curEffectSize $numSims $directoryName $ipwConfigFile 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
    echo $!
done


# binary, zero es
cd /disk2/banditSimulations/sameArmSimsConsolidated

variancesT=(5.0 2.0 1.25); # Note: we're not crossing nsT and variancesT because variancesT is actual variance with ES with that num steps
nsT=(394 64 26);
nsB=(785 88 32);
arrayLength=${#nsB[@]};
for ((i=0; i<$arrayLength; i++)); do
    curN=${nsB[$i]}
    directoryName="bbEqualMeansEqualPrior"$curN;
    echo $directoryName
    mkdir -p $directoryName
    python3 $pythonScriptDirectory/run_effect_size_simulations_beta.py 0.5,0.5 $numSims $directoryName "Thompson" "armsEqual" $curN $ipwConfigFile 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
    echo $!
done

arrayLength=${#nsB[@]};
for ((i=0; i<$arrayLength; i++)); do
    curN=${nsB[$i]}
    directoryName="bbEqualMeansArmsHigh"$curN;
    echo $directoryName
    mkdir -p $directoryName
    python3 $pythonScriptDirectory/run_effect_size_simulations_beta.py 0.5,0.5 $numSims $directoryName "Thompson" "armsHigh" $curN $ipwConfigFile 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
    echo $!
done

arrayLength=${#nsB[@]};
for ((i=0; i<$arrayLength; i++)); do
    curN=${nsB[$i]}
    directoryName="bbEqualMeansArmsLow"$curN;
    echo $directoryName
    mkdir -p $directoryName
    python3 $pythonScriptDirectory/run_effect_size_simulations_beta.py 0.5,0.5 $numSims $directoryName "Thompson" "armsLow" $curN $ipwConfigFile 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
    echo $!
done


wait # only start 18 due to other stuff running

# normal, non-zero es and moving prior - 9 sims
cd /disk2/banditSimulations/normalPriorChanges24April2018WithProbs
muValues=(-1 0 1);
arrayLength=${#effectSizesT[@]};
for ((i=0; i<$arrayLength; i++)); do
    for ((j=0; j<${#muValues[@]}; j++)); do
        curEffectSize=${effectSizesT[$i]}
        curMu=${muValues[$j]}
        directoryName="ngArmsMu"$curMu"ES"$curEffectSize;
        echo $directoryName
        mkdir -p $directoryName
        python3 $pythonScriptDirectory/run_effect_size_simulations.py 0.5,$curMu -0.5 $curEffectSize $numSims $directoryName $ipwConfigFile 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
        echo $!
    done
done



# normal, zero es and moving prior - 9 sims
cd /disk2/banditSimulations/normalZeroESPriorChanges24April2018WithProbs

arrayLength=${#variancesT[@]};
for ((i=0; i<$arrayLength; i++)); do
    curVariance=${variancesT[$i]}
    curN=${nsT[$i]}
    directoryName="ngEqualMeansEqualPrior"$curVariance;
    echo $directoryName
    mkdir -p $directoryName
    python3 $pythonScriptDirectory/run_effect_size_simulations.py 0.0,0.0 0.0 $curVariance $numSims $directoryName "Thompson" $curN $ipwConfigFile 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
    echo $!
done


arrayLength=${#variancesT[@]};
for ((i=0; i<$arrayLength; i++)); do
    curVariance=${variancesT[$i]}
    curN=${nsT[$i]}
    directoryName="ngEqualMeansArmsHigh"$curVariance;
    echo $directoryName
    mkdir -p  $directoryName
    python3 $pythonScriptDirectory/run_effect_size_simulations.py 0.0,-1.0 0.0 $curVariance $numSims $directoryName "Thompson" $curN $ipwConfigFile 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
    echo $!
done

arrayLength=${#variancesT[@]};
for ((i=0; i<$arrayLength; i++)); do
    curVariance=${variancesT[$i]}
    curN=${nsT[$i]}
    directoryName="ngEqualMeansArmsLow"$curVariance;
    echo $directoryName
    mkdir -p $directoryName
    python3 $pythonScriptDirectory/run_effect_size_simulations.py 0.0,1.0 0.0 $curVariance $numSims $directoryName "Thompson" $curN $ipwConfigFile 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
    echo $!
done