import json
import os.path
import sys
from pathlib import Path
import argparse
from collections import defaultdict

def createASSISTmentConfigs():

    # ========== parse arguments ==========

    arg_parser = argparse.ArgumentParser(description='''Creates ASSISTments configs''')

    arg_parser.add_argument('--template_file', help='''Template to use as a starting point for configs''')

    # required arguments

    arg_parser.add_argument('--config_out_directory', help='''Directory where configs will be written''')

    arg_parser.add_argument(
        '--simulation_subdir',
        help='''
            The name of the folder inside /disk2/contextualBanditSimsNoEffect/ExperimentData/complete-or-ProblemCount/Outcomes-or-Parameters
            in which simulation data will be stored. Please note that this argument does not get auto-generated using other
            arguments like lambdaNumber. The reason for this design choice is that it's more and more difficult to auto-generate
            reasonable names for subdir once we haZve more than one argument (e.g., lambdaNumber).
            ''',
        type=str
    )

    arg_parser.add_argument('--lambdaNumber', type=int)

    # optional arguments

    arg_parser.add_argument(
        '--assistments_path',
        help='''The path to the ASSISTments data file''',
        default="/disk2/contextualBanditSimsNoEffect/ExperimentData/ASSISTmentsData.csv"
    )

    arg_parser.add_argument('--online', type=int, default=1)
    arg_parser.add_argument('--dow_scheduler_name', type=str, default=None)

    args = arg_parser.parse_args()

    if args.online == 1:
        assert args.dow_scheduler_name is None, "Data over-weighting does not work with the online version."

    assistments_data_path = args.assistments_path #"/Users/louisye/Desktop/ASSISTments Data (complete) - Raw Data.csv"
    config_file_directory = args.config_out_directory#"/Users/louisye/Desktop/ASSISTmentsConfigFolder/" + completeOrProblemCount + fileName

    # =====================================

    # ========== obtain a list of problem set numbers ==========

    # We will loop through them when creating new configs from the template config.

    # Gets all the possible problem sets out of the ASSISTmentsData.csv
    # Also creating a dictionary that stores amount of numStudents for each problem set
    # ACTUAL_STUDENT_NUM (false): use 1000 as our numStudent
    # ACTUAL_STUDENT_NUM (true): the acutal number of student from ASSISTmentsData.csv ie use the numStudent_in_problem_set dictionary

    ACTUAL_STUDENT_NUM = True

    problem_set_numbers = []
    numStudent_in_problem_set = defaultdict(int)
    with open(assistments_data_path) as dataset_ASSISTment:
        for row in dataset_ASSISTment:
            current_problem_set_number = row.split(",") [0]
            if current_problem_set_number not in problem_set_numbers:
                problem_set_numbers.append(current_problem_set_number)
            if row.split(",")[4] == 'TRUE':
                numStudent_in_problem_set[current_problem_set_number] += 1

    #removing the first and last element of problemSet because they aren't problem sets
    problem_set_numbers = problem_set_numbers[1:len(problem_set_numbers)-1]

    # ==========================================================

    # ========== obtain information about the template config ==========

    # These two variables are only useful if args.template_file is None (second case below).
    # They act as dummies because we can't find the corresponding values in the template (because there's no template!).

    string_to_replace = "##PROBLEMSETNUMBER##"

    # It turns out that this is not used by any code.
    # lambda_string_to_replace = "##LAMBDAVAL##"

    if args.template_file is not None:
        # config with defaults
        with open(args.template_file) as jsonFile:
            config_dictionary = json.load(jsonFile)
            # print(config_dictionary["lambda"])

        config_dictionary['lambda'] = args.lambdaNumber
        config_dictionary['lambdaString'] = str(args.lambdaNumber)

        completeOrProblemCount = config_dictionary["rewardScheme"]
        simTypeLower = config_dictionary["simType"]
        if simTypeLower == "parameters":
            simType = "Params"
        elif simTypeLower == "outcomes":
            simType = "Outcomes"
        else:
            print("Error: unknown simType:", simTypeLower)
            sys.exit(0)
    else:
        completeOrProblemCount = "complete"  # "ProblemCount" or "complete"
        simType = "Params"  # "Outcomes" #"Outcomes" or "Params"
        simTypeLower = "parameters"  # "outcomes" #"outcomes" or "parameters"
        config_dictionary = {
          "simType": simTypeLower,
          "numStudents": 1000,
          "numTrials": 1000,
          "rewardFile": "/disk2/contextualBanditSimsNoEffect/ExperimentData/"+ completeOrProblemCount + "/Outcomes/"+"lambda_"+args.lambdaNumber+"/ASSISTments_" + string_to_replace + "_C1_N1k/CompareBanditRewardFile_NNUMSTUDENTS_TNUMTRIALS" + "lambda" + args.lambdaNumber,
          "outfilePrefix": "/disk2/contextualBanditSimsNoEffect/ExperimentData/"+ completeOrProblemCount + "/Outcomes/"+"lambda_"+args.lambdaNumber+"/ASSISTments_" + string_to_replace + "_C1_N1k/CompareBanditActionsOut_TYPE_NNUMSTUDENTS_TNUMTRIALS"+ "lambda" + args.lambdaNumber,
          "conditionsToActionsFile": "/disk2/contextualBanditSimsNoEffect/ExperimentData/"+ completeOrProblemCount + "/Outcomes/"+"lambda_"+args.lambdaNumber+"/ASSISTments_"+ string_to_replace + "_C1_N1k/CompareBanditConditionsToActions_NNUMSTUDENTS"+ "lambda" + args.lambdaNumber,
          "source": "/disk2/contextualBanditSimsNoEffect/ExperimentData/ASSISTmentsData.csv",
          "rewardScheme": completeOrProblemCount,
          "contextHeaders": ["Prior Percent Correct"],
          "qcutBehavior": {"Prior Percent Correct":  4},
          "replacement": True,
          "lambda": args.lambdaNumber,
          "lambdaString": str(args.lambdaNumber)
        } # lambdaString is used for labeling the problem set

    # lambda_num = config_dictionary.get("lambdaString", config_dictionary.get("lambda", 1))

    # ==================================================================

    # ========== generate new configs from template config ===========

    keys_with_string_to_replace = ["rewardFile", "outfilePrefix", "conditionsToActionsFile"]

    for problem_set in problem_set_numbers:  # loop through the experimentID of every problem set

        # config_dictionary_copy will store the changes and eventually get saved as a new config
        config_dictionary_copy = config_dictionary.copy()

        using_template_file = args.template_file is not None
        # In directory names in configs, simType "Params" corresponds to "Parameters" and simType "Outcomes" corresponds to "Outcomes".
        simTypeInDirName = 'Parameters' if simType == 'Params' else 'Outcomes'

        for key in keys_with_string_to_replace:
            if using_template_file:
                config_dictionary_copy[key] = config_dictionary_copy[key].replace(
                    str(config_dictionary_copy["experimentId"]), str(problem_set)
                ).replace(
                    simTypeInDirName, f'{simTypeInDirName}/{args.simulation_subdir}'
                )
            else:
                config_dictionary_copy[key] = config_dictionary_copy[key].replace(
                    string_to_replace, problem_set
                )

        config_dictionary_copy["experimentId"] = int(problem_set)

        if ACTUAL_STUDENT_NUM:
            config_dictionary_copy["numStudents"] = numStudent_in_problem_set[problem_set]

        if args.online == 0:
            config_dictionary_copy["online"] = 0

        if args.dow_scheduler_name is not None:
            if args.dow_scheduler_name == "pyramidal":
                config_dictionary_copy["dow_scheduler_name"] = "pyramidal"
            else:
                raise ValueError(f"{args.dow_scheduler_name} is not a valid dow scheduler.")

        json_data = json.dumps(config_dictionary_copy, indent=4)

        # Writing to sample.json
        file_name = "ASSISTments" + simType + "Config" + problem_set + ".json"
        config_file_path = os.path.join(config_file_directory, completeOrProblemCount, file_name)
        cur_directory = os.path.dirname(config_file_path)
        Path(cur_directory).mkdir(parents=True, exist_ok=True)

        with open(config_file_path, "w") as outfile:
            outfile.write(json_data)


if __name__ == "__main__":
    createASSISTmentConfigs()
