'''
Created on Jul 19, 2016

Usage:
make_summary_line_plots.py <in_file>
where in_file is the name of the file containing the data (output
from driver_gaussian_rewards_two_bandits, based on one of the graphs).


make_summary_line_plots.py <in_file_prefix> <comma_sep_correlations>
where in_file_prefix is the path and beginning of file name for the regrets,
and comma_sep_correlations contains all correlations we'd like to graph.
E.g.
graphsSteps120Switch60/average-true-t60cor -1,-0.5,0,0.5,1



@author: arafferty
'''
import numpy as np
import sys
import matplotlib.pyplot as plt
import csv

def read_temporal_summary_file(filename):
    '''
    Reads in the data from filename and returns a dictionary
    with keys that are strings set by the first column in each row, and
    values that are the regrets from the remaining columns of each row.
    '''
    data_array = {}
    with open(filename, newline='') as csvfile:
        filereader = csv.reader(csvfile, delimiter=',')
        for line in filereader:
            cur_regrets = [float(line[i]) for i in range(1, len(line))]
            data_array[line[0]] = np.array(cur_regrets)
        
    
    return data_array

def graph_data(data_array, step_to_start_plotting, row_names_to_plot, filename=None):
    '''
    This method extract out the data to be graphed and then save the graph.
    '''
    data1, data2, legendlabels = get_data(data_array, step_to_start_plotting, row_names_to_plot)
    make_line_plot(data1, data2, step_to_start_plotting, legendlabels, filename)
    
def get_data(data_array, step_to_start_plotting, row_names_to_plot):
    data1 = []
    data2 = []

    legendlabels = []
    for alg in data_array:
        alg_name = alg[:alg.rindex('-')]
        if alg_name in row_names_to_plot:
            legendlabels.append(alg_name) # clip the number of steps off the end
#             if include_regret_prior_to_start:
            data1.append(data_array[alg][step_to_start_plotting:])
#             else:
            # Need to subtract off accumulated regret prior to step_to_start_plotting
            data_to_plot = data_array[alg][(step_to_start_plotting-1):] - data_array[alg][step_to_start_plotting - 1]
            data2.append(data_to_plot)
    return data1, data2, legendlabels


def make_line_plot(data1, data2, xtickstart, legendlabels, title=None, filename=None):
    '''
    This method should make a line plot with the xaxis labels starting a xtickstart and
    legendlabes for each item. Saves to the given filename.
    '''
    dpi = 160.0
    fig_width  = 2080 / dpi
    fig_height = 1024 / dpi
#     fig = plt.figure(figsize=(fig_width, fig_height), dpi=dpi)
#     ax = fig.add_axes([0,0,0,0])
    fig, (ax1, ax2) = plt.subplots(1,2,figsize=(fig_width, fig_height), dpi=dpi)
    ax1.plot(np.array(data1).T)
    ax1.set_ylabel('Total regret')
    ax1.set_xlabel('Timestep')
    ax1.set_title(title)
    ax2.plot(np.array(data2).T)

    plt.subplots_adjust(bottom=.25, left=.07, right=.8, top=.90, hspace=.2)
    ax2.legend(legendlabels,fontsize=10,loc='center left', bbox_to_anchor=(1, 0.5))

    if filename == None:
        plt.show()
    else:
        plt.savefig(filename)
        
def plot_all_correlations(prefix, correlations, suffix, switch_point, names_to_plot, filename = None):
    dpi = 160.0
    fig_width  = 2080 / dpi
    fig_height = 1024 / dpi
#     fig = plt.figure(figsize=(fig_width, fig_height), dpi=dpi)
#     ax = fig.add_axes([0,0,0,0])
    fig, axarray = plt.subplots(len(correlations),2,figsize=(fig_width, fig_height), dpi=dpi)
    font = {'family' : 'serif','size'   : 10}
    plt.rc('font', **font)
    for i in range(len(correlations)):
        correlation = correlations[i]
        data_array = read_temporal_summary_file(prefix + str(correlation) + suffix)
        data1, data2, legendlabels = get_data(data_array, switch_point, names_to_plot)
        axarray[i,0].plot(np.array(data1).T)
        axarray[i,0].set_ylim( (0, 18) )
        axarray[i,0].set_yticks([0,5,10,15])#np.arange(0,3,18))

        axarray[i,0].set_ylabel('Total regret',fontsize=10)
        axarray[i,1].plot(np.array(data2).T)
        
        axarray[i,1].set_ylim( (0, 10) )
        
        axarray[i,0].set_title("Correlation: " + correlation,fontsize=10)
        axarray[i,1].set_title("Correlation: " + correlation,fontsize=10)



    axarray[i,0].set_xlabel('Timestep')
    axarray[i,1].set_xlabel('Timestep')

    axarray[0,0].set_title("Regret by step" + "\n" + axarray[0,0].get_title(),fontsize=10)
    axarray[0,1].set_title("Regret by step, excluding regret before switch"+ "\n" + axarray[0,1].get_title(),fontsize=10)

    axarray[0,1].legend(legendlabels,fontsize=10,loc='center left', bbox_to_anchor=(1, 0.5))
    for row in range(len(correlations) - 1):
        plt.setp([a.get_xticklabels() for a in axarray[row, :]], visible=False)
    plt.subplots_adjust(right=.84,wspace=.1, hspace=.25)
        
    if filename == None:
        plt.show()
    else:
        plt.savefig(filename)
    
    
    
def main():
    if len(sys.argv) == 2:
        table_file = sys.argv[1]

        data_array = read_temporal_summary_file(table_file)
        print(data_array.keys())
        switch_point = 60
        names_to_plot = ['epsilon-true', 'hist-ucb1-true', 
                         'thompson-true', 'random-true', 'rand-ucb1-true', 'rand-thompson-true']
    #     names_to_plot = ['rel-thompson-immediate', 'epsilon-immediate', 'hist-ucb1-immediate', 
    #                      'obl-rand-thompson-immediate', 'var-hist-ucb1-immediate', 'ucb1-immediate', 
    #                      'thompson-immediate', 'random-immediate', 'rand-ucb1-immediate', 'rand-thompson-immediate'] 
    #     names_to_plot = ['rel-thompson-true', 'epsilon-true', 'hist-ucb1-true', 
    #                      'obl-rand-thompson-true', 'var-hist-ucb1-true', 'ucb1-true', 
    #                      'thompson-true', 'random-true', 'rand-ucb1-true', 'rand-thompson-true']
        graph_data(data_array, switch_point, names_to_plot, False)
    else:
        prefix = sys.argv[1]
        correlations = sys.argv[2].split(",") # -1,-0.5,-0.25,0,0.25,0.5,0.75,1
        graph_out_file = sys.argv[3]
        suffix = ".csv"
        switch_point = 60
        names_to_plot = ['epsilon-true', 'hist-ucb1-true', 
                         'thompson-true', 'random-true', 'rand-ucb1-true', 'rand-thompson-true']
        plot_all_correlations(prefix, correlations, suffix, switch_point, names_to_plot, graph_out_file)   

if __name__ == "__main__":
    main()