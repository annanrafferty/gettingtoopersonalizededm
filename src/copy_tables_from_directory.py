'''
Created on Nov 14, 2016

Takes two directories as command line arguments. The first
directory is a loction where a bunch of simulations have been run
(in separate directories). This extracts the table files to the 
smaller versions (with just summary information) and copies
into the summary directory with an informative name.

@author: arafferty
'''

from os import listdir
from os.path import isdir, join, isfile
import sys

num_lines = 20


def main():
    copyFromDirectory = sys.argv[1];
    copyToDirectory = sys.argv[2];
    
    # Need to go one level deep in copyFromDirectory
    simDirectories = [d for d in listdir(copyFromDirectory) if isdir(join(copyFromDirectory, d))]
    for directory in simDirectories:
        # find the file that contains table
        simFiles = [f for f in listdir(join(copyFromDirectory, directory)) if isfile(join(join(copyFromDirectory, directory), f))]
        for file in simFiles:
            if "table" in file and "compiled" not in file:
                outFile = join(copyToDirectory, directory) + "compiled.txt"
                with open(join(join(copyFromDirectory, directory), file)) as f, open(outFile, 'w') as w:
                    count = 0
                    for line in f:
                        w.write(line)
                        count += 1
                        if count > num_lines:
                            break;

if __name__ == "__main__":
    main();
                    


