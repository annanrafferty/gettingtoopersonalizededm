#!/bin/bash
numSims=500
effectSizesT=(0.2 0.5 0.8);
effectSizesB=(0.1 0.3 0.5);

arrayLength=${#effectSizesT[@]};
for ((i=0; i<$arrayLength; i++)); do
    curEffectSize=${effectSizesT[$i]}
    directoryName="ngUniformSwitchNonSig"$curEffectSize;
    echo $directoryName
    mkdir $directoryName
    python3 ~/banditalgorithms/src/louie_experiments/run_switch_to_best_simulations.py 0.5 -0.5 $curEffectSize $numSims $directoryName "uniform" unusedArg7 unusedArg8 switchIfNonSig:true 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
    echo $!
done
for ((i=0; i<$arrayLength; i++)); do
    curEffectSize=${effectSizesT[$i]}
    directoryName="ngUniformSwitchSig"$curEffectSize;
    echo $directoryName
    mkdir $directoryName
    python3 ~/banditalgorithms/src/louie_experiments/run_switch_to_best_simulations.py 0.5 -0.5 $curEffectSize $numSims $directoryName "uniform" unusedArg7 unusedArg8 switchIfNonSig:false 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
    echo $!
done
arrayLength=${#effectSizesT[@]};
for ((i=0; i<$arrayLength; i++)); do
    curEffectSize=${effectSizesT[$i]}
    directoryName="ng"$curEffectSize;
    echo $directoryName
    mkdir $directoryName
    python3 ~/banditalgorithms/src/louie_experiments/run_switch_to_best_simulations.py 0.5 -0.5 $curEffectSize $numSims $directoryName 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
    echo $!
done



arrayLength=${#effectSizesB[@]};
for ((i=0; i<$arrayLength; i++)); do
    curEffectSize=${effectSizesB[$i]}
    directoryName="bbUniformSwitchNonSig"$curEffectSize;
    echo $directoryName
    mkdir $directoryName
    python3 ~/banditalgorithms/src/louie_experiments/run_switch_to_best_simulations.py $curEffectSize $numSims $directoryName "uniform" unusedArg5 unusedArg6 unusedArg7 unusedArg8 switchIfNonSig:true binary 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
    echo $!
done

arrayLength=${#effectSizesB[@]};
for ((i=0; i<$arrayLength; i++)); do
    curEffectSize=${effectSizesB[$i]}
    directoryName="bbUniformSwitchSig"$curEffectSize;
    echo $directoryName
    mkdir $directoryName
    python3 ~/banditalgorithms/src/louie_experiments/run_switch_to_best_simulations.py $curEffectSize $numSims $directoryName "uniform" unusedArg5 unusedArg6 unusedArg7 unusedArg8 switchIfNonSig:false binary 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
    echo $!
done

arrayLength=${#effectSizesB[@]};
for ((i=0; i<$arrayLength; i++)); do
    curEffectSize=${effectSizesB[$i]}
    directoryName="bb"$curEffectSize;
    echo $directoryName
    mkdir $directoryName
    python3 ~/banditalgorithms/src/louie_experiments/run_switch_to_best_simulations.py $curEffectSize $numSims $directoryName Thompson binary 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
    echo $!
done

