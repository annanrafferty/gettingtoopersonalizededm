This directory contains scripts for analyzing simulation data
stored in disk2. The goal is to first process the simulation data
into the data for plotting and then save the data for plotting. Later on,
we can plot directly using the data for plotting rather re-processing
the simulation data each time we start jupyter notebooks.

Please ask Zhihan if you have any questions.

