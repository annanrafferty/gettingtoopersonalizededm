from functools import partial
import glob
import time
import os
import pandas as pd
from multiprocessing import Pool
import argparse
import numpy as np

MAX_WORKERS = 8  # chosen heuristically; more doesn't help
FIRST_NUM_STUDENTS = 250  # for swarm plot of proportion of optimal actions
read_csv = partial(pd.read_csv, header=1)

class DFReader:

    def __init__(self, directory):

        print(directory)

        start = time.perf_counter()

        self.df_contextual_list = []
        task_list = glob.glob(os.path.join(directory, 'CompareBanditActionsOut_ModContextual*.csv'))
        with Pool(processes=MAX_WORKERS) as pool:
            self.df_contextual_list = pool.map(read_csv, task_list)

        num_contextual_csv_loaded = len(task_list)

        self.df_noncontextual_list = []
        task_list = glob.glob(os.path.join(directory, 'CompareBanditActionsOut_NonContextual*.csv'))
        with Pool(processes=MAX_WORKERS) as pool:
            self.df_noncontextual_list = pool.map(read_csv, task_list)

        num_noncontextual_csv_loaded = len(task_list)

        end = time.perf_counter()

        print(f'[PROGRESS] Loading {num_contextual_csv_loaded} contextual CSVs and {num_noncontextual_csv_loaded} non-contextual CSVs took {round(end - start, 2)} seconds.')

    def calc_df(self, calc_func, num_students=None, generate_example_file=False, contextual_only=False,
                noncontextual_only=False):

        if contextual_only and noncontextual_only:
            print("[ERROR] Only one of contextual_only and noncontextual_only can be true.")
            return

        if generate_example_file:
            one_unprocessed_df = self.df_contextual_list[0]
            return one_unprocessed_df

        # ========== apply calc_func to each contextual CSV ==========

        with Pool(processes=MAX_WORKERS) as pool:
            df_contextual_list = pool.map(
                calc_func,
                self.df_contextual_list if num_students is None else self.df_contextual_list[:num_students]
            )

        df_contextual = pd.concat(df_contextual_list)
        if contextual_only:
            return df_contextual

        with Pool(processes=MAX_WORKERS) as pool:
            df_noncontextual_list = pool.map(
                calc_func,
                self.df_noncontextual_list if num_students is None else self.df_noncontextual_list[:num_students]
            )

        df_noncontextual = pd.concat(df_noncontextual_list)
        if noncontextual_only:
            return df_noncontextual

        # ========== if contextual_only is False and noncontextual_only is False ==========

        df_contextual["Type"] = 'Contextual'
        df_noncontextual["Type"] = 'Non Contextual'

        df = pd.concat([df_contextual, df_noncontextual])

        return df

def calc_total_reward_direct(df):
    df = df[['contextualVariable0', 'ObservedRewardofAction']]
    df = df.groupby(['contextualVariable0'])['ObservedRewardofAction'].sum()
    df = df.to_frame('Total Reward')
    return df

def calc_POA(df, first_num_students):
    mean = df[:first_num_students]['MatchesOptimalExpectedAction'].mean()
    return pd.DataFrame([[mean]], columns=['prop_optimal_action'])  # just a single column!

def calc_POA_over_time(df):
    df = pd.DataFrame(df['MatchesOptimalExpectedAction'].expanding().mean())
    df.rename(columns = {'MatchesOptimalExpectedAction':'PropOptimalAction'}, inplace = True)
    df['num_students_seen'] = df.index
    return df.iloc[49::50, :]  # ignores first 50 students due to high variance for graphing; after that retain POA per 50 students

if __name__ == "__main__":

    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--simulation_data_dir')
    args = arg_parser.parse_args()

    directory = args.simulation_data_dir
    results_dir = os.path.join(directory, 'results')
    os.makedirs(results_dir, exist_ok=True)

    # Read in all CSVs.
    reader = DFReader(directory)

    # total_reward_df contains the total reward
    # for each trial (1000 trials in total)
    # for each value of the 0th contextual variable (2 values in total: 0 and 1)
    # for each bandit type (2 types in total: contextual and non-contextual).
    # Therefore, it should contains 4000 rows.
    # To double-check this, use .shape property on the df after loading.

    total_reward_df = reader.calc_df(calc_total_reward_direct)
    total_reward_df.to_hdf(os.path.join(results_dir, 'total_reward_df.h5'), key='df', mode='w')

    # For swarm plot of POA

    POA_df = reader.calc_df(partial(calc_POA, first_num_students=FIRST_NUM_STUDENTS))
    POA_df.to_hdf(os.path.join(results_dir, 'POA_df.h5'), key='df', mode='w')

    # For plotting POA over time

    ## For mean and SD

    contextual_POA_over_time_df = reader.calc_df(calc_POA_over_time, contextual_only=True)
    contextual_POA_over_time_df.to_hdf(os.path.join(results_dir, 'contextual_POA_over_time_df.h5'), key='df', mode='w')

    noncontextual_POA_over_time_df = reader.calc_df(calc_POA_over_time, noncontextual_only=True)
    noncontextual_POA_over_time_df.to_hdf(os.path.join(results_dir, 'noncontextual_POA_over_time_df.h5'), key='df', mode='w')

    ## For median and IQR (inter-quartile range)

    contextual_POA_over_time_median = contextual_POA_over_time_df.groupby(
        'num_students_seen').median().to_numpy().flatten()
    contextual_POA_over_time_25 = contextual_POA_over_time_df.groupby('num_students_seen').quantile(
        0.25).to_numpy().flatten()
    contextual_POA_over_time_75 = contextual_POA_over_time_df.groupby('num_students_seen').quantile(
        0.75).to_numpy().flatten()

    np.save(os.path.join(results_dir, 'contextual_POA_over_time_median.npy'), contextual_POA_over_time_median)
    np.save(os.path.join(results_dir, 'contextual_POA_over_time_25.npy'), contextual_POA_over_time_25)
    np.save(os.path.join(results_dir, 'contextual_POA_over_time_75.npy'), contextual_POA_over_time_75)

    noncontextual_POA_over_time_median = noncontextual_POA_over_time_df.groupby(
        'num_students_seen').median().to_numpy().flatten()
    noncontextual_POA_over_time_25 = noncontextual_POA_over_time_df.groupby('num_students_seen').quantile(
        0.25).to_numpy().flatten()
    noncontextual_POA_over_time_75 = noncontextual_POA_over_time_df.groupby('num_students_seen').quantile(
        0.75).to_numpy().flatten()

    np.save(os.path.join(results_dir, 'noncontextual_POA_over_time_median.npy'), noncontextual_POA_over_time_median)
    np.save(os.path.join(results_dir, 'noncontextual_POA_over_time_25.npy'), noncontextual_POA_over_time_25)
    np.save(os.path.join(results_dir, 'noncontextual_POA_over_time_75.npy'), noncontextual_POA_over_time_75)



