import sys
import csv
import numpy


thompsonFile = 'thompsonoutput.csv'
softMaxFile = 'SoftMaxoutput.csv'

def compareRewards(input1=thompsonFile, input2=softMaxFile, index=0):
    #get the overall rewards (for now without indexes) for the two files
    rewards = [generateOverallReward(input1), generateOverallReward(input2)]
    print("Thompson Rewards: " + ", ".join(map(str, rewards[0])))
    print("SoftMax Rewards: " + ", ".join(map(str, rewards[1])))
    return rewards

def generateOverallReward(inputfile, index=0):
    #rewards for mooclet 1, 2, and 3
    rewards = [0,0,0]
    #number of rows so far
    numRows = 0
    with open(inputfile, newline='') as inf:
        reader = csv.DictReader(inf)
        for row in reader:
            rewards[0] += int(row['RewardMOOClet1'])
            rewards[1] += int(row['RewardMOOClet2'])
            rewards[2] += int(row['RewardMOOClet3'])
            numRows += 1
        
    rewards = [reward/numRows for reward in rewards]
    return rewards
        
        
        
        
        
        
        

def main():
  if len(sys.argv) == 3: 
    compareRewards(sys.argv[1], sys.argv[2])
  elif len(sys.argv) == 4:
      compareRewards(sys.argv[1], sys.argv[2], sys.argv[3])
  else:
    compareRewards()

if __name__ == "__main__":
  main()