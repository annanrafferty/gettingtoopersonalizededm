#!/bin/bash
numSims=500
# effectSizesT=(0.2 0.5 0.8);
effectSizesT=(0.5);
effectSizesB=(0.3);
softmaxBetas=(0.0 0.1 0.2 0.3 0.4 0.5);
#reorderingSpecs=('PreferBetterAction1' 'PreferBetterAction2' 'PreferBetterActions' 'PreferWorseAction1' 'PreferWorseAction2' 'PreferWorseActions')
reorderingSpecs=('PreferWorseAction1' 'PreferWorseAction2' 'PreferWorseActions')

# arrayLength=${#effectSizesT[@]};
# for ((i=0; i<$arrayLength; i++)); do
#     curEffectSize=${effectSizesT[$i]}
#     for ((j=0; j<${#softmaxBetas[@]}; j++)); do
#         for ((k=0; k<${#reorderingSpecs[@]}; k++)); do
#             softmaxBeta=${softmaxBetas[$j]}
#             reorderingSpec=${reorderingSpecs[$k]}
#             directoryName="ngSoftmax"$softmaxBeta""$reorderingSpec"ES"$curEffectSize;
#             echo $directoryName
#             mkdir $directoryName
#             python3 ~/banditalgorithms/src/louie_experiments/run_effect_size_simulations.py 0.5 -0.5 $curEffectSize $numSims $directoryName "Thompson" -1 $softmaxBeta $reorderingSpec 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
#             echo $!
#         done
#     done
#     wait
# done


# arrayLength=${#effectSizesB[@]};
# for ((i=0; i<$arrayLength; i++)); do
#     curEffectSize=${effectSizesB[$i]}
#     for ((j=0; j<${#softmaxBetas[@]}; j++)); do
#         for ((k=0; k<${#reorderingSpecs[@]}; k++)); do
#             softmaxBeta=${softmaxBetas[$j]}
#             reorderingSpec=${reorderingSpecs[$k]}
#             directoryName="bbSoftmax"$softmaxBeta""$reorderingSpec"ES"$curEffectSize;
#             echo $directoryName
#             mkdir $directoryName
#             python3 ~/banditalgorithms/src/louie_experiments/run_effect_size_simulations_beta.py $curEffectSize $numSims $directoryName "Thompson" "armsEqual" -1 $softmaxBeta $reorderingSpec 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
#             echo $!
#         done
#         wait
#     done
#
# done


# # Sims with no actual effect (so we can see how reordering affects false positive rate)
variancesT=(5.0 2.0 1.25);
nsT=(394 64 26); # Note: we're not crossing nsT and variancesT because variancesT is actual variance with ES with that num steps
arrayLength=${#variancesT[@]};
curEffectSize=0;
for ((n=0; n<${#nsT[@]}; n++)); do
    curN=${nsT[$n]}
    curVariance=${variancesT[$n]}
    for ((j=0; j<${#softmaxBetas[@]}; j++)); do
        for ((k=0; k<${#reorderingSpecs[@]}; k++)); do
            softmaxBeta=${softmaxBetas[$j]}
            reorderingSpec=${reorderingSpecs[$k]}
            directoryName="ngSoftmax"$softmaxBeta""$reorderingSpec"EqualMeans"$curVariance;
            echo $directoryName
            mkdir $directoryName
            python3 ~/banditalgorithms/src/louie_experiments/run_effect_size_simulations.py 0.0 0.0 $curVariance $numSims $directoryName "Thompson" $curN $softmaxBeta $reorderingSpec 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
            echo $!
        done
    done
    wait; #6 softmax betas x 6 reordering specs means 36 cores - finish using these before moving on
done

# # binary reward Sims with no actual effect
# nsB=(785 88 32);
# curEffectSize=0;
# for ((n=0; n<${#nsB[@]}; n++)); do
#     curN=${nsB[$n]}
#     for ((j=0; j<${#softmaxBetas[@]}; j++)); do
#         for ((k=0; k<${#reorderingSpecs[@]}; k++)); do
#             softmaxBeta=${softmaxBetas[$j]}
#             reorderingSpec=${reorderingSpecs[$k]}
#             directoryName="bbSoftmax"$softmaxBeta""$reorderingSpec"EqualMeans"$curVariance;
#             echo $directoryName
#             mkdir $directoryName
#             python3 ~/banditalgorithms/src/louie_experiments/run_effect_size_simulations_beta.py 0.5,0.5 $numSims $directoryName "Thompson" "armsEqual" $curN $softmaxBeta $reorderingSpec 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
#             echo $!
#         done
#     done
#     wait; #6 softmax betas x 6 reordering specs means 36 cores - finish using these before moving on
# done