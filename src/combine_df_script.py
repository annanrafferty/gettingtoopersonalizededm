'''
Created on Sep 21, 2017

Script for combining dataframes that were pickled across multiple simulations,
to output to a matlab readable format (CSV). 

@author: rafferty
'''

import os
import pandas as pd
from get_specs_from_filenames import *

valid_outcome_measures = {'problemcount','logcount', 'complete'}



# Directories to look in for dataframes
directories_to_combine = {'normalZeroESPriorChanges24April2018'}#{'empiricalParametersRedoProblemCount7November2017'}#{'empiricalParametersAll3November2017','empiricalParametersUniform6November2017'}#{'sameArms23August2017', 'sameArms24August2017'}

# Whether we'll be combining dfs from simulations with effect size = 0 or != 0.
zero_effect_size = True

# Whether we're working with reordered rewards, where we have to grab the amount of reordering and
# the preference direction
reordered_rewards = False

# Whether we're working with real data, in which case we need to get the experiment number, outcome variable
# Real data has two versions: parameter only (don't need to get whether we shuffled the data, or with parameters)
real_data = False
parameter_only = False 

# Suffix of relevant dataframe pickles
pickle_suffix = 'Df.pkl'

# Longer suffix to exclude - made because we have overlapping dataframe suffixes
pickle_exclude_suffix = 'OverallStatsDf.pkl'

#CSV to write out to
# out_csv = 'CombinedEmpiricalParamsRedoProblemCount.csv'
# out_csv = 'CombinedNonZeroEffectSizeDFNormalShiftedPrior.csv'
out_csv = 'CombinedZeroEffectSizeDFNormalShiftedPrior.csv'


# Add missing fields if necessary
add_unequal_variance_fields = True
columns = []
# Walk through all dataframes to combine to one CSV
is_first = True # We'll mark the first one we write to use write rather than append and to write headers
for directory in directories_to_combine:
    for file in os.listdir(directory):
        if file.endswith(pickle_suffix) and not file.endswith(pickle_exclude_suffix):
            filename = os.path.join(directory, file)
            cur_df = pd.read_pickle(filename)
            # Add columns related to simulation params - get these from filename
            # current columns: num_steps,sim,sample_size_1,sample_size_2,mean_1,mean_2,total_reward,ratio,power,actual_es,stat,pvalue,df,prior_type,sampling_type
            prior_type = get_prior_spec_from_filename(file)
            cur_df['prior_type'] = prior_type
            
            sampling_type = get_sampling_spec_from_filename(file)
            cur_df['sampling_type'] = sampling_type

            reward_type = get_reward_spec_from_filename(file)
            cur_df['reward_type'] = reward_type
            
            if zero_effect_size:
                variance = '-1.0'
                if reward_type == 'normal':
                    variance = get_variance_from_filename(file)
                cur_df['actual_arm_variance'] = float(variance)
            else:
                effect_size = float(get_effect_size_from_filename(file))
                cur_df['effect_size'] = effect_size
            
            if add_unequal_variance_fields:
                # Potentially missing fields: 'statUnequalVar','pvalueUnequalVar', 'dfUnequalVar'
                if 'statUnequalVar' not in cur_df.columns:
                    cur_df['statUnequalVar'] = cur_df['stat']
                    cur_df['pvalueUnequalVar'] = cur_df['pvalue']
                    cur_df['dfUnequalVar'] = cur_df['df']

            cur_df['power'] = cur_df['power'].fillna(-1)

            
            if reordered_rewards:
                cur_df['softmax'] = get_softmax_from_filename(file)
                cur_df['reorderingPreference'] = get_preference_type_from_filename(file)
                
            if real_data:
                cur_df['experimentNum'] = get_experiment_num_from_filename(file)
                cur_df['outcomeMeasure'] = get_outcome_measure_from_filename(file)
                if not parameter_only:
                    cur_df['shuffle'] = get_shuffle_boolean_from_real_data_filename(file)
                
            if len(columns) == 0:
                columns = cur_df.columns
            # Now write to csv in progress
            if is_first:
                is_first = False
                cur_df.to_csv(out_csv, encoding='utf-8', index=False, columns = columns)
            else:
                # After the first file, append and don't write headers again
                cur_df.to_csv(out_csv, encoding='utf-8', index=False, mode='a', header=False, columns = columns)
            

            
            
            

