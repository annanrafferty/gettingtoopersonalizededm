import sys
import numpy
import pandas
from math import sqrt,log

DELTA = 0.5
ALPHA = 1.0 + sqrt(log(2/DELTA)/2)

inputFile = pandas.read_csv('simulated_data_files_input.csv')

#hard coded for now...
outputColumns = ['SampleNumber', 'agequartilesUSER', 'ndaysactUSER', 'actionMOOClet1', 'mooclet1a', 'mooclet1b', 'mooclet1c']
contextColumns = ['agequartilesUSER', 'ndaysactUSER']
outputFile = pandas.DataFrame(columns = outputColumns)

# pullrowt goes from 1 to 4000


# p_arm1 = coeff*contextvector + calculateUCB(pullrowt,personcontextrowt)
# p_arm2
# p_arm3



def runLinUCB():
    #sampleFile["ucb"] = sampleFile.applymap(lambda x: calculateUCB(x.index, sampleFile))

    mooclet1versions = [0,1,2]
    mooclet2versions = [0,1,2]
    mooclet3versions = [0,1,2]
    actionNames = [["mooclet1a","mooclet1b","mooclet1c"],["mooclet2a","mooclet2b","mooclet2c"],["mooclet3a","mooclet3b","mooclet3c"]]
    actions = [mooclet1versions, mooclet2versions, mooclet3versions]
    
    #for all samples
    for i, row in enumerate(inputFile.values):
        
        #create current output row
        currentOutputRow = pandas.Series(index=outputColumns)
        
        #add sample number + context to output row
        currentOutputRow.set_value('SampleNumber', row[outputColumns.index('SampleNumber')])
        currentOutputRow.set_value('agequartilesUSER', row[outputColumns.index('agequartilesUSER')])
        currentOutputRow.set_value('ndaysactUSER', row[outputColumns.index('ndaysactUSER')])
        
        #stores all ucbs for a given iteration
#########do we still need this?########### I think it can be removed
        ucbs = []
        
        #assumes all values are above -1
        bestUpperBound = -1
        bestAction = '-1'
        
        #for all actions (currently only for mooclet1)
        for j, currentAction in enumerate(actionNames[0]):
            ucb = calculateUCB(i, inputFile, currentAction)
            ucbs.append(ucb)
            #print the val (change to save)
            #print(ucb)
            
            #add ucb to coefficients*context - JOSEPH NEED TO IMPLEMENT (next line)
            upperBound = """coefficients*context""" + ucb
            
            #add upperBound value to output row
            currentOutputRow.set_value(currentAction, row[outputColumns.index(currentAction)])
            
            if upperBound > bestUpperBound:
                bestAction = currentAction
            #end
            
        #end
        
        #add best action to output row
        currentOutputRow.set_value('actionMOOClet1', row[outputColumns.index('actionMOOClet1')])
        
        #add row to output rows
        outputFile.append(currentOutputRow, ignore_index = 'true')
        
    #end
    
    #output outputFile to csv file - (SAM?) NEED TO IMPLEMENT
    
    s = pandas.Series(ucbs)
    #file_with_ucb = pandas.merge(sampleFile, s)
    s.to_csv("UCBoutput.csv")


# DOug has defined UCB as the thing you ADD to predicted mean, coefficients*context
#contextA1 contextA2 contextA3
def calculateUCB(sampleNumber, samples, currentAction):

	currentSample = samples.iloc[sampleNumber]
	currentContext = currentSample[contextColumns]
	transposedContext = currentContext.T

	#identity matrix
	dimension = len(currentContext)
	identityMatrix = numpy.identity(dimension)


	A = None

	#m x d (first (sampleNumber-1) observations of the feature vector for given action)
		
	priorContext = pandas.DataFrame(index = contextColumns)

	if currentAction in samples.index:
		actionSamples = samples.groupby(currentAction, sort=False)
		actionSamples = actionSamples.get_group(currentAction)
		priorActions = actionContext.iloc[0:sampleNumber]
		priorContext = priorActions[contextColumns]
		
	if priorContext.size == 0:
		A = identityMatrix
	else:
		#D(a) transposed * D(a) + I(d)
		#d(a) = prior context
		transposedPriorContext = priorContext.T
		A = (transposedPriorContext.dot(priorContext)) + identityMatrix
	#end

	#multiply the following three
	#tranposedContext invert(A) context

	someNumber = (transposedContext.dot(numpy.linalg.inv(A))).dot(currentContext)
	upperConfidenceBound = ALPHA * sqrt(someNumber)

	return upperConfidenceBound
	
#end



def main():
    runLinUCB()
    
if __name__ == "__main__":
    main()

