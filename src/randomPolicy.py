import sys
import csv
import random
import math
import numpy




def calculateRandomPolicy(source, dest):
    numActions = 3
    numMooclets = 3
    with open(source, newline='') as inf, open(dest, 'w', newline='') as outf:
        reader = csv.DictReader(inf)
        fieldNamesOut = reader.fieldnames[0:3]
        #output the conditions chosen
        fieldNamesOut.append('MOOClet1')
        fieldNamesOut.append('MOOClet2')
        fieldNamesOut.append('MOOClet3')
        #output our samples drawn
        fieldNamesOut.append('RewardMOOClet1')
        fieldNamesOut.append('RewardMOOClet2')
        fieldNamesOut.append('RewardMOOClet3')

        
        writer = csv.DictWriter(outf, fieldnames=fieldNamesOut)
        writer.writeheader()
        sampleNumber = 0
        for row in reader:
            sampleNumber += 1
            #get the user vars
            ageQuartile = int(row['agequartilesUSER']);
            #user 0 instead of -1 for age quartiles
            if ageQuartile==-1:
              ageQuartile=0;
            
            nDaysAct = int(row['ndaysactUSER']);
            
                
            #choose a random action
            actions = []
            for i in range(numMooclets):
                actions.append(random.randint(0,numActions - 1))

            # get reward signals
            rewards = []
            for i in range(numMooclets):
                characterToAppend = chr(ord('A') + i)
                rewards.append(int(row['MOOClet' + str(i+1) + characterToAppend + str(actions[i]+1)]))
                


            #write out some of the inputs, which versions we chose, samples
            writer.writerow({'SampleNumber' : sampleNumber, 'agequartilesUSER': ageQuartile, 'ndaysactUSER' : nDaysAct,
             'MOOClet1' : actions[0], 'MOOClet2' : actions[1], 'MOOClet3' : actions[2],
                             'RewardMOOClet1' : rewards[0], 'RewardMOOClet2' : rewards[1], 'RewardMOOClet3' : rewards[2]})

def main():
  if len(sys.argv) == 3: 
    calculateRandomPolicy(sys.argv[1], sys.argv[2])
  else:
    calculateRandomPolicy('simulated_data_files_input.csv', 'testRandom_simData.csv')

if __name__ == "__main__":
  main()
