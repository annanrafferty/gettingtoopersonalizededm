import glob
import os
import re
import pandas as pd

# please see parquet demo.ipynb in google team drive/MAB for Experimentation/Winter 2021
# https://colab.research.google.com/drive/1iA0YeUoG2PlqRG6wujJ9AM1FySRe9-rN?usp=sharing


def remove_df(directory, keywords=["ModContextual", "NonContextual"], retain_csv=5, remove_reward_csv=True):
    """delete csv"""
    for kw in keywords:
        csv_list = glob.glob(os.path.join(directory, 'CompareBanditActionsOut_{}*.csv'.format(kw)))

        if len(csv_list) > 0:
            for csv in csv_list:
                # search for digits after T in string
                num_trial_search = re.search(r'T(\d+)', os.path.basename(csv))

                if num_trial_search:
                    num_trial = int(num_trial_search.group(1))
                    if num_trial not in range(retain_csv):
                        os.remove(csv)

    if remove_reward_csv:
        csv_list = glob.glob(os.path.join(directory, 'CompareBanditRewardFile_*.csv'))

        if len(csv_list) > 0:
            for csv in csv_list:
                # search for digits after T in string
                num_trial_search = re.search(r'T(\d+)', os.path.basename(csv))

                if num_trial_search:
                    num_trial = int(num_trial_search.group(1))
                    if num_trial not in range(retain_csv):
                        os.remove(csv)


def compress_df(directory, keywords=["ModContextual", "NonContextual", "Uniform"], delete_csv=True, retain_csv=5, remove_reward_csv=True):
    try:
        import pyarrow
    except ImportError:
        try:
            import fastparquet
        except ImportError:
            print("Please install pyarrow or fastparquet to convert compress csv to parquet")

    """collate as parquet"""
    for kw in keywords:
        csv_list = glob.glob(os.path.join(directory, 'CompareBanditActionsOut_{}*.csv'.format(kw)))

        if len(csv_list) > 0:
            max_trial = -1

            df_list = []
            for csv in csv_list:

                # search for digits after T in string
                num_trial_search = re.search(r'T(\d+)', os.path.basename(csv))

                if num_trial_search:
                    num_trial = int(num_trial_search.group(1))

                    df = pd.read_csv(csv, header=1)
                    df['Trial'] = num_trial
                    df_list.append(df)

                    max_trial = max(num_trial, max_trial)

                    # presume num_student same
                    num_student_search = re.search(r'N(\d+)', os.path.basename(csv))
                    if num_student_search:
                        num_student = num_student_search.group(1)

            if len(df_list) > 0:
                df = pd.concat(df_list)
                df.to_parquet(os.path.join(directory, '{}_Reward_N{}_T{}.parquet.gzip'.format(kw, num_student, max_trial)),
                              compression='gzip')

    if delete_csv:
        remove_df(directory, keywords, retain_csv, remove_reward_csv)
