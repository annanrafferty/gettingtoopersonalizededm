import matplotlib as mpl
mpl.use('Agg')
mpl.rc('savefig', dpi=600, bbox='tight')
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd
import glob as glob
import os
import sys
import re
import itertools
from multiprocessing import Pool

sns.set_style("whitegrid")
sns.color_palette('colorblind')
sns.set_context('paper')
import math


def sigmoid(x):
    return 1 / (1 + math.exp(-x))


def roundup(x):
    return int(math.ceil(x / 10.0)) * 10


class GraphPRewardByTimesPicked:
    def __init__(self, main_dir, write_dir):
        self.main_dir = main_dir
        self.write_dir = write_dir

        os.makedirs(self.write_dir, exist_ok=True)
        os.chdir(self.main_dir)

    def _parse_dir(self, dir_name):

        effect_size, group_size, effect_probability = dir_name.lower().split('_')
        if effect_size == 'm':
            effect_size = 'Main'
        elif effect_size == 'co':
            effect_size = 'Crossover'

        if group_size == '55':
            group_size = '50-50'
        elif group_size == '82':
            group_size = '80-20'

        if effect_probability == '1':
            effect_probability = 'None'
        elif effect_probability == '5':
            effect_probability = 'Rand'

        return effect_size, group_size, effect_probability

    def _get_df_list(self, dir_name):
        sample_size = sys.maxsize

        df_con_list = [pd.read_csv(f, header=1) for i, f in
                       enumerate(glob.glob(os.path.join(dir_name, '*ActionsOut_Contextual*.csv'))) if i < sample_size]
        df_noncon_list = [pd.read_csv(f, header=1) for i, f in
                          enumerate(glob.glob(os.path.join(dir_name, '*ActionsOut_NonContextual*.csv'))) if
                          i < sample_size]

        return df_con_list, df_noncon_list

    def main(self):
        df_list = []
        for child in next(os.walk('.'))[1]:

            try:
                effect_size, group_size, effect_probability = self._parse_dir(child)
                print(child)
            except ValueError:
                continue

            df_con_list, df_noncon_list = self._get_df_list(child)

            df_con_list = [self._parse_p_reward_by_times_picked(df, con=True) for df in df_con_list]
            df_noncon_list = [self._parse_p_reward_by_times_picked(df, con=False) for df in df_noncon_list]

            df_con = pd.concat(df_con_list)
            df_noncon = pd.concat(df_noncon_list)

            df_con["Type"] = 'Con'
            df_noncon["Type"] = 'Noncon'

            df = pd.concat([df_con, df_noncon])

            title = 'Arm {} Expected P(Reward) over trials' + '| ES ' + effect_size + '| GS ' + group_size + '| EP ' + effect_probability
            self._graph_p_reward_by_times_picked(df, title)

            df_list.append(df)
        return df_list

    def _parse_p_reward_by_times_picked(self, df, con):
        columns = ['Group Sizes']
        for arm in range(1, 3):
            columns.append('Arm {} Times Picked'.format(arm))
            columns.append('Arm {} Total Reward'.format(arm))
            columns.append('Arm {} % correct'.format(arm))
            columns.append('Arm {} Expected P(Reward)'.format(arm))
        df_table = pd.DataFrame(index=range(2), columns=columns, dtype=np.float32).fillna(0)

        for group in range(2):
            df_group = df[df['contextualVariable0'] == group]
            df_table.at[group, 'Group Sizes'] = len(df_group) / len(df)
            for arm in range(1, 3):
                df_group_arm = df_group[df_group['AlgorithmAction'] == arm]
                df_table.at[group, 'Arm {} Times Picked'.format(arm)] = len(df_group_arm)
                df_table.at[group, 'Arm {} Total Reward'.format(arm)] = sum(df_group_arm['ObservedRewardofAction'])
                df_table.at[group, 'Arm {} % correct'.format(arm)] = \
                    df_table.at[group, 'Arm {} Total Reward'.format(arm)] / (
                            df_table.at[group, 'Arm {} Times Picked'.format(arm)] + 1e-9) * 100
                if con:
                    weights = [float(weight) for weight in df['Action{}EstimatedMu'.format(arm)].iloc[-1].split(',')]
                    df_table.at[group, 'Arm {} Expected P(Reward)'.format(arm)] = sigmoid(np.dot(weights, (1, group)))
                else:
                    df_table.at[group, 'Arm {} Expected P(Reward)'.format(arm)] = \
                        df['Action{}EstimatedProb'.format(arm)].iloc[-1]

        return df_table

    def _graph_p_reward_by_times_picked(self, df, title):
        for arm in range(1, 3):
            g = sns.FacetGrid(data=df, col="Type", xlim=(0, 1000), ylim=(0, 1))
            g = g.map_dataframe(sns.scatterplot,
                                x="Arm {} Times Picked".format(arm), y="Arm {} Expected P(Reward)".format(arm),
                                size="Arm {} % correct".format(arm), hue="Arm {} % correct".format(arm))
            g.add_legend()
            g.set_ylabels("Arm {} Expected P(Reward)".format(arm))
            g.set_xlabels("Trials")
            plt.subplots_adjust(top=0.8)
            g.fig.suptitle(title.format(arm))
            g.savefig(os.path.join(self.write_dir, title.format(arm)))


graphing = GraphPRewardByTimesPicked('/disk2/contextualBanditSimsNoEffect/ESP',
                                     '/disk2/contextualBanditSimsNoEffect/ESP/ContextualReward')
graphing.main()

# class GraphExpandingMeanReward:
#     def __init__(self, main_dir, write_dir):
#         self.main_dir = main_dir
#         self.write_dir = write_dir
#
#         os.makedirs(self.write_dir, exist_ok=True)
#         os.chdir(self.main_dir)
#
#     def _parse_dir(self, dir_name):
#
#         effect_size, group_size, effect_probability = dir_name.lower().split('_')
#         if effect_size == 'm':
#             effect_size = 'Main'
#         elif effect_size == 'co':
#             effect_size = 'Crossover'
#
#         if group_size == '55':
#             group_size = '50-50'
#         elif group_size == '82':
#             group_size = '80-20'
#
#         if effect_probability == '1':
#             effect_probability = 'None'
#         elif effect_probability == '5':
#             effect_probability = 'Rand'
#
#         return effect_size, group_size, effect_probability
#
#     def _get_df_list(self, dir_name):
#         sample_size = sys.maxsize
#
#         df_con_list = [pd.read_csv(f, header=1) for i, f in
#                        enumerate(glob.glob(os.path.join(dir_name, '*ActionsOut_Contextual*.csv'))) if i < sample_size]
#         df_noncon_list = [pd.read_csv(f, header=1) for i, f in
#                           enumerate(glob.glob(os.path.join(dir_name, '*ActionsOut_NonContextual*.csv'))) if
#                           i < sample_size]
#
#         return df_con_list, df_noncon_list
#
#     def main(self):
#         for child in next(os.walk('.'))[1]:
#             if child in self.write_dir:
#                 continue
#             else:
#                 print(child)
#
#             effect_size, group_size, effect_probability = self._parse_dir(child)
#             df_con_list, df_noncon_list = self._get_df_list(child)
#
#             df_con_list = [self._parse_contextual_expanding_mean_reward(df) for df in df_con_list]
#             df_noncon_list = [self._parse_contextual_expanding_mean_reward(df) for df in df_noncon_list]
#
#             df_con = pd.concat(df_con_list)
#             df_noncon = pd.concat(df_noncon_list)
#
#             df_con["Type"] = 'Con'
#             df_noncon["Type"] = 'Noncon'
#
#             df = pd.concat([df_con, df_noncon])
#
#             title = 'Expanding Mean Reward over trials' + '| ES ' + effect_size + '| GS ' + group_size + '| EP ' + effect_probability
#             self._graph_expanding_mean_reward(df, title)
#
#     def _parse_contextual_expanding_mean_reward(self, df_):
#         df = df_[['n', 'contextualVariable0', 'ObservedRewardofAction']]
#         df = df.groupby(['contextualVariable0'], group_keys=False)['ObservedRewardofAction'].expanding().mean()
#         df = df.to_frame('Expanding Mean Reward').reset_index().rename(index=str, columns={'level_1': 'n'})
#         return df
#
#     def _graph_expanding_mean_reward(self, df, title):
#         g = sns.FacetGrid(data=df, col="Type", hue='contextualVariable0', xlim=(0, 1000), ylim=(0, 1))
#         g = g.map_dataframe(sns.lineplot, x='n', y='Expanding Mean Reward', ci=68)
#         g.add_legend()
#         g.set_ylabels("Expanding Mean Reward")
#         g.set_xlabels("Trials")
#         plt.subplots_adjust(top=0.8)
#         g.fig.suptitle(title)
#         g.savefig(os.path.join(self.write_dir, title))
#
# graphing = GraphExpandingMeanReward('/disk2/contextualBanditSimsNoEffect/ESP',
#                                     '/disk2/contextualBanditSimsNoEffect/ESP/ExpandingMeanReward')
# graphing.main()
