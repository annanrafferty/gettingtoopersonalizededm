from generate_comparison_data import run
from os import listdir
from os.path import isfile, join, isdir
import sys
import os
import time

def main():
    # mypath = "../../configFiles/ParamsConfigs/complete"
    # list_of_ASSISments_config_files = [f for f in listdir(mypath) if isfile(join(mypath, f))]
    # for config_file_name_ASSISTments in list_of_ASSISments_config_files:
    #     inputPath = "../../configFiles/ParamsConfigs/complete/" + config_file_name_ASSISTments
    #     run(inputPath, 40)
    #     print("Finished one config")
    # path = "../../configFiles/OutcomesConfigs/complete"
    # path = "../../configFiles/OutcomesConfigs/ProblemCount"

    # "../../configFiles/ParamsConfigs/complete", "../../configFiles/ParamsConfigs/ProblemCount",
    # "../../configFiles/ParamsConfigsLambda1000/complete", "../../configFiles/ParamsConfigsLambda1000/ProblemCount",
    # "../../configFiles/ParamsConfigsLambda1M/complete", "../../configFiles/ParamsConfigsLambda1M/ProblemCount",


    # pathList = ["../../configFiles/OutcomesConfigsLambda1000/complete", "../../configFiles/OutcomesConfigsLambda1000/ProblemCount",
    #             "../../configFiles/OutcomesConfigsLambda1M/complete", "../../configFiles/OutcomesConfigsLambda1M/ProblemCount"]

    # Make sure to run this script inside louie_experiments
    # because the paths below assumes that.

    # pathList = [
    #
    #     "../../configFiles/ASSISTmentsRegConfigs/ParamsConfigsLambda1/complete",
    #     "../../configFiles/ASSISTmentsRegConfigs/ParamsConfigsLambda1/ProblemCount",
    #
    #     "../../configFiles/ASSISTmentsRegConfigs/ParamsConfigsLambda1000/complete",
    #     "../../configFiles/ASSISTmentsRegConfigs/ParamsConfigsLambda1000/ProblemCount",
    #
    #     "../../configFiles/ASSISTmentsRegConfigs/ParamsConfigsLambda1000000/complete",
    #     "../../configFiles/ASSISTmentsRegConfigs/ParamsConfigsLambda1000000/ProblemCount",
    #
    #     "../../configFiles/ASSISTmentsRegConfigs/OutcomesConfigsLambda1/complete",
    #     "../../configFiles/ASSISTmentsRegConfigs/OutcomesConfigsLambda1/ProblemCount",
    #
    #     "../../configFiles/ASSISTmentsRegConfigs/OutcomesConfigsLambda1000/complete",
    #     "../../configFiles/ASSISTmentsRegConfigs/OutcomesConfigsLambda1000/ProblemCount",
    #
    #     "../../configFiles/ASSISTmentsRegConfigs/OutcomesConfigsLambda1000000/complete",
    #     "../../configFiles/ASSISTmentsRegConfigs/OutcomesConfigsLambda1000000/ProblemCount"
    #
    # ]

    pathList = [
        "../../configFiles/ASSISTmentsPyramidalConfigs/OutcomesConfigs/complete",
        "../../configFiles/ASSISTmentsPyramidalConfigs/OutcomesConfigs/ProblemCount",
        "../../configFiles/ASSISTmentsPyramidalConfigs/ParamsConfigs/complete",
        "../../configFiles/ASSISTmentsPyramidalConfigs/ParamsConfigs/ProblemCount",
    ]

    for path in pathList:
        print(path)
        assert isdir(path), f'{path} is not a valid directory.'

    for i, path in enumerate(pathList):
        if len(sys.argv) > 1:
            path = sys.argv[1]

        num_cores = 30
        if len(sys.argv) > 2:
            num_cores = int(sys.argv[2])
        list_of_ASSISments_config_files = [f for f in listdir(path) if isfile(join(path, f)) and f.endswith(".json")]
        # list_of_ASSISments_config_files = ["ASSISTmentsOutcomesConfig226210.json"]

        num_configs = len(list_of_ASSISments_config_files)
        for j, config_file_name_ASSISTments in enumerate(list_of_ASSISments_config_files):
            inputPath = os.path.join(path, config_file_name_ASSISTments)
            run(
                inputPath, nCores=num_cores,
                additional_msg=f"In the {i+1}/{len(pathList)} th path: the {j+1}/{num_configs}th config"
            )

if __name__ == "__main__":
    main()
