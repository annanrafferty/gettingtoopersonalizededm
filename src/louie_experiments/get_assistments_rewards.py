'''
Created on Nov 3, 2017

@author: rafferty
'''
import csv

EXPERIMENT_ID_HEADER = 'problem_set';
CONDITION_HEADER = 'InferredCondition'
EXPERIENCED_CONDITION_HEADER = 'ExperiencedCondition'
VIDEO_HEADER = 'Could See Video'

def read_assistments_rewards(data_file, reward_header, experiment_identifier, is_cost = False):
    arm_1_identifier = 'C'
    arm_1_rewards = []
    arm_2_rewards = []
    with open(data_file, newline='') as inf:
        reader = csv.DictReader(inf)
        for row in reader:
            if row[EXPERIMENT_ID_HEADER].strip() == experiment_identifier.strip():
                # Check if we have a value for this student - may not have values for all students

                if row[reward_header].strip() != "":
                    # Include this row if the student experienced a condition
                    if row[EXPERIENCED_CONDITION_HEADER] == 'TRUE' and \
                    not (row[VIDEO_HEADER] == '0' and row[CONDITION_HEADER] == 'E'):
                        queue = arm_1_rewards
                        if arm_1_identifier != row[CONDITION_HEADER]:
                            queue = arm_2_rewards
                        cur_reward = float(row[reward_header])
                        if is_cost:
                            cur_reward *= -1
                        queue.append(cur_reward)
    
        
    if sum(arm_1_rewards)/len(arm_1_rewards) > sum(arm_2_rewards)/len(arm_2_rewards):
        return arm_1_rewards, arm_2_rewards
    else:
        return arm_2_rewards, arm_1_rewards
                    
if __name__ == "__main__":
    # Debugging only
    arm_1_rewards, arm_2_rewards = read_assistments_rewards('/Users/rafferty/Downloads/ThisOne.csv', 'ProblemCount', '255116')
    print(len(arm_1_rewards))
    print(len(arm_2_rewards))
    print(sum(arm_1_rewards)/len(arm_1_rewards))
    print(sum(arm_2_rewards)/len(arm_2_rewards))

    
                