'''
Created on Oct 13, 2019
Based on the get_assistments_rewards module by rafferty

@author: rafferty, yeec, liz2
'''
import sys
import itertools
import json
import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import OneHotEncoder

EXPERIMENT_ID_HEADER = 'problem_set'
CONDITION_HEADER = 'Condition'
ARM_IDENTIFIERS = ['C', 'E']
EXPERIENCED_CONDITION_HEADER = 'ExperiencedCondition'
VIDEO_HEADER = 'Could See Video'
REWARD_HEADER = 'Reward'
COMPLETE_HEADER = 'complete'
PROBLEM_COUNT_HEADER = 'ProblemCount'


class AssistmentsData:
    def __init__(self, config, auto=False):
        """
        EXPECTS (in config):
            "source" - Location of the ASSISTments data, as csv
            "experimentId" - Which experiment to draw from
            "contextHeaders" - Headers of data to use as contextual variables
            ("qcutBehavior") - Dict of {header: num} where each header's values will be cut into num quantiles
            ("rewardScheme") - Uses COMPLETE_HEADER as reward if no value, or uses specific scheme e.g. "ProblemCount"

        :param config: dict with at least the above information
        :param auto: whether to automatically compute; default False; can call .retrieve_data() to compute
        """
        # items from config
        self.data_file = config["source"]
        self.experiment_identifier = config["experimentId"]
        self.context_headers = config["contextHeaders"]
        self.qcut_behavior = config["qcutBehavior"] if "qcutBehavior" in config else dict()
        self.reward_scheme = config["rewardScheme"] if "rewardScheme" in config else None
        # items to be set in retrieveData
        self.df = None
        self.context_dict = None
        self.context_vectors = None
        # set values for the three above
        if auto:
            self.retrieve_data()

    def retrieve_data(self):
        # Read pandas dataframe from file
        df = pd.read_csv(self.data_file)
        df = df[df[EXPERIMENT_ID_HEADER] == self.experiment_identifier]

        # Qcut values of marked headers into n bins according to dict value (int)
        for header in self.qcut_behavior.keys():
            # Drop n/a fields
            df = df.dropna(subset=[header])
            # Quantile-cut marked fields
            df[header] = pd.qcut(df[header], self.qcut_behavior[header], labels=False)

        # Create dict of unique values of context fields
        self.context_dict = dict()
        for header in self.context_headers:
            # Drop n/a fields
            df = df.dropna(subset=[header])
            # Mark values in dict
            # TODO: no guarantee that these are 0-based indices; may not actually break anything
            self.context_dict[header] = sorted(df[header].unique())

        # Create list of all possible unique context vectors
        self.context_vectors = list(itertools.product(*[range(len(self.context_dict[header])) for header in
                                                        self.context_headers]))

        # Drop students that didn't experience condition
        df = df[df[EXPERIENCED_CONDITION_HEADER] == True]
        df = df[~((df[VIDEO_HEADER] == 0) & (df[CONDITION_HEADER] == 'E'))]

        # Generate reward column
        df = AssistmentsData.append_reward(df, self.reward_scheme)

        # Save finalized df
        self.df = df

    def get_context_dict(self):
        if self.context_dict is None:
            self.retrieve_data()
        return self.context_dict

    def get_context_headers(self):
        return self.context_headers

    def get_context_vectors(self):
        if self.context_vectors is None:
            self.retrieve_data()
        return self.context_vectors

    def get_df(self):
        if self.df is None:
            self.retrieve_data()
        return self.df

    @staticmethod
    def append_reward(df, reward_scheme):
        if reward_scheme is not None and reward_scheme.lower() == 'problemcount':
            df = AssistmentsData.append_problemcount_reward(df)
        else:
            df = AssistmentsData.append_complete_reward(df)
        # reward should not be None
        return df

    @staticmethod
    def append_complete_reward(df):
        # assign directly from complete
        df[REWARD_HEADER] = df[COMPLETE_HEADER]
        return df

    @staticmethod
    def append_problemcount_reward(df):
        # first, assign those who did complete the assignment by relation to median
        completed = df[COMPLETE_HEADER] == 1
        # quantile into binary across median problems to complete
        df[REWARD_HEADER] = pd.qcut(df[completed][PROBLEM_COUNT_HEADER], 2, labels=False, duplicates='drop')
        # flip: those with median or lower problem count get 1 reward; higher than median get 0
        df[REWARD_HEADER] = df[REWARD_HEADER].replace({1: 0, 0: 1})

        # then, assign those who did not complete the assignment to 0 reward
        df[REWARD_HEADER] = df[REWARD_HEADER].fillna(0)

        return df



def read_assistments_outcomes_to_config(config):
    """Adds ASSISTments outcomes information to the given config dict

    EXPECTS (in config):
        "source" - Location of the ASSISTments data, as csv
        "experimentId" - Which experiment to draw from
        "contextHeaders" - Headers of data to use as contextual variables
        ("qcutBehavior") - Dict of {header: num} where each header's values will be cut into num quantiles
        ("rewardScheme") - Uses COMPLETE_HEADER as reward if no value, or uses specific scheme e.g. "ProblemCount"

    ADDS (in place; nothing is returned):
        "rewardDict" - Dict where [action][vector] yields queue of (observed) rewards as list
        "vectorQueue" - Queue of observed context vectors
        "contextualStructure" - List of contextual variables as ints according to their respective number of groups

    :param config: dict with at least the above information
    """
    # Retrieve data
    assistments_data = AssistmentsData(config, auto=True)
    context_dict = assistments_data.get_context_dict()
    context_headers = assistments_data.get_context_headers()
    context_vectors = assistments_data.get_context_vectors()
    df = assistments_data.get_df()

    # Create reward queues
    reward_dict = {i: {vector: [] for vector in context_vectors} for i in range(len(ARM_IDENTIFIERS))}
    for i in range(len(ARM_IDENTIFIERS)):
        for vector in context_vectors:
            # Condition df on arm, then on each context var
            conditioned_df = df[df[CONDITION_HEADER] == ARM_IDENTIFIERS[i]]
            for j in range(len(vector)):
                header = context_headers[j]
                value = context_dict[header][vector[j]]
                conditioned_df = conditioned_df[conditioned_df[header] == value]
            # Add reward of all compatible students to dict
            reward_dict[i][vector] = conditioned_df[REWARD_HEADER].tolist()

    # Make randomized list of context vectors
    vector_queue = []
    for vector in context_vectors:
        conditioned_df = df
        for j in range(len(vector)):
            header = context_headers[j]
            value = context_dict[header][vector[j]]
            conditioned_df = conditioned_df[conditioned_df[header] == value]
        # Add number of instances of vector to vector_dict
        vector_queue.extend([vector]*len(conditioned_df))

    # rebuild structure
    contextual_structure = [len(context_dict[header]) for header in context_headers]

    # update config; nothing is returned
    config["rewardDict"] = reward_dict
    config["vectorQueue"] = vector_queue
    config["contextualStructure"] = contextual_structure


def read_assistments_params_to_config(config):
    """Adds ASSISTments parameters information to the given config dict

        EXPECTS (in config):
            "source" - Location of the ASSISTments data, as csv
            "experimentId" - Which experiment to draw from
            "contextHeaders" - Headers of data to use as contextual variables
            ("qcutBehavior") - Dict of {header: num} where each header's values will be cut into num quantiles
            ("rewardScheme") - Uses COMPLETE_HEADER as reward if no value, or uses specific scheme e.g. "ProblemCount"

        ADDS (in place; nothing is returned):
            "contextualStructure" - List of contextual variables as ints according to their respective number of groups
            "contextualGenerationProcess" -
            "conditions" -

        :param config: dict with at least the above information
        """
    # Retrieve data
    assistments_data = AssistmentsData(config, auto=True)
    context_dict = assistments_data.get_context_dict()
    context_headers = assistments_data.get_context_headers()
    context_vectors = assistments_data.get_context_vectors()
    df = assistments_data.get_df()

    # rebuild structure
    contextual_structure = [len(context_dict[header]) for header in context_headers]

    # create gen dict from proportions of values
    contextual_gen_process = dict()
    for i in range(len(context_headers)):
        prop_df = df[context_headers[i]].value_counts(normalize=True, sort=False)
        props = list(prop_df)
        contextual_gen_process[i] = props

    # fit one hot encoder
    X = df[[context_headers[0]]] # TODO: currently only supports 1 convar
    ohe = OneHotEncoder(sparse=False, categories='auto')
    ohe.fit(X)
    X = ohe.transform(X)
    y = df[REWARD_HEADER]

    # get logistic regression coeffs from each condition
    conditions = []
    for action in ARM_IDENTIFIERS:
        condition = df[CONDITION_HEADER] == action
        coeffs = []

        clf = LogisticRegression(solver='liblinear')
        clf.fit(X[condition], y[condition])
        coeffs.extend(list(clf.intercept_))
        coeffs.extend(list(clf.coef_[0]))

        conditions.append(coeffs)
    conditions = [conditions] # TODO: currently only supports 1 experiment

    # update config; nothing is returned
    config["contextualStructure"] = contextual_structure
    config["contextualGenerationProcess"] = contextual_gen_process
    config["conditions"] = conditions


if __name__ == "__main__":
    # Debugging only
    with open(sys.argv[1]) as jsonFile:
        config = json.load(jsonFile)
    if "simType" not in config:
        print('Config not compatible; "simType" unspecified')
        exit(1)

    if config["simType"] == "parameters":
        print("Reading parameters config:")
        read_assistments_params_to_config(config)
        for updated_entry in "contextualStructure", "contextualGenerationProcess", "conditions":
            print()
            print(updated_entry + ' -')
            print(config[updated_entry])
    elif config["simType"] == "outcomes":
        print("Reading outcomes config:")
        read_assistments_outcomes_to_config(config)
        for updated_entry in "rewardDict", "vectorQueue", "contextualStructure":
            print()
            print(updated_entry + ' -')
            print(config[updated_entry])
    else:
        print('Config not compatible; "simType": "' + config["simType"] + '" not recognized')
        exit(1)