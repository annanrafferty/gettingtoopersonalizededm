import glob
import os
import generate_comparison_data

print(os.getcwd())
for configurationFile in glob.glob('/home/liz2/MAB/banditalgorithms/configFiles/ESPConfigs/*.json'):
    print(configurationFile)
    generate_comparison_data.run(configurationFile)