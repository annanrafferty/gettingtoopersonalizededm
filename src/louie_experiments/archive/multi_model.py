﻿import numpy as np
from base_model import *
from beta_bernoulli import *
from output_format import *


class MultiBetaBern(BaseModel):
    '''
    Context-conditional Beta-Bernoulli model for Thompson Sampling.
    This model uses separate Beta-Bernoulli models for each possible context.
    Not available for continuous contextual variables.
    '''

    def __init__(self, success, failure, contextualStructure):
        self.struct = contextualStructure.contextualStructure
        self.contexts = contextualStructure.getAllContextualCombinations()
        self.models = [BetaBern(success=success, failure=failure) for _ in range(len(self.contexts))]

    ## BEGIN SUPPORT METHODS ##

    def __getModelByContext(self, x, includeIntercept=True):
        contextVector = self.__undummyContext(x)
        index = self.contexts.index(contextVector)
        return self.models[index]

    def __undummyContext(self, x, includeIntercept=True):
        '''
        Converts a dummy-coded context vector into a base context vector
        '''
        index = 1 if includeIntercept else 0
        contextVector = []
        for contextVar in self.struct:
            subcontext = x[index:(index+contextVar-1)]
            contextVector.append(self.__undummySingle(subcontext))
            index += contextVar
        return tuple(contextVector)

    def __undummySingle(self, subcontext):
        '''
        Converts a dummy-coded subarray of context into the value of a contextual
        variable
        '''
        for i in range(len(subcontext)):
            if subcontext[i] == 1:
                return i + 1
        return 0

    ## END SUPPORT METHODS ##

    def update_posterior(self, x, y):
        # update success/failure counts per observed reward
        model = self.__getModelByContext(x)
        model.update_posterior(x, y)

    def draw_expected_value(self, x, num_samples = 1):
        model = self.__getModelByContext(x)
        return model.draw_expected_value(x, num_samples=num_samples)

    def remove_from_model(self, x, y):
        model = self.__getModelByContext(x)
        model.remove_from_model(x, y)

    def write_parameters(self, out_row, action, context = None):
        totalSuccess = 0
        totalFailure = 0
        for model in self.models:
            totalSuccess += model.success
            totalFailure += model.failure

        # success count for each action
        out_row[H_ALGO_ACTION_SUCCESS.format(action + 1)] = totalSuccess
                
        # failure count for each action
        out_row[H_ALGO_ACTION_FAILURE.format(action + 1)] = totalFailure

        # estimated reward probability for each arm is simply the
        # mean of the current beta distribution
        out_row[H_ALGO_ESTIMATED_PROB.format(action + 1)] = \
            totalSuccess / float(totalFailure)
    
    def get_expected_value(self):
        totalSuccess = 0
        totalFailure = 0
        for model in self.models:
            totalSuccess += model.success
            totalFailure += model.failure
        return totalSuccess / float(totalSuccess + totalFailure)

    def save_state(self):
        for model in self.models:
            model.save_state()


    def restore_state(self):
        for model in self.models:
            model.restore_state()


    def reset_state(self):
        for model in self.models:
            model.reset_state()