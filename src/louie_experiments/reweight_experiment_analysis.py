'''
Created on Jul 12, 2018

Experimenting with inverse probability of treatment weighting.
Assumes experiment has been previously run.

@author: rafferty
'''

import pandas as pd
import numpy as np


from output_format import H_ALGO_ACTION_FAILURE, H_ALGO_ACTION_SUCCESS, H_ALGO_ACTION, H_ALGO_OBSERVED_REWARD
from output_format import H_ALGO_ESTIMATED_MU, H_ALGO_ESTIMATED_V, H_ALGO_ESTIMATED_ALPHA, H_ALGO_ESTIMATED_BETA
from output_format import H_ALGO_PROB_BEST_ACTION, H_ALGO_NUM_TRIALS
import beta_bernoulli
import thompson_policy
import ng_normal
import run_effect_size_simulations
import run_effect_size_simulations_beta
import read_config

IPW_MEAN_HEADER = "IPWMeanAction{}"
HT_MEAN_HEADER = "HTMeanAction{}"
MLE_MEAN_HEADER = "MLEMeanAction{}"
MAX_WEIGHT_COUNT_HEADER = "CountOfMaxWeights"
SAMPLES_HEADER = "TotalSamples"
MAX_ALLOWED_WEIGHT_HEADER = "MaxWeightParam"
NUM_STEPS_HEADER = "NumSteps"
NUM_SAMPLES_BY_ACTION_HEADER = "NumSamplesAction{}"
PRIOR_MEAN_HEADER = "PriorMean"

IPW_OUT_FILE_SUFFIX = "IPWInfo.pkl" 
IPW_OUT_FILE_CSV_SUFFIX = "IPWInfo.csv" 
WRITE_CSV = False

IPWS_COL = "ipws"
EPSILON_PROB = .000001



USE_CACHED_PROBS = True # only relevant for binary rewards - normal rewards never cached because getting the same sequence is prob 0



def calculate_all_ipw_estimates(num_sims, step_sizes, outfile_directory, outfile_prefix,
                                 prior, num_samples = 100, num_actions = 2, binary_rewards = True,
                                 config = {}):
    assert num_actions == 2
    read_config.apply_defaults(config)
    cached_probs = {}
    rows = []
    for i in range(num_sims):
        for num_steps in step_sizes:
            if not binary_rewards or not USE_CACHED_PROBS:
                cached_probs = {} # reinitialize the caching every cycle if we aren't caching
                
            if binary_rewards:
                cur_output_file = run_effect_size_simulations_beta.get_output_filename(outfile_directory, num_steps, i)
            else:
                cur_output_file = run_effect_size_simulations.get_output_filename(outfile_directory, num_steps, i)
            #print("processing file:",cur_output_file)
            ipw_row = calculate_ipw(cur_output_file, num_samples, num_actions, cached_probs, prior, binary_rewards, 
                                    config = config)
            #print("processing completed:",cur_output_file)

            ipw_row[NUM_STEPS_HEADER] = num_steps
            rows.append(ipw_row)
    
    dataframe_headers = []
    ipw_headers = [IPW_MEAN_HEADER,HT_MEAN_HEADER,MLE_MEAN_HEADER, NUM_SAMPLES_BY_ACTION_HEADER]
    for header in ipw_headers:
        for action in range(num_actions):
            dataframe_headers.append(header.format(action + 1))
    dataframe_headers.append(MAX_WEIGHT_COUNT_HEADER)
    dataframe_headers.append(NUM_STEPS_HEADER)
    df = pd.DataFrame.from_records(rows, columns = dataframe_headers)
    df[MAX_ALLOWED_WEIGHT_HEADER] = config[read_config.MAX_WEIGHT_HEADER]
    df[SAMPLES_HEADER] = num_samples
#     if binary_rewards:
#         outfile_prefix = run_effect_size_simulations_beta.get_outfile_prefix(outfile_directory, bandit_type_prefix, effect_size, n)
#     else: 
#         outfile_prefix = run_effect_size_simulations.get_outfile_prefix(outfile_directory, bandit_type_prefix, effect_size, n, variance)
    print("writing to", outfile_prefix + IPW_OUT_FILE_SUFFIX) 
    df.to_pickle(outfile_prefix + IPW_OUT_FILE_SUFFIX)
    if WRITE_CSV:
        df.to_csv(outfile_prefix + IPW_OUT_FILE_CSV_SUFFIX)


def create_models_normal(actions_df, prior, num_actions):
    assert num_actions == 2
    cache_keys = [] # no cache_keys for normally-dist. rewards
    all_models = []
    for action in range(num_actions):
        cur_models = [ng_normal.NGNormal(mu=mu, k=k, alpha=alpha, beta=beta) for (mu,k, alpha,beta) in 
                      zip(actions_df.loc[:,H_ALGO_ESTIMATED_MU.format(action + 1)],
                          actions_df.loc[:,H_ALGO_ESTIMATED_V.format(action + 1)],
                          actions_df.loc[:,H_ALGO_ESTIMATED_ALPHA.format(action + 1)],
                          actions_df.loc[:,H_ALGO_ESTIMATED_BETA.format(action + 1)])]
        # add in the one for the prior
        cur_models.insert(0, ng_normal.NGNormal(mu=prior[0], k=prior[1], alpha=prior[2], beta=prior[3]))
        all_models.append(cur_models)
    return all_models, cache_keys

def create_models_binary(actions_df, prior, num_actions):
    assert num_actions == 2

    all_models = []
    cache_keys = [[] for _ in range(actions_df.shape[0])]
    for action in range(num_actions):
        [cache_keys[i].extend((successes,failures)) for (i,successes,failures) in zip(range(actions_df.shape[0]),actions_df.loc[:,H_ALGO_ACTION_SUCCESS.format(action + 1)],actions_df.loc[:,H_ALGO_ACTION_FAILURE.format(action + 1)])]
        cur_models = [beta_bernoulli.BetaBern(successes, failures) for (successes,failures) in zip(actions_df.loc[:,H_ALGO_ACTION_SUCCESS.format(action + 1)],actions_df.loc[:,H_ALGO_ACTION_FAILURE.format(action + 1)])]
        # add in the one for the prior
        cur_models.insert(0, beta_bernoulli.BetaBern(prior[0], prior[1]))
        all_models.append(cur_models)
    # Add in a cache key for the prior
    cache_keys.insert(0, prior*num_actions)
    return all_models,cache_keys
        


def calculate_ipw(actions_infile, num_samples, num_actions = 2, cached_probs={}, 
                  prior = [1,1], binary_rewards = True, config = {}):
    assert num_actions == 2
    read_config.apply_defaults(config)
    match_num_samples = config[read_config.MATCH_NUM_SAMPLES_HEADER]
    smoothing_adder = config[read_config.SMOOTHING_ADDER_HEADER]
    max_weight = config[read_config.MAX_WEIGHT_HEADER]
    ipw_row = {}
    ipws = []
    actions_df = pd.read_csv(actions_infile,skiprows=1)
    max_weights = 0
    if binary_rewards:
        all_models, cache_keys = create_models_binary(actions_df, prior, num_actions)
        ipw_row[PRIOR_MEAN_HEADER] = prior[0] / sum(prior)

    else:
        all_models, cache_keys = create_models_normal(actions_df, prior, num_actions)
        ipw_row[PRIOR_MEAN_HEADER] = prior[0]
    saved_probs = [];
    modified_probs_exist = False
    cache_key = None
    for row, i in zip(actions_df.iterrows(), range(len(all_models[0]) - 1)): # we skip the last model because it has the probabilities for the next sample that would be assigned
        if len(cache_keys) > i:
            cache_key = tuple(cache_keys[i])
            
        # First, look in the file to see if we have a probability we can use - if so, use it
        if False and not np.isnan(row[1][H_ALGO_PROB_BEST_ACTION.format(1)]) and \
            (not match_num_samples or num_samples == row[1][H_ALGO_NUM_TRIALS]): # we can use a cached probability
            probs = [row[1][H_ALGO_PROB_BEST_ACTION.format(1)], row[1][H_ALGO_PROB_BEST_ACTION.format(2)]]
        elif cache_key is not None and cache_key in cached_probs:
            # Next, look in the cach keys
            probs = cached_probs[cache_key]
            modified_probs_exist = True
        else:
            #Finally, need to calculate probability action 1 is better to use for ipw and then add to cache
            cur_models = [models[i] for models in all_models]
            counts = thompson_policy.estimate_probability_condition_assignment(None, num_samples, num_actions, cur_models)
            probs = [count / num_samples for count in counts]
            cached_probs[cache_key] = probs
            modified_probs_exist = True
        
        # Save the unsmoothed probability if asked for and if the probability stored currently isn't present or isn't for the right number of samples
        if config[read_config.SAVE_MODIFIED_ACTIONS_FILE_HEADER]:
            saved_probs.append(probs)
            
        # Perform smoothing on the probability vector
        probs = [(prob * num_samples + smoothing_adder) / (num_samples + smoothing_adder * num_actions) for prob in probs]
        for j in range(len(probs)):
            probs[j] = max(probs[j], 0 + EPSILON_PROB)
            probs[j] = min(probs[j], 1 - EPSILON_PROB)
        
        # Determine which probability is relevant based on which condition this sample was assigned to
        condition_assigned = int(actions_df.iloc[i].loc[H_ALGO_ACTION])
        prob = probs[condition_assigned - 1] # map back to 0 indexing

        weight = 1/prob
        if weight > max_weight:
            max_weights += 1
            weight = max_weight
        ipws.append(weight)
        
#     print("actions file:", actions_infile)
#     print("config says to save:",config[read_config.SAVE_MODIFIED_ACTIONS_FILE_HEADER])
#     print("modified probs exist:", modified_probs_exist)
    if modified_probs_exist and config[read_config.SAVE_MODIFIED_ACTIONS_FILE_HEADER]:
        prob_df = pd.DataFrame.from_records(saved_probs)
        actions_df.loc[:,[H_ALGO_PROB_BEST_ACTION.format(1),H_ALGO_PROB_BEST_ACTION.format(2)]] = prob_df.values
        actions_df[H_ALGO_NUM_TRIALS] = num_samples
        actions_file_first_line = ""
        with open(actions_infile) as infile:
            actions_file_first_line = infile.readline()
        with open(actions_infile,'w') as outfile:
            outfile.write(actions_file_first_line)
        actions_df.to_csv(actions_infile, mode='a', index=False)

    actions_df[IPWS_COL] = ipws
    
    ipw_means = []
    means = []
    ipw_terms = []
    ht_means = []

    for action in range(num_actions):
        cur_action = actions_df.loc[actions_df.loc[:,H_ALGO_ACTION] == (action + 1)]
        ipw_row[NUM_SAMPLES_BY_ACTION_HEADER.format(action + 1)] = cur_action.shape[0]
        if sum(cur_action.loc[:,IPWS_COL]) == 0:
            print("problem")
        if cur_action.shape[0] == 0:
            ipw_mean = float("nan")
            ipw_row[IPW_MEAN_HEADER.format(action + 1)] = ipw_mean
            ipw_means.append(ipw_mean)
            
            ht_mean = float("nan")
            ipw_row[HT_MEAN_HEADER.format(action + 1)] = ht_mean
            ht_means.append(ht_mean)
            
            ipw_terms.append(sum(cur_action.loc[:,IPWS_COL] * cur_action.loc[:,H_ALGO_OBSERVED_REWARD]))
            mean = float("nan")
            ipw_row[MLE_MEAN_HEADER.format(action + 1)] = mean
            means.append(mean)
        else:
            ipw_mean = sum(cur_action.loc[:,IPWS_COL] * cur_action.loc[:,H_ALGO_OBSERVED_REWARD]) / sum(cur_action.loc[:,IPWS_COL])
            ipw_row[IPW_MEAN_HEADER.format(action + 1)] = ipw_mean
            ipw_means.append(ipw_mean)
            
            ht_mean = 1/(len(all_models[0]) - 1) * sum(cur_action.loc[:,IPWS_COL] * cur_action.loc[:,H_ALGO_OBSERVED_REWARD])
            ipw_row[HT_MEAN_HEADER.format(action + 1)] = ht_mean
            ht_means.append(ht_mean)
            
            ipw_terms.append(sum(cur_action.loc[:,IPWS_COL] * cur_action.loc[:,H_ALGO_OBSERVED_REWARD]))
            mean = np.mean(cur_action.loc[:,H_ALGO_OBSERVED_REWARD])
            ipw_row[MLE_MEAN_HEADER.format(action + 1)] = mean
            means.append(mean)
        
    ipw_row[MAX_WEIGHT_COUNT_HEADER] = max_weights
#     print("means:", means)
#     print("ipw_means:", ipw_means)
#     print("ht_means:", ht_means)
# 
#     print("diff in means:", means[0] - means[1])
#     print("diff in ipw_means:", ipw_means[0] - ipw_means[1])
#     print("ipw_terms:", 1/actions_df.shape[0] *(ipw_terms[0]-ipw_terms[1]))
#     print("count of max_weights:", max_weights, "/", len(cache_keys) - 1)
    
    return ipw_row

def main():
    config = {}
    read_config.apply_defaults(config)
    tmpfilename = '/Users/rafferty/banditalgorithms/data/tbb_actions_32_312.csv'

#     tmpfilename = '/Users/rafferty/banditalgorithms/data/tbb_actions_4312_1.csv'
    is_binary = True
    prior = [1,1]
    
#     tmpfilename = '/Users/rafferty/banditalgorithms/data/tng_actions_26_397.csv'
    tmpfilename = '/Users/rafferty/banditalgorithms/data/tng_actions_26_3.csv'
    is_binary = False
    prior = [0,1,1,1]
    
    cached_probs = {}
#     for i in range(5):
    calculate_ipw(tmpfilename, num_samples=5000, num_actions = 2, cached_probs={}, prior = prior, binary_rewards = is_binary, config = config)
#     configurationFile = sys.argv[1]
#     with open(configurationFile) as jsonFile:
#         config = json.load(jsonFile)
#     effect_size = config["effect_size"]
#     n = config["n"]
#     outfile_prefix = config["outfile_directory"] + config["bandit_type_prefix"] + str(effect_size);
#                                                           
#     if effect_size == 0:
#         # Then include the n  in the prefix
#         outfile_prefix += "N" + str(n) 
# #     df = calculate_statistics_from_sims(outfile_directory, num_sims, step_sizes, effect_size, DESIRED_ALPHA)

if __name__=="__main__":
    main()