"""
file: master_comparison_data.py

Takes a master JSON configuration and runs generate_comparison_data
for all simulation sets that the master JSON declares.

Two arguments, one optional: first is config file, second is number
of cores to use (default 40).
"""

import sys
import json
import itertools
import random
from string import Template
from scipy.special import logit, expit
import generate_comparison_data


def genFileNames(simSetup, config):
    structure, generationProcess, interceptProb, coefficientProb = (simOption[0] for simOption in simSetup)
    filePathPrefix = config["filePathPrefix"]
    # Use file path template from config to assign file path
    fileNameTemplate = Template(config["fileNameTemplate"])
    mapping = {"structure": structure, "generationProcess": generationProcess, "interceptProb": interceptProb,
               "coefficientProb": coefficientProb}
    filePath = fileNameTemplate.substitute(mapping)
    fileName = filePath + ".json"
    # Append file names (templates) to the file path
    rewardFile = filePathPrefix + filePath + config["rewardFileExt"]
    outfilePrefix = filePathPrefix + filePath + config["outfilePrefixExt"]
    conditionsToActionsFile = filePathPrefix + filePath + config["conditionsToActionsFileExt"]

    return fileName, rewardFile, outfilePrefix, conditionsToActionsFile


def genConditions(structure, interceptProbs, coefficientProbs):
    conditions = []
    randConditions = {}
    for experiment in range(len(interceptProbs)):
        experimentConditions = []
        randExperimentConditions = {}
        for arm in range(len(interceptProbs[experiment])):
            interceptProb = interceptProbs[experiment][arm]
            coefficientProb = {key : coefficientProbs[key][experiment][arm] for key in coefficientProbs.keys()}
            armConditions, randArmConditions = genCoeffs(structure, interceptProb, coefficientProb)
            experimentConditions.append(armConditions)
            randExperimentConditions = {k : randExperimentConditions[k] + [v] if k in randExperimentConditions
                                        else [v] for k, v in randArmConditions.items()}
        conditions.append(experimentConditions)
        randConditions = {k : randConditions[k] + [v] if k in randConditions
                          else [v] for k, v in randExperimentConditions.items()}

    return conditions, randConditions


def genCoeffs(structure, interceptProb, coefficientProb):
    # Insert intercept into coefficientProb
    coefficientProb["int"] = interceptProb
    # If keyword "all" in coefficientProb, extend to all vars -- TODO: NOT MAINTAINED
    if "all" in coefficientProb:
        for i in range(len(structure)):
            if str(i) not in coefficientProb:
                coefficientProb[str(i)] = coefficientProb["all"]

    # Calculate coefficients from probabilities in coefficientProb
    coeffs = calcCoeffs(coefficientProb, structure)

    # If keyword "rand0" in coefficientProb, handle separately -- TODO: NOT MAINTAINED
    randCoeffs = {}
    if "rand0" in coefficientProb:
        randOptions = []
        j = 0
        while "rand{}".format(j) in coefficientProb:
            randOptions.append(coefficientProb["rand{}".format(j)])
            j += 1
        k = 0
        for randSort in itertools.product(randOptions, repeat=len(structure)):
            randProb = {str(i) : randSort[i] for i in range(len(structure))}
            randProb["int"] = interceptProb
            # Calculate coefficients from probabilities in randProb
            randCoeffs["rand{}".format(k)] = calcCoeffs(randProb, structure)
            k += 1

    return coeffs, randCoeffs


def calcCoeffs(probs, structure):
    coeffs = []
    intercept = calcInterceptFromProb(probs["int"])
    coeffs.append(intercept) # initialize with intercept
    # If a range included, set probs for each in range to match endpoint probs given
    probKeys = list(probs.keys()) # force static from dynamic
    for probKey in probKeys:
        if "-" in probKey:
            varFirst, varLast = tuple(probKey.split("-"))
            varRange = list(range(int(varFirst), int(varLast)+1))
            endpointCoeff = calcCoeffFromProb(probs[probKey], intercept)
            # Distribute coeff total amongst constituent convars
            distCoeff = endpointCoeff / len(varRange)
            distProb = calcProbFromCoeff(distCoeff, intercept)
            for i in varRange:
                probs[str(i)] = distProb
    # Calc for indiv convars
    for conVar in range(len(structure)):  # by 0-index of contextual variable
        varCoeffs = []
        for group in range(structure[conVar]):  # by 0-index of group
            groupCoeff = 0
            # TODO:: only assigns special coefficients to group 1
            if group == 1 and str(conVar) in probs:
                groupProb = probs[str(conVar)]
                groupCoeff = calcCoeffFromProb(groupProb, intercept)
            varCoeffs.append(groupCoeff)
        coeffs.extend(varCoeffs)

    return coeffs


def calcInterceptFromProb(interceptProb):
    return logit(interceptProb)


def calcCoeffFromProb(groupProb, intercept):
    return logit(groupProb) - intercept

def calcProbFromCoeff(groupCoeff, intercept):
    return expit(groupCoeff + intercept)

def adjustGenerationProcess(generationProcess, structure):
    # If keyword "all" in generationProcess, extend to all vars
    if "all" in generationProcess:
        for i in range(len(structure)):
            if str(i) not in generationProcess:
                generationProcess[str(i)] = generationProcess["all"]

    # # If keyword "rand0" in coefficientProb, randomly assign all unassigned vars
    # if "rand0" in generationProcess:
    #     randOptions = []
    #     # Fetch "rand*" options
    #     j = 0
    #     while "rand{}".format(j) in generationProcess:
    #         randOptions.append("rand{}".format(j))
    #         j += 1
    #     # Randomly assign to all unassigned vars
    #     for i in range(len(structure)):
    #         if str(i) not in generationProcess:
    #             generationProcess[str(i)] = random.choice(randOptions)

    return generationProcess


def genSimConfig(simSetup, config):
    structure, generationProcess, interceptProb, coefficientProb = (simOption[1] for simOption in simSetup)
    simConfig = dict()
    simConfig["numTrials"] = config["numTrials"]
    simConfig["numStudents"] = config["numStudents"]
    simConfig["contextualStructure"] = structure
    simConfig["contextualGenerationProcess"] = adjustGenerationProcess(generationProcess, structure)
    simConfig["conditions"], simConfig["randConditions"] = genConditions(structure, interceptProb, coefficientProb)
    simConfig["fileName"], simConfig["rewardFile"], simConfig["outfilePrefix"], simConfig["conditionsToActionsFile"] = \
        genFileNames(simSetup, config)

    return simConfig


def genSimConfigs(config):
    structures = config["structures"].items()
    generationProcesses = config["generationProcesses"].items()
    interceptProbs = config["interceptProbs"].items()
    coefficientProbs = config["coefficientProbs"].items()
    simList = [structures, generationProcesses, interceptProbs, coefficientProbs]
    simSetups = list(itertools.product(*simList))

    simConfigs = []
    for simSetup in simSetups:
        simConfigs.append(genSimConfig(simSetup, config))

    return simConfigs


def main(configFile, nCores=40):
    with open(configFile) as jsonFile:
        config = json.load(jsonFile)
    simConfigs = genSimConfigs(config)
    # Save all configs for use
    relPath = configFile[:configFile.rfind('/') + 1]
    simConfigPath = relPath + config["outputFolder"]
    simConfigFiles = []
    for simConfig in simConfigs:
        simConfigFileName = simConfigPath + simConfig["fileName"]
        with open(simConfigFileName, 'w') as jsonFile:
            json.dump(simConfig, jsonFile)
        simConfigFiles.append(simConfigFileName)
    # Run generate_comparison_data with all configs
    if config.get('run', False):
    	for simConfigFile in simConfigFiles:
        	print(simConfigFile)
        	generate_comparison_data.run(simConfigFile, nCores)


if __name__ == "__main__":
    configFile = sys.argv[1]
    nCores = 40 if len(sys.argv) <= 2 else int(sys.argv[2])
    main(configFile, nCores)