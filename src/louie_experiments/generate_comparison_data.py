'''
Module for developing simulated datasets where contextual variables have an impact on arm choices.
Supports simultaneous factorial experiments - two separate choices made at once.
Currently does not support interactions among contextual variables or interactions between
the experiments.


Created on Apr 16, 2018

@author: rafferty

'''
import numpy as np
import itertools
import sys
import os
import csv
import json
import thompson_policy
import logistic_regression
import multi_model
import random
from get_assistments_contextual_rewards import read_assistments_outcomes_to_config, read_assistments_params_to_config
from functools import partial
from bandit_data_format import HEADER_ACTUALREWARD, HEADER_OPTIMALACTION, HEADER_TRUEPROB
from clean_up import compress_df

# import istarmap  # import to apply patch
from multiprocessing import Pool
# import tqdm
import time


class Experiment:

    def __init__(self, conditions):
        '''
        conditions: iterable of Condition instances
        '''
        self.conditions = conditions

    def getValue(self, contextualVariables, conditionAssignment):
        return self.conditions[conditionAssignment].getValue(contextualVariables)


class Condition:
    def __init__(self, contextualCoefficients):
        '''
        contextualCoefficients: iterable with an intercept at index 0 and each
        subsequent coefficient corresponds to a contextual variable (either with a particular
        dummy coding or one value for continuous)
        '''
        self.coeff = contextualCoefficients

    def getValue(self, contextualVariables):
        return np.inner(self.coeff, [1] + contextualVariables)  # 1 is for the intercept


# class LinearModel:
#
#     def __init__(self, intercept, experiments, contextualStructure):
#         self.intercept = intercept
#         self.experiments = experiments
#         self.contextualStructure = contextualStructure
#
#     def simulateReward(self, contextualVariableValues, conditionVector, noiseModel=np.random.normal):
#         dummyCodedVariableValues = self.contextualStructure.getDummyCodedVersion(contextualVariableValues)
#         outcome = self.intercept
#         for experiment, condition in zip(self.experiments, conditionVector):
#             outcome += experiment.getValue(dummyCodedVariableValues, condition)
#
#         # Add random noise
#         outcome += noiseModel()
#         return outcome


class LogisticModel:

    def __init__(self, intercept, experiments, contextualStructure):
        self.intercept = intercept
        self.experiments = experiments
        self.contextualStructure = contextualStructure

    def getSuccessProbability(self, contextualVariableValues, conditionVector):
        dummyCodedVariableValues = self.contextualStructure.getDummyCodedVersion(contextualVariableValues)
        outcome = self.intercept
        for experiment, condition in zip(self.experiments, conditionVector):
            outcome += experiment.getValue(dummyCodedVariableValues, condition)

        # Calculate probability of success
        exponential = np.exp(outcome)
        successProb = exponential / (1 + exponential)

        return successProb

    def simulateReward(self, contextualVariableValues, conditionVector):
        successProb = self.getSuccessProbability(contextualVariableValues, conditionVector)
        success = np.random.binomial(1, successProb)
        return success


class ContextualStructure:

    def __init__(self, config, dummyAll=True):
        '''
        config is json and must declare:
        contextualStructure: iterable where each entry is a contextual variable.
        entries > 0 indicate a categorical variable with that number of options.
        entries = -1 indicate a continuous variable.

        May also declare: contextualDependencies, contextualGenerationProcess which impact generateContext.
        contextualDependencies: dictionary with string keys corresponding to any variables that are dependent
        on the value of other variables. We require that the original structure be partially ordered such that
        a contextual variable that is dependent on other variables comes later in the structure than those it's
        dependent on. (So variable 0 is always lacking dependencies on other variables.)

        contextualGenerationProcess: dictionary with string keys, where if a variable appears as a key,
        the value specifies the distribution for generating context. (currently only categorical distributions
        are allowed.) If the variable is not specified, its value is generated uniformly at random.  If the variable
        is dependent on another variable, then the keys are of the form "i-v" where i is the index of the variable
        and v is the value. If it's dependent on multiple variables, these are comma separated (e.g., "i-v1,j-v2").

        dummyAll: boolean for whether all groups of a contextual variable should be dummied. If False, group 0 is
        not dummied, is inferred from 0s on all other dummy variables, and is assumed to have coefficients of 0 on
        all conditions. If True, group 0 is dummied, and may have nonzero coefficients. Only applies to discrete
        contextual variables.
        '''
        self.dummyAll = dummyAll
        self.contextualStructure = config["contextualStructure"]
        self.contextualDependencies = {}
        self.contextualGenerationProcess = {}
        if "contextualDependencies" in config:
            self.contextualDependencies = config["contextualDependencies"]

        if "contextualGenerationProcess" in config:
            self.contextualGenerationProcess = config["contextualGenerationProcess"]
            # Normalize distributions so we don't have to do it every time
            for key in self.contextualGenerationProcess:
                if not isinstance(self.contextualGenerationProcess[key], dict):
                    # Have the distribution here
                    self.contextualGenerationProcess[key] = self.__getNormalizedDistribution(
                        self.contextualGenerationProcess[key])
                else:
                    for innerKey in self.contextualGenerationProcess[key]:
                        self.contextualGenerationProcess[key][innerKey] = self.__getNormalizedDistribution(
                            self.contextualGenerationProcess[key][innerKey])

        self.undummifiedVersion = None if not dummyAll else ContextualStructure(config, dummyAll=False)

    def __getNormalizedDistribution(self, dist):
        total = sum(dist)
        normDist = [val / total for val in dist]
        return normDist

    def generateContext(self):
        '''
        If contextualGenerationProcess exists and is non-empty (specified on construction), then generates
        contextual variable values according to the probabilities specified there. Otherwise, generates
        contextual variables uniformly at random.
        '''
        if len(self.contextualGenerationProcess) == 0:
            return self.generateUniformlyAtRandom()
        else:
            variableValues = []
            for varIndex in range(len(self.contextualStructure)):
                generatingDistribution = None
                stringVarIndex = str(varIndex)
                if stringVarIndex not in self.contextualDependencies:
                    # Generate this without dependence on other values
                    if stringVarIndex in self.contextualGenerationProcess:
                        generatingDistribution = self.contextualGenerationProcess[stringVarIndex]
                else:
                    # Need to find the values for ones it's dependent on
                    dependentVars = self.contextualDependencies[stringVarIndex]
                    #                     dependentVars.sort()
                    depValueKey = [dependentVar + "-" + str(variableValues[int(dependentVar)]) for dependentVar in
                                   dependentVars]
                    generatingDistribution = self.contextualGenerationProcess[stringVarIndex][",".join(depValueKey)]

                if generatingDistribution == None:
                    variableValues.append(
                        self.__generateSingleVariableUniformlyAtRandom(self.contextualStructure[varIndex]))
                else:
                    # Assumes a categorical distribution, summing to 1 handled in constructor
                    variableValues.append(np.random.choice(len(generatingDistribution), p=generatingDistribution))
            return variableValues

    def generateUniformlyAtRandom(self):
        '''
        Generates a list of contextual variables (non-dummy-coded) selected uniformly at random from their range.
        If a variable is continuous, assumes the range is [0,1].
        '''
        variableValues = [self.__generateSingleVariableUniformlyAtRandom(varStructure) for varStructure in
                          self.contextualStructure]
        return variableValues

    def __generateSingleVariableUniformlyAtRandom(self, varStructure):
        if varStructure == -1:
            return np.random.uniform(0, 1)
        else:
            return np.random.randint(varStructure)

    def getAllContextualCombinations(self):
        '''
        Returns a list of all possible combinations of contextual variable values.
        Assumes all contextual variables are categorical.
        '''
        if any(np.array(self.contextualStructure) == -1):
            print("Error: can't get all contextual variable combinations if some variables are continuous.")

        return [item for item in itertools.product(*[range(i) for i in self.contextualStructure])]

    def getNumberOfVariables(self):
        return len(self.contextualStructure)

    def getDummyCodedVersion(self, contextVariableValues):
        '''
        Converts from iterable contextVariableValues where categorical variables with
        n options take on values (0,...,n-1) to a dummy coded version where there would
        be n values, all 0/1, and the ith value is 1 for value 1,...,n-1 and no value
        is 1 for the 0th value.
        '''
        dummyMod = 0 if self.dummyAll else 1  # if dummyAll set False, no dummy for group 0
        dummyCodedVariables = []
        for i in range(len(contextVariableValues)):
            if self.contextualStructure[i] == -1:
                # Continuous variable - no dummy coding
                dummyCodedVariables.append(contextVariableValues[i])
            else:
                dummyCoding = np.zeros(self.contextualStructure[i] - dummyMod)
                # all 0 if not dummyMod and group 0; else, (adjusted) group = 1
                if contextVariableValues[i] - dummyMod >= 0:
                    dummyCoding[contextVariableValues[i] - dummyMod] = 1
                dummyCodedVariables.extend(dummyCoding)
        return dummyCodedVariables

    def getContextValueVersion(self, dummyCodedVariables):
        '''
        Converts from iterable dummyCodedValues; inverse operation of getDummyCodedVersion
        above.
        '''
        dummyMod = 0 if self.dummyAll else 1  # if dummyAll set False, no dummy for group 0
        contextVariableValues = []
        j = 0  # dummy variable index
        for i in range(len(self.contextualStructure)):
            if self.contextualStructure[i] == -1:
                # Continuous variable - no dummy coding
                contextVariableValues.append(dummyCodedVariables[j])
                j += 1
            else:
                # one less dummy variable if not dummyAll
                dummyCoding = dummyCodedVariables[j: j + self.contextualStructure[i] - dummyMod]
                # if not dummyAll, group number is 1-indexed (skip 0); if dummyAll, 0-indexed
                contextValue = np.inner(dummyCoding, range(dummyMod, self.contextualStructure[i]))
                contextVariableValues.append(contextValue)
                j += self.contextualStructure[i] - dummyMod
        return contextVariableValues

    def get_context(self, row, includeIntercept=True):
        contextNames = ['contextualVariable' + str(i) for i in range(self.getNumberOfVariables())]
        contextVector = []
        if includeIntercept:
            contextVector.append(1)

        contextualVariableValues = []
        for contextName, i in zip(contextNames, range(len(contextNames))):
            if self.contextualStructure[i] == -1:
                # Continuous variable
                contextualVariableValues.append(float(row[contextName]))
            else:
                contextualVariableValues.append(int(row[contextName]))
        contextVector.extend(self.getDummyCodedVersion(contextualVariableValues))
        return contextVector

    def getUndummifiedVersion(self):
        """
        Returns an alternate version of this ContextualStructure object with the same configuration but with k-1
        dummy variables instead of k dummy variables. None if current instance is dummyAll=False.
        """
        return self.undummifiedVersion


class OutcomesStructure:

    def __init__(self, config, dummyAll=True, __suppressRandomization__=False):
        """
        The following parameters must be present in the config dict:
        (see read_assistments_rewards_with_context() below)
        "rewardDict" - Dict where [action][vector] yields queue of (observed) rewards as list
        "vectorQueue" - Queue of observed context vectors
        "contextualStructure" - List of contextual variables as ints according to their respective number of groups
        "replacement" - False if no replacement, else true
        "numStudents" - Number of random samples to pull if "replacement" above is true
        """
        self.rewardDict = config["rewardDict"]
        self.vectorQueue = config["vectorQueue"]
        self.contextualStructure = config["contextualStructure"]
        self.replacement = None if not config["replacement"] else config["numStudents"]
        self.dummyAll = dummyAll
        if not __suppressRandomization__:
            self.randomize()
        # for original contextual model
        self.undummifiedVersion = None
        if dummyAll:
            # update to randomization & suppress randomization in new object
            newConfig = config
            newConfig["rewardDict"], newConfig["vectorQueue"] = self.rewardDict, self.vectorQueue
            self.undummifiedVersion = OutcomesStructure(newConfig, dummyAll=False, __suppressRandomization__=True)

    def randomize(self):
        '''
        randomizes the order of all queues
        if replacing, randomly samples from each queue until target number of samples is met
        '''
        for action_dict in self.rewardDict.values():
            for vector in action_dict.keys():
                reward_queue = action_dict[vector]
                if self.replacement is None:
                    # if no replacement, shuffle the original queue
                    random.shuffle(reward_queue)
                else:
                    # otherwise, construct a new queue of that many random samples from the original queue
                    reward_queue = [random.choice(reward_queue) for _ in range(self.replacement)]
                    # random.choice used over random.choices for compatibility with Python 3.5-
                action_dict[vector] = reward_queue

        if self.replacement is None:
            # if no replacement, shuffle the original queue
            random.shuffle(self.vectorQueue)
        else:
            # otherwise, construct a new queue of that many random samples from the original queue
            self.vectorQueue = [random.choice(self.vectorQueue) for _ in range(self.replacement)]
            # random.choice used over random.choices for compatibility with Python 3.5-

    def get_context(self, row, includeIntercept=True):
        """
        :param row: row of the simulation; the student's index
        :param includeIntercept: whether to include the intercept in the dummified context
        :return:
        """
        # returns None if no more context available
        if len(self.vectorQueue) == 0:
            return None

        contextVector = []
        if includeIntercept:
            contextVector.append(1)
        contextVector.extend(self.getDummyCodedVersion(self.vectorQueue[row]))

        return contextVector

    def getVectorQueue(self):
        return self.vectorQueue.copy()

    def getRewardDict(self):
        """
        retrieves a deep copy of the instanced reward dict, allowing for independent modification
        :return: dict reward_dict_copy
        """
        reward_dict_copy = dict()
        for action_item in self.rewardDict.items():
            action, action_dict = action_item
            reward_dict_copy[action] = dict()
            for vector_item in action_dict.items():
                vector, vector_queue = vector_item
                reward_dict_copy[action][vector] = vector_queue.copy()

        return reward_dict_copy

    def getDummyCodedVersion(self, contextVariableValues):
        """
        Converts from iterable contextVariableValues where categorical variables with
        n options take on values (0,...,n-1) to a dummy coded version where there would
        be n values, all 0/1, and the ith value is 1 for value 1,...,n-1 and no value
        is 1 for the 0th value.
        """
        dummyMod = 0 if self.dummyAll else 1  # if dummyAll set False, no dummy for group 0
        dummyCodedVariables = []
        for i in range(len(contextVariableValues)):
            if self.contextualStructure[i] == -1:
                # Continuous variable - no dummy coding
                dummyCodedVariables.append(contextVariableValues[i])
            else:
                dummyCoding = np.zeros(self.contextualStructure[i] - dummyMod)
                # all 0 if not dummyMod and group 0; else, (adjusted) group = 1
                if contextVariableValues[i] - dummyMod >= 0:
                    dummyCoding[contextVariableValues[i] - dummyMod] = 1
                dummyCodedVariables.extend(dummyCoding)

        return dummyCodedVariables

    def getUndummifiedVersion(self):
        return self.undummifiedVersion


def generateContextualRewardFile(config, num_trial):
    numStudents = config["numStudents"]
    # Contextual variables: 1 binary, 2 trinary (so with dummy coding, we'll have two coefficients here for each)
    ## 5 total contextual coefficients, plus an intercept, for each condition
    structure = ContextualStructure(config)

    conditions = np.asarray(config["conditions"])
    # Randomly deactivate conditions according to effect probabilities. effectProbs is probability effect exists.
    if "effectProbs" in config:
        effectProbs = config["effectProbs"]
        effects = [[[1 if np.random.random() < prob else 0 for prob in arm] for arm in exp] for exp in effectProbs]
        conditions = conditions * np.asarray(effects)
    numConditions = [len(curConditions) for curConditions in conditions]
    experiments = [Experiment([Condition(coeff) for coeff in curConditions]) for curConditions in conditions]
    model = LogisticModel(0, experiments, structure)
    # Now simulate some data. We'll assume that the contextual variables are distributed
    # uniformly at random.
    conditionVectors = makeConditionVectors(numConditions)
    if not os.path.isfile(config["conditionsToActionsFile"]):
        with open(config["conditionsToActionsFile"], 'w', encoding='utf-8') as out:
            writer = csv.writer(out)
            writer.writerow(["actionNumber", "conditionVector"])
            for v, a in zip(conditionVectors, range(len(conditionVectors))):
                writer.writerow([a + 1, str(v)])

    reward_file = subTypeAndTrial(config["rewardFile"], num_trial)
    with open(reward_file, 'w', encoding='utf-8') as out:
        writer = csv.writer(out)
        writer.writerow(['n'] + ['contextualVariable' + str(i) for i in range(structure.getNumberOfVariables())]
                        + [HEADER_ACTUALREWARD.format(a + 1) for a in range(len(conditionVectors))] + [
                            HEADER_OPTIMALACTION] \
                        + [HEADER_TRUEPROB.format(a + 1) for a in range(len(conditionVectors))])

        for n in range(numStudents):
            contextualVariables = structure.generateContext()
            rewards = [model.simulateReward(contextualVariables, conditionVector) for
                       conditionVector in conditionVectors]

            successProbs = [model.getSuccessProbability(contextualVariables, conditionVector)
                            for conditionVector in conditionVectors]
            optimalActions = np.argwhere(successProbs == np.amax(successProbs)).flatten() + 1
            optimalActionsString = ';'.join(str(index) for index in optimalActions)
            writer.writerow([n] + contextualVariables + rewards + [optimalActionsString] + successProbs)
    return structure


def runThompsonBandit(config, num_trial, bandit_type="ModContextual"):
    '''
    Just for experimenting with running the contextual bandit code.
    bandit_type: type should be one of "ModContextual", "MultiNonContextual", "NonContextual", "Uniform", or "Contextual"
    '''
    # Unpack information from config
    epsilon = 0 if "epsilon" not in config else config["epsilon"]  # Apply epsilon if present in config
    lambda_val = config.get("lambda", 1)  # Get regularization lambda value; default = 1

    reward_file = subTypeAndTrial(config["rewardFile"], num_trial)  # Name with type and trial number
    outfile_bandit_name = bandit_type.replace("Empirical",
                                              "")  # Matching old code behavior, empirical runs keep just the bandit name, not the empirical
    outfile = subTypeAndTrial(config["outfilePrefix"] + "_ExpArms", num_trial, outfile_bandit_name)
    models = None

    # Make contextual structure
    if "Empirical" in bandit_type:
        outcomesStructure = OutcomesStructure(config)
        reward_dict = outcomesStructure.getRewardDict()
        vector_queue = outcomesStructure.getVectorQueue()
        num_actions = len(reward_dict)
        contextual_structure = outcomesStructure  # .contextualStructure
    else:
        conditions = config["conditions"]
        num_conditions = [len(curConditions) for curConditions in conditions]
        condition_vectors = makeConditionVectors(num_conditions)
        num_actions = len(condition_vectors)
        contextual_structure = generateContextualRewardFile(config, num_trial)

    # Make context function - Ignored by morst non-contextual bandits
    get_context = lambda row: contextual_structure.get_context(row, includeIntercept=True)

    # Make Models
    if bandit_type == "NonContextual" or bandit_type == "EmpiricalNonContextual":
        pass  # Nothing special to configure for NonContextual
    elif bandit_type == "Uniform":
        epsilon = 1  # Force uniform sampling, overrides anything set in the config
    elif bandit_type == "Contextual" or bandit_type == "ModContextual" or bandit_type == "EmpiricalModContextual":
        if bandit_type == "EmpiricalModContextual":
            d = evalContextualStructure(outcomesStructure.contextualStructure)
        else:
            dummyAll = bandit_type == "ModContextual"  # Dummify everything for ModContextual but not Contextual
            # Set up contextual structures and models
            d = evalContextualStructure(config["contextualStructure"], dummyAll=dummyAll)

        models = [
            logistic_regression.RLogReg(
                D=d,
                online=bool(config.get('online', 1)),
                Lambda=lambda_val,
                intercept_lambda=config.get('intercept_lambda', None),
                dow_scheduler_name=config.get('dow_scheduler_name', None)
            )
            for _ in range(num_actions)]
        get_context = lambda row: contextual_structure.get_context(row, includeIntercept=True)
    elif bandit_type == "MultiNonContextual":
        models = [multi_model.MultiBetaBern(success=1, failure=1, contextualStructure=contextual_structure)
                  for _ in range(num_actions)]
        get_context = lambda row: contextual_structure.get_context(row, includeIntercept=True)
    else:
        raise Exception("Unknown bandit type: " + bandit_type)

    # Run bandit
    if "Empirical" in bandit_type:
        chosen_actions, models = thompson_policy.calculate_thompson_contextual_bandit_empirical_params(reward_dict,
                                                                                                       vector_queue,
                                                                                                       outcomesStructure.contextualStructure,
                                                                                                       num_actions,
                                                                                                       outfile,
                                                                                                       models,
                                                                                                       get_context=get_context,
                                                                                                       epsilon=epsilon)
    else:
        chosen_actions, models = thompson_policy.calculate_thompson_single_bandit(reward_file,
                                                                                  num_actions,
                                                                                  outfile,
                                                                                  models,
                                                                                  epsilon=epsilon,
                                                                                  get_context=get_context)
    return chosen_actions, models


def subTypeAndTrial(fname, num_trial=None, type=None):
    if "TYPE" in fname:
        fname = fname.replace("TYPE", type)
    if "NUMTRIALS" in fname:
        fname = fname.replace("NUMTRIALS", str(num_trial))
    return fname + ".csv"


def calcNumActions(conditions):
    """
    Calculates the total number of actions from the conditions matrix.
    This is the number experiments multiplied by the number of arms in each experiment.

    :param conditions: from the JSON config. should be a 3-dimensional matrix.
    :return: number of actions inferred from conditions vector
    """
    sum = 0
    for experiment in conditions:
        for arm in experiment:
            sum += 1

    return sum


def getUndummifiedConditions(config):
    conditions = config["conditions"]
    contextualStructure = config["contextualStructure"]
    newConditions = []
    for experiment in conditions:
        newExperiment = []
        for arm in experiment:
            # For each arm's coefficients, rearrange into version for k-1 dummy variables from k
            k = 1
            newArm = [arm[0], ]
            for numVarStates in contextualStructure:
                # adjust for group 0
                coeffMod = 0
                if arm[k] != 0:
                    newArm[0] += arm[k]
                    coeffMod = arm[k]
                coeffs = [coeff - coeffMod for coeff in arm[k + 1: k + numVarStates]]
                newArm.extend(coeffs)
                k += numVarStates
            newExperiment.append(newArm)
        newConditions.append(newExperiment)

    return newConditions


def evalContextualStructure(contextualStructure, dummyAll=True):
    """
    Calculates the number of anticipated values in each condition array.
    This is 1 intercept, plus 2 for each binary contextual variable, 3 for each trinary, etc.
    """
    dummyMod = 0 if dummyAll else 1
    sum = 1  # Start with intercept as first entry in array
    for numVarStates in contextualStructure:
        sum += (numVarStates - dummyMod) if numVarStates != -1 else 1
    return sum


def makeConditionVectors(numConditions):
    '''
    Creates a list of all combinations of condition assignments given the list numConditions
    that lists the number of conditions in each experiment
    '''
    return [vector for vector in itertools.product(*[range(x) for x in numConditions])]


def makeConditionVectorsFromConfig(config):
    '''
    Creates a list of all combinations of condition assignments given the list numConditions
    that lists the number of conditions in each experiment
    '''
    conditions = config["conditions"]
    numConditions = [len(curConditions) for curConditions in conditions]
    return [vector for vector in itertools.product(*[range(x) for x in numConditions])]


def loadConfiguration(configurationFile):
    '''
    Returns the JSON object stored in configuration file.
    Used to setup the structure of the generating reward function.
    '''
    with open(configurationFile) as jsonFile:
        config = json.load(jsonFile)

    for key in config:
        if isinstance(config[key], str) and '/' in config[key]:
            if '~' in config[key]:
                config[key] = os.path.expanduser(config[key])
            os.makedirs(os.path.dirname(config[key]), exist_ok=True)
        if "file" in key.lower():
            config[key] = config[key].replace("NUMSTUDENTS", str(config["numStudents"]))
            config[key] = config[key] + ".csv"

    return config


def main(config, num_trial, seed=None):
    random.seed(seed)
    np.random.seed(seed)
    # if num_trial % 10 == 0:
    #     print(".", end='')
    if config.get("simType", "").lower() == "outcomes":
        # Read in ASSISTments data to config
        read_assistments_outcomes_to_config(config)

        # Run bandits for EmpiricalModContextual and EmpiricalNonContextual
        runThompsonBandit(config, num_trial, bandit_type="EmpiricalModContextual")
        runThompsonBandit(config, num_trial, bandit_type="EmpiricalNonContextual")
    else:
        # three options: parameters based on assistments config, standard sim type (main, crossover, etc), or
        # generate conditions randomly
        if ("simType" not in config or config["simType"].lower() == "standard") and \
                ("randConditions" in config and len(config["randConditions"]) > 0):
            config["conditions"] = random.choice(list(config["randConditions"].values()))
        elif config.get("simType", "").lower() == "parameters":
            read_assistments_params_to_config(config)

        # Run all bandits
        # runThompsonBandit(config, num_trial, bandit_type="Contextual")
        runThompsonBandit(config, num_trial, bandit_type="ModContextual")
        # runThompsonBandit(config, num_trial,  bandit_type="MultiNonContextual")
        runThompsonBandit(config, num_trial, bandit_type="NonContextual")
        runThompsonBandit(config, num_trial, "Uniform")


def run(configFile, nCores=20, additional_msg=None):
    """
    additional_msg (str): an additional message that you might want to print per iteration
        For an example of this, see generate_comparison_data_ASSISTments_all.py.
        I've placed it as the third argument so no existing code should be affected.
    """
    config = loadConfiguration(configFile)

    # This is commented out because this is not parallelized.
    # Previously, we had this and [i for i in range(1, config["numTrials"])] later on.
    # Now, we have this commented out and [i for i in range(0, config["numTrials"])] later on.

    # main(config, 0)

    if additional_msg is None:
        # configFile.split('/')[-1] removes the directory and keeps the file name
        print(f"========== Running {configFile.split('/')[-1]} ==========")
    else:
        print(f"========== Running {configFile.split('/')[-1]} | {additional_msg} ==========")

    start = time.time()

    with Pool(nCores) as p:
        iterable = zip(*[
            itertools.repeat(config),
            [i for i in range(0, config["numTrials"])],
            np.random.randint(2 ** 32 - 1, size=config["numTrials"])
        ])

        p.starmap(main, iterable)  # the old code

        # # the new code below now offers a progress bar
        # for _ in tqdm.tqdm(p.istarmap(main, iterable), total=config["numTrials"]):
        #     pass

    end = time.time()
    print(f'Duration: {round((end - start) / 60, 2)} minutes')

    print("compressing")
    compress_df(os.path.dirname(config["outfilePrefix"]),
                keywords=["ModContextual", "NonContextual", "Uniform"],
                delete_csv=True, retain_csv=5, remove_reward_csv=True)
    # please see parquet demo.ipynb in google team drive/MAB for Experimentation/Winter 2021
    # https://colab.research.google.com/drive/1iA0YeUoG2PlqRG6wujJ9AM1FySRe9-rN?usp=sharing

if __name__ == "__main__":
    configFile = sys.argv[1]
    nCores = 20 if len(sys.argv) <= 2 else int(sys.argv[2])
    run(configFile, nCores)
