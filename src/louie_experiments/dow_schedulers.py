# import unittest


class Pyramidal:

    """Buggy version corrected. Should be used with offline logistic regression."""

    def __init__(self):
        self.num_ts_elapsed = 1  # number of time-steps elapsed

        # The reason why num_ts_elapsed is set to 1 is because, for any learning to happen at all,
        # we must have at least one data point, which occurs AFTER the first time-step.

    def get_new_dow(self):
        dow = self.num_ts_elapsed / 2 + 1 / 2
        self.num_ts_elapsed += 1
        return dow


# ========== map schedulers' name to their classes ==========

dict_of_schedulers = {
    'pyramidal': Pyramidal
}

# ========== unit tests for schedulers =========


# class TestPyramidalDecay(unittest.TestCase):
#
#     def test_non_increasing(self):
#
#         scheduler = PyramidalDecay(initial_lambda=50, baseline_lambda=1)
#
#         prev_lamb = None
#         for i in range(1000):
#             lamb = scheduler.lamb
#             if prev_lamb is None:
#                 prev_lamb = lamb
#             else:
#                 self.assertTrue(prev_lamb >= lamb)
#
#     def test_correct(self):
#
#         scheduler = PyramidalDecay(initial_lambda=1000, baseline_lambda=0)
#
#         for i in range(1, 1001):  # 1st to 1000th students (inclusive)
#             _ = scheduler.lamb
#         correct_ans = scheduler.initial_lambda / (scheduler.num_ts_elapsed / 2 + 1 / 2)
#
#         self.assertTrue(scheduler.num_ts_elapsed == 1001)  # When num_ts_elapsed == 1001, it is not actually used.
#         self.assertTrue(correct_ans == scheduler.lamb)
#
#
# if __name__ == "__main__":
#     unittest.main()