#!/bin/bash
numSims=500
effectSizesT=(0.2 0.5 0.8);
effectSizesB=(0.1 0.3 0.5);


arrayLength=${#effectSizesT[@]};
for ((i=0; i<$arrayLength; i++)); do
    curEffectSize=${effectSizesT[$i]}
    directoryName="ngHighArms"$curEffectSize;
    echo $directoryName
    mkdir $directoryName
    python3 ~/banditalgorithms/src/louie_experiments/run_effect_size_simulations.py 1.0 0.5 $curEffectSize $numSims $directoryName 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
    echo $!
done

arrayLength=${#effectSizesT[@]};
for ((i=0; i<$arrayLength; i++)); do
    curEffectSize=${effectSizesT[$i]}
    directoryName="ngLowArms"$curEffectSize;
    echo $directoryName
    mkdir $directoryName
    python3 ~/banditalgorithms/src/louie_experiments/run_effect_size_simulations.py -0.5 -1.0 $curEffectSize $numSims $directoryName 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
    echo $!
done



arrayLength=${#effectSizesB[@]};
for ((i=0; i<$arrayLength; i++)); do
	curEffectSize=${effectSizesB[$i]}
	directoryName="bbHighArms"$curEffectSize;
	echo $directoryName
	mkdir $directoryName
    python3 ~/banditalgorithms/src/louie_experiments/run_effect_size_simulations_beta.py $curEffectSize $numSims $directoryName "Thompson" "armsHigh" 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
    echo $!
done

arrayLength=${#effectSizesB[@]};
for ((i=0; i<$arrayLength; i++)); do
	curEffectSize=${effectSizesB[$i]}
	directoryName="bbLowArms"$curEffectSize;
	echo $directoryName
	mkdir $directoryName
    python3 ~/banditalgorithms/src/louie_experiments/run_effect_size_simulations_beta.py $curEffectSize $numSims $directoryName "Thompson" "armsLow" 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
    echo $!
done

