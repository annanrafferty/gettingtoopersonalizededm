import os
import argparse
import json

# ========== utility functions ==========

def listdir_advanced(dir_name: str) -> list:
    for name in os.listdir(dir_name):
        if not name.startswith('.'):  # no hidden directories
            yield name, os.path.join(dir_name, name)  # give complete path, too

def dissect_path_within_config(path: str) -> dict:
    """Dissect either config['rewardFiles'], config['outfilePrefix'] or config['conditionsToActionsFile']."""

    # E.g., /disk2/contextualBanditSimsNoEffect/ExperimentData/complete/Outcomes/lambda_1/ASSISTments_226210_C1_N1k/CompareBanditRewardFile_NNUMSTUDENTS_TNUMTRIALS
    # Index:   0              1                       2            3        4       5                 6                                    7

    values = path.split('/')[1:]  # the first one would be the empty string ""
    organized_values = {
        'rewardScheme': values[3],
        'simType': values[4].lower(),
        'lambda': int(values[5].split('_')[-1]),
        'experimentId': int(values[6].split('_')[1]),
        'finalDirName': values[7]
    }
    return organized_values

def shorten(simType: str) -> str:
    """
    This unifies 'parameters' and 'params' as two ways of denoting the same simType.
    This function does not change 'outcomes'.
    """
    return simType[:4] if simType.startswith('param') else simType

# ========== main code ==========

arg_parser = argparse.ArgumentParser()
arg_parser.add_argument('--exp_spec_dir', help='See comments in createASSISTmentConfigs_reg.sh.')
args = arg_parser.parse_args()


for dir_name1, dir_path1 in listdir_advanced(args.exp_spec_dir):

    # e.g.,
    # dir_name1: OutcomesConfigsLambda1
    # dir_path1: ASSISTmentsRegConfigs/OutcomesConfigsLambda1

    dir_name_claimed_simType = dir_name1.split('Configs')[0].lower()  # e.g., "outcomes"
    dir_name_claimed_lambda = int(dir_name1.split('Lambda')[-1])  # e.g., 1

    for dir_name2, dir_path2 in listdir_advanced(dir_path1):

        # e.g.,
        # dir_name2: complete
        # dir_path2: ASSISTmentsRegConfigs/OutcomesConfigsLambda1/complete

        dir_name_claimed_rewardScheme = dir_name2  # e.g., "complete"

        for config_name, config_path in listdir_advanced(dir_path2):

            # e.g.,
            # config_name: ASSISTmentsOutcomesConfig226210Lambda1.json
            # config_path: ASSISTmentsRegConfigs/OutcomesConfigsLambda1/complete/ASSISTmentsOutcomesConfig226210Lambda1.json

            config_name_claimed_simType = config_name.split('Config')[0].split('ments')[-1].lower()  # e.g., "outcomes"
            config_name_claimed_experimentId = int(config_name.split('.')[0].split('Config')[-1])  # e.g., 226210
            # config_name_claimed_lambda = int(config_name.split('Lambda')[-1].split('.')[0])  # e.g., 1

            with open(config_path, 'r') as json_f:
                config = json.load(json_f)

            rewardFile_claimed_values = dissect_path_within_config(config['rewardFile'])
            outfilePrefix_claimed_values = dissect_path_within_config(config['outfilePrefix'])
            conditionsToActionsFile_values = dissect_path_within_config(config['conditionsToActionsFile'])

            assert shorten(dir_name_claimed_simType) == \
                   shorten(config_name_claimed_simType) == \
                   shorten(config['simType']) == \
                   shorten(rewardFile_claimed_values['simType']) == \
                   shorten(outfilePrefix_claimed_values['simType']) == \
                   shorten(conditionsToActionsFile_values['simType'])

            # rewardScheme test
            assert dir_name_claimed_rewardScheme == \
                   config['rewardScheme'] == \
                   rewardFile_claimed_values['rewardScheme'] == \
                   outfilePrefix_claimed_values['rewardScheme'] == \
                   conditionsToActionsFile_values['rewardScheme']

            # experimentId test
            assert config_name_claimed_experimentId == \
                   config['experimentId'] == \
                   rewardFile_claimed_values['experimentId'] == \
                   outfilePrefix_claimed_values['experimentId'] == \
                   conditionsToActionsFile_values['experimentId']

            # lambda test
            assert dir_name_claimed_lambda == \
                   config['lambda'] == \
                   int(config['lambdaString']) == \
                   rewardFile_claimed_values['lambda'] == \
                   outfilePrefix_claimed_values['lambda'] == \
                   conditionsToActionsFile_values['lambda']

            # config_name_claimed_lambda == \

            # path name test
            assert rewardFile_claimed_values['finalDirName'] == "CompareBanditRewardFile_NNUMSTUDENTS_TNUMTRIALS"
            assert outfilePrefix_claimed_values['finalDirName'] == "CompareBanditActionsOut_TYPE_NNUMSTUDENTS_TNUMTRIALS"
            assert conditionsToActionsFile_values['finalDirName'] == "CompareBanditConditionsToActions_NNUMSTUDENTS"
