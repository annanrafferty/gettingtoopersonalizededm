from matplotlib import pyplot as plt
from cycler import cycler
import numpy as np
import sys

if len(sys.argv) > 1:
    filename = sys.argv[1]
else:
    filename = '/Users/rafferty/banditalgorithms/data/sim25Aug2016/steps600switch450Graphs/average-true-t450cor-1.csv'
labels = np.genfromtxt(filename, delimiter=',', usecols=0, dtype=str)
raw_data = np.genfromtxt(filename, delimiter=',')[:,1:]
data = {label: row for label, row in zip(labels, raw_data)}


dataset = []
for alg in data:
    dataset.append(data[alg])
dataset = np.array(dataset)


colors = ['r', 'g', 'b', 'y']
linestyles = ['-', '--', ':', '-.']
linewidths = [1, 1, 3, 3]

color_cycler_list = []
for c in colors:
    color_cycler_list += [c]*len(linestyles)
    
linestyles_list = linestyles*len(colors)
linewidths_list = linewidths*len(colors)

plt.rc('axes', prop_cycle=(cycler('color', color_cycler_list) +
                           cycler('linestyle', linestyles_list) +
                           cycler('linewidth', linewidths_list)))
plt.plot(dataset.T)
plt.legend(data.keys(), loc='best')
plt.show()