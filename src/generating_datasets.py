import sys
import random
import csv
import numpy

#TODO: OUTPUT THE ACTIONS AND REWARDS TO THE CORRECT COLUMNS

inputFile = 'simulated_data_files_input.csv'

#baseline proportions for the 3 mooclets. needs to be changed
#response rates - NOT weights
baselineProportions = [[0.6,0.3,0.3], [0.3,0.2,0.5], [0.3,0.2,0.5]]



"""
Input: A 2D vector that has the response proportions for particular MOOClets. (e.g. [0.3, 0.2, 0.5] )

Output: A simulated data file in the right format for Sarah's, our code for algoritms to run on.
However, it will generate the list of rewards at each time step just based on the proportions that are inputted
into the function.

"""

#source is the input file (in this case simply used to get column headers for output file
#respprop is the n-dimensional vector of response proportions
def generateGroundTruth(source, respProp, dest='generate_selected_truth_policy.csv'):
    #shouldn't need a number of mooclets or actions
    #numActions = 3
    #numMooclets = 3
    
    #turn the proportions into weights
    baselineWeights = [[prop/sum(mooclet) for prop in mooclet] for mooclet in respProp]
    
    with open(source, newline='') as inf, open(dest, 'w', newline='') as outf:
        reader = csv.DictReader(inf)
        fieldNamesOut = reader.fieldnames
        # #output the conditions chosen
        # fieldNamesOut.append('MOOClet1')
        # fieldNamesOut.append('MOOClet2')
        # fieldNamesOut.append('MOOClet3')
        # #output our samples drawn
        fieldNamesOut.append('RewardMOOClet1')
        fieldNamesOut.append('RewardMOOClet2')
        fieldNamesOut.append('RewardMOOClet3')
        

        
        writer = csv.DictWriter(outf, fieldnames=fieldNamesOut)
        writer.writeheader()
        sampleNumber = 0
        for row in reader:
            sampleNumber += 1
            
            #get the user vars
            ageQuartile = 0
            # #user 0 instead of -1 for age quartiles
            # if ageQuartile==-1:
            #   ageQuartile=0;
            
            nDaysAct = 0
            
                
            #choose an action based on proportions
            actions = []
            for i in range(len(baselineWeights)):
                rand = random.random()
                sumprob = 0
                action = None
                for j in range(len(baselineWeights[i])):
                    sumprob += baselineWeights[i][j]
                    if rand <= sumprob:
                        action = j
                        break
                actions.append(action)
                
            
            mooclet1Acond = [0,0,0]
            mooclet1Acond[actions[0]] = 1
            mooclet2Bcond = [0,0,0]
            mooclet2Bcond[actions[1]] = 1
            mooclet3Ccond = [0,0,0]
            mooclet3Ccond[actions[2]] = 1
            # # get reward signals
            # rewards = []
            # for i in range(len(actions)):
            #     characterToAppend = chr(ord('A') + i)
            #     rewards.append(int(row['MOOClet' + str(i+1) + characterToAppend + str(actions[i]+1)]))
                


            #write out some of the inputs, which versions we chose, samples
            writer.writerow({'SampleNumber' : sampleNumber, 'agequartilesUSER': ageQuartile, 'ndaysactUSER' : nDaysAct,
             'MOOClet1A1': mooclet1Acond[0], 'MOOClet1A2': mooclet1Acond[1], 'MOOClet1A3': mooclet1Acond[2], 'MOOClet2B1': mooclet2Bcond[0], 'MOOClet2B2': mooclet2Bcond[1],
             'MOOClet2B3': mooclet2Bcond[2], 'MOOClet3C1': mooclet3Ccond[0], 'MOOClet3C2': mooclet3Ccond[1], 'MOOClet3C3': mooclet3Ccond[2],
             'MOOClet1A1PROB' : respProp[0][0], 'MOOClet1A2PROB' : respProp[0][1], 'MOOClet1A3PROB' : respProp[0][2], 'MOOClet2B1PROB' : respProp[1][0],
             'MOOClet2B2PROB' : respProp[1][1], 'MOOClet2B3PROB' : respProp[1][2], 'MOOClet3C1PROB' : respProp[2][0], 'MOOClet3C2PROB' : respProp[2][1], 
             'MOOClet3C3PROB' : respProp[2][2]})
"""
optional parameters for actually choosing a condition - not used here
'MOOClet1A1': mooclet1Acond[0], 'MOOClet1A2': mooclet1Acond[1], 'MOOClet1A3': mooclet1Acond[2], 'MOOClet2B1': mooclet2Bcond[0], 'MOOClet2B2': mooclet2Bcond[1],
             'MOOClet2B3': mooclet2Bcond[2], 'MOOClet3C1': mooclet3Ccond[0], 'MOOClet3C2': mooclet3Ccond[1], 'MOOClet3C3': mooclet3Ccond[2], , 'RewardMOOClet1' : rewards[0], 'RewardMOOClet2' : rewards[1], 'RewardMOOClet3' : rewards[2]
             """

def main():
  if len(sys.argv) == 3: 
    generateGroundTruth(sys.argv[1], sys.argv[2])
  elif len(sys.argv) == 4:
      generateGroundTruth(sys.argv[1], sys.argv[2], sys.argv[3])
  else:
    generateGroundTruth('simulated_data_files_input.csv', baselineProportions)

if __name__ == "__main__":
  main()