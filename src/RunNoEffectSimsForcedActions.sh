#!/bin/bash
numSims=500
variancesT=(5.0 2.0 1.25); # Note: we're not crossing nsT and variancesT because variancesT is actual variance with ES with that num steps
nsT=(394 64 26);
nsB=(785 88 32);
forcedActionParam=0.01;


arrayLength=${#variancesT[@]};
for ((i=0; i<$arrayLength; i++)); do
    curVariance=${variancesT[$i]}
    curN=${nsT[$i]}
    directoryName="ngEqualMeansUniform"$curVariance;
    echo $directoryName
    mkdir $directoryName
    python3 ~/banditalgorithms/src/louie_experiments/run_effect_size_simulations.py 0.0 0.0 $curVariance $numSims $directoryName "uniform" $curN "forceActions,"$forcedActionParam 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
    echo $!
done

arrayLength=${#variancesT[@]};
for ((i=0; i<$arrayLength; i++)); do
    curVariance=${variancesT[$i]}
    curN=${nsT[$i]}
    directoryName="ngEqualMeansEqualPrior"$curVariance;
    echo $directoryName
    mkdir $directoryName
    python3 ~/banditalgorithms/src/louie_experiments/run_effect_size_simulations.py 0.0 0.0 $curVariance $numSims $directoryName "Thompson" $curN "forceActions,"$forcedActionParam 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
    echo $!
done

wait

arrayLength=${#variancesT[@]};
for ((i=0; i<$arrayLength; i++)); do
    curVariance=${variancesT[$i]}
    curN=${nsT[$i]}
    directoryName="ngEqualMeansArmsHigh"$curVariance;
    echo $directoryName
    mkdir $directoryName
    python3 ~/banditalgorithms/src/louie_experiments/run_effect_size_simulations.py 0.5 0.5 $curVariance $numSims $directoryName "Thompson" $curN "forceActions,"$forcedActionParam 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
    echo $!
done

arrayLength=${#variancesT[@]};
for ((i=0; i<$arrayLength; i++)); do
    curVariance=${variancesT[$i]}
    curN=${nsT[$i]}
    directoryName="ngEqualMeansArmsLow"$curVariance;
    echo $directoryName
    mkdir $directoryName
    python3 ~/banditalgorithms/src/louie_experiments/run_effect_size_simulations.py -0.5 -0.5 $curVariance $numSims $directoryName "Thompson" $curN "forceActions,"$forcedActionParam 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
    echo $!
done

wait

arrayLength=${#nsB[@]};
for ((i=0; i<$arrayLength; i++)); do
    curN=${nsB[$i]}
    directoryName="bbEqualMeansUniform"$curN;
    echo $directoryName
    mkdir $directoryName
    python3 ~/banditalgorithms/src/louie_experiments/run_effect_size_simulations_beta.py 0.5,0.5 $numSims $directoryName "uniform" "armsEqual" $curN "forceActions,"$forcedActionParam 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
    echo $!
done

arrayLength=${#nsB[@]};
for ((i=0; i<$arrayLength; i++)); do
    curN=${nsB[$i]}
    directoryName="bbEqualMeansEqualPrior"$curN;
    echo $directoryName
    mkdir $directoryName
    python3 ~/banditalgorithms/src/louie_experiments/run_effect_size_simulations_beta.py 0.5,0.5 $numSims $directoryName "Thompson" "armsEqual" $curN "forceActions,"$forcedActionParam 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
    echo $!
done

wait

arrayLength=${#nsB[@]};
for ((i=0; i<$arrayLength; i++)); do
    curN=${nsB[$i]}
    directoryName="bbEqualMeansArmsHigh"$curN;
    echo $directoryName
    mkdir $directoryName
    python3 ~/banditalgorithms/src/louie_experiments/run_effect_size_simulations_beta.py 0.5,0.5 $numSims $directoryName "Thompson" "armsHigh" $curN "forceActions,"$forcedActionParam 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
    echo $!
done

arrayLength=${#nsB[@]};
for ((i=0; i<$arrayLength; i++)); do
    curN=${nsB[$i]}
    directoryName="bbEqualMeansArmsLow"$curN;
    echo $directoryName
    mkdir $directoryName
    python3 ~/banditalgorithms/src/louie_experiments/run_effect_size_simulations_beta.py 0.5,0.5 $numSims $directoryName "Thompson" "armsLow" $curN "forceActions,"$forcedActionParam 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
    echo $!
done

