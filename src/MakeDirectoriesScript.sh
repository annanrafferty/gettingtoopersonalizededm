# These are the directories that need to be created before running driver_guassian_rewards_two_bandits.py


mkdir imm
mkdir imm/random
mkdir imm/thompson
mkdir imm/rel_thompson
mkdir imm/rand_thompson
mkdir imm/obl_rand_thompson
mkdir imm/epsilon
mkdir imm/ucb1
mkdir imm/hist_ucb1
mkdir imm/var_hist_ucb1
mkdir imm/rand_ucb1/
mkdir imm/obl_rand_ucb1
mkdir imm/queue_thompson

mkdir imm/input

mkdir true
mkdir true/random
mkdir true/thompson
mkdir true/rel_thompson
mkdir true/rand_thompson
mkdir true/obl_rand_thompson
mkdir true/epsilon
mkdir true/ucb1
mkdir true/hist_ucb1
mkdir true/var_hist_ucb1
mkdir true/rand_ucb1/
mkdir true/obl_rand_ucb1
mkdir true/queue_thompson

mkdir true/input

mkdir graphs