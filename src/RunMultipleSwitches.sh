#!/bin/bash
initialSteps=(60 60 120 120 240 240 240);
switchSteps=(30 45 60 90 60 120 180);
arrayLength=${#initialSteps[@]};
n=1000
for ((i=0; i<$arrayLength; i++)); do
	curSteps=${initialSteps[$i]}
	curSwitch=${switchSteps[$i]}
	
	# directoryName="steps"$curSteps"switch"$curSwitch;
#     echo $directoryName
#     mkdir $directoryName
#     cd $directoryName
#     ~/banditalgorithms/src/MakeDirectoriesScript.sh
#     python3 ~/banditalgorithms/src/louie_experiments/driver_gaussian_rewards_two_bandits.py -t $curSteps -s $curSwitch -n $n -w False &> outFileMVN.log &
#     cd ..
#     echo "Finished $curSteps switch $curSwitch"
	directoryName="steps"$curSteps"switch"$curSwitch"twoFactor";
	echo $directoryName
        mkdir $directoryName
        cd $directoryName
        ~/banditalgorithms/src/MakeDirectoriesScript.sh
        python3 ~/banditalgorithms/src/louie_experiments/driver_gaussian_rewards_two_bandits.py -t $curSteps -s $curSwitch -n $n -w False --twoFactor True &> outFileTwoFactor.log &
        cd ..
	echo "Finished $curSteps switch $curSwitch twoFactor"
done

# mkdir steps120switch30
# cd steps120switch30
# ~/banditalgorithms/src/MakeDirectoriesScript.sh
# python3 ~/banditalgorithms/src/louie_experiments/driver_gaussian_rewards_two_bandits.py 120 30 1000
# cd ..
# echo "Finished 120 switch 30"
#
#
# mkdir steps120switch60
# cd steps120switch60
# ~/banditalgorithms/src/MakeDirectoriesScript.sh
# python3 ~/banditalgorithms/src/louie_experiments/driver_gaussian_rewards_two_bandits.py 120 60 1000
# cd ..
# echo "Finished 120 switch 60"
#
# mkdir steps120switch90
# cd steps120switch90
# ~/banditalgorithms/src/MakeDirectoriesScript.sh
# python3 ~/banditalgorithms/src/louie_experiments/driver_gaussian_rewards_two_bandits.py 120 90 1000
# cd ..
# echo "Finished 120 switch 90"
#
#
# mkdir steps240switch120
# cd steps240switch120
# ~/banditalgorithms/src/MakeDirectoriesScript.sh
# python3 ~/banditalgorithms/src/louie_experiments/driver_gaussian_rewards_two_bandits.py 240 120 1000
# cd..
# echo "Finished 240 switch 120"
#
#
# mkdir steps240switch60
# cd steps240switch60
# ~/banditalgorithms/src/MakeDirectoriesScript.sh
# python3 ~/banditalgorithms/src/louie_experiments/driver_gaussian_rewards_two_bandits.py 240 60 1000
# cd ..
# echo "Finished 240 switch 60"
#
#
#
# mkdir steps1200switch600
# cd steps1200switch600
# ~/banditalgorithms/src/MakeDirectoriesScript.sh
# python3 ~/banditalgorithms/src/louie_experiments/driver_gaussian_rewards_two_bandits.py 1200 600 1000
# cd ..
# echo "Finished 1200 switch 600"
