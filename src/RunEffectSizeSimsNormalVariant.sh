#!/bin/bash
numSims=500
effectSizesT=(0.2 0.5 0.8);
effectSizesB=(0.1 0.3 0.5);
# muValues=(-1.5 -1 0 1 1.5)
muValues=(-1 0 1);

ipwConfigFile="/home/arafferty/banditAlgIPW/src/IPWConfig.json";
pythonScriptDirectory="/home/arafferty/banditAlgIPW/src/louie_experiments"

arrayLength=${#effectSizesT[@]};
for ((i=0; i<$arrayLength; i++)); do
    for ((j=0; j<${#muValues[@]}; j++)); do
        curEffectSize=${effectSizesT[$i]}
        curMu=${muValues[$j]}
        directoryName="ngArmsMu"$curMu"ES"$curEffectSize;
        echo $directoryName
        mkdir $directoryName
        python3 $pythonScriptDirectory/run_effect_size_simulations.py 0.5,$curMu -0.5 $curEffectSize $numSims $directoryName $ipwConfigFile 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
        echo $!
    done
done

# arrayLength=${#effectSizesT[@]};
# for ((i=0; i<$arrayLength; i++)); do
#     curEffectSize=${effectSizesT[$i]}
#     directoryName="ngUniform"$curEffectSize;
#     echo $directoryName
#     mkdir $directoryName
#     python3 ~/banditalgorithms/src/louie_experiments/run_effect_size_simulations.py 0.5 -0.5 $curEffectSize $numSims $directoryName "uniform" 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
#     echo $!
# done

# arrayLength=${#effectSizesT[@]};
# for ((i=0; i<$arrayLength; i++)); do
#     curEffectSize=${effectSizesT[$i]}
#     directoryName="ngLowArms"$curEffectSize;
#     echo $directoryName
#     mkdir $directoryName
#     python3 ~/banditalgorithms/src/louie_experiments/run_effect_size_simulations.py -0.5 -1.0 $curEffectSize $numSims $directoryName 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
#     echo $!
# done



# arrayLength=${#effectSizesB[@]};
# for ((i=0; i<$arrayLength; i++)); do
#     curEffectSize=${effectSizesB[$i]}
#     directoryName="bbHighArms"$curEffectSize;
#     echo $directoryName
#     mkdir $directoryName
#     python3 ~/banditalgorithms/src/louie_experiments/run_effect_size_simulations_beta.py $curEffectSize $numSims $directoryName "Thompson" "armsHigh" 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
#     echo $!
# done
#
# arrayLength=${#effectSizesB[@]};
# for ((i=0; i<$arrayLength; i++)); do
#     curEffectSize=${effectSizesB[$i]}
#     directoryName="bbLowArms"$curEffectSize;
#     echo $directoryName
#     mkdir $directoryName
#     python3 ~/banditalgorithms/src/louie_experiments/run_effect_size_simulations_beta.py $curEffectSize $numSims $directoryName "Thompson" "armsLow" 2> $directoryName"/errorOutput.log" > $directoryName"/output.log" &
#     echo $!
# done

