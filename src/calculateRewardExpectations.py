"""
Module for calculating expected and actual reward based on data. 
Useful functionality:
-- Take in a file with data points(context vectors/opportunities to take actions)
   and probabilities of responses and output expected reward
   probabilities from each Mooclet if highest reward action always taken
-- Take in a file with data points(context vectors/opportunities to take actions)
   and probabilities of responses as well as the fixed sampled response and output
   actual reward probability from each Mooclet if highest reward action always taken
-- Take in a file with data points(context vectors/opportunities to take actions)
   and probabilities of responses as well as the fixed sampled response and a file
   with the actual policy from a particular algorithm and calculate whether the best 
   action was chosen at each point, the total reward from each Mooclet, and the
   average difference in reward probability between chosen and actual actions.
"""
import sys
import os
import pandas
import random
import math
import numpy
import csv
import RLCodeforEmailStudyANR as thompson
import randomPolicy

def calculateOptimalActionsFromSimulatedDataFile(simulatedDataFile, moocletName, numActions):
    df = pandas.read_csv(simulatedDataFile)
    firstProbabilityIndex = df.columns.get_loc(getMoocletColumnName(moocletName, 1)+'PROB')
    maxRewardDf = df.ix[:,firstProbabilityIndex:(firstProbabilityIndex + numActions)].max(axis=1)
    optimalActionIndexDf = df.ix[:,firstProbabilityIndex:(firstProbabilityIndex + numActions)].idxmax(axis=1)
    return maxRewardDf, optimalActionIndexDf

def getMoocletColumnName(moocletName, action):
    characterToAppend = chr(ord('A') + int(moocletName[-1]) -1)
    return moocletName + characterToAppend + str(action) 

def calculateExpectedReward(simulatedDataFile, moocletName, numActions):
    maxRewardDf, optimalActionIndexDf = calculateOptimalActionsFromSimulatedDataFile(simulatedDataFile, moocletName, numActions)
    # expected reward is just average over the maxes
    return maxRewardDf.mean()
    
def calculateRewardBasedOnOptimalActions(simulatedDataFile, moocletName, numActions):
    maxRewardDf, optimalActionIndexDf = calculateOptimalActionsFromSimulatedDataFile(simulatedDataFile, moocletName, numActions)
    df = pandas.read_csv(simulatedDataFile)
    reward = 0
    # this isn't efficient, but not sure on efficient way and just want it calculated...
    for i in range(len(df)):
        bestAction = optimalActionIndexDf[i] # this gives a column name
        curReward = df.loc[i,[bestAction[0:-4]]][0]# chop off the last 4 characters which are PROB
        reward += curReward
    return reward / len(df)
        
        
    
def calculateActualReward(policyOutFile, moocletName, numActions, proportion=1.0):
    df = pandas.read_csv(policyOutFile)
    return df[['Reward' + moocletName]].loc[int(len(df)*proportion):].mean().ix[0]
    
def calculateProportionOfOptimalActions(simulatedDataFile, policyOutFile, moocletName, numActions, proportion=1.0):
    maxRewardDf, optimalActionIndexDf = calculateOptimalActionsFromSimulatedDataFile(simulatedDataFile, moocletName, numActions)
    df = pandas.read_csv(policyOutFile)
    placeToStart = int(len(df) - .7*len(df))
    optimalActionsChosen = 0
    # this isn't efficient, but not sure on efficient way and just want it calculated...
    for i in range(placeToStart, len(df)):
        bestAction = optimalActionIndexDf[i] # this gives a column name
        optimalActionIndex = int(bestAction[len(moocletName)+1:(len(moocletName)+2)]) - 1 # these start at 1, policy version starts at 0
        if optimalActionIndex == df[moocletName][i]:
            optimalActionsChosen += 1

    return optimalActionsChosen / (len(df)-placeToStart)

def runSimulations(simulatedDataFile, moocletPrefix, numMooclets, numActions, numIterations, directoryForFiles, proportion):
    if directoryForFiles[-1] != os.sep:
        directoryForFiles += os.sep
    statisticsForResults = []
         
    
    for i in range(numIterations):
        # create samples from each policy - could simplify for more policies
        randomFileName = directoryForFiles + 'RandomPolicy' + str(i) +'.csv'
        randomPolicy.calculateRandomPolicy(simulatedDataFile, randomFileName)
        thompsonFileName = directoryForFiles + 'ThompsonPolicy' + str(i) +'.csv'
        thompson.calculateThompson(simulatedDataFile, thompsonFileName)
        curStats = []
        for j in range(numMooclets):
            curStats.append([i, j, 'random', calculateActualReward(randomFileName, moocletPrefix + str(j+1), numActions, proportion), calculateProportionOfOptimalActions(simulatedDataFile,randomFileName, moocletPrefix + str(j+1), numActions, proportion)])
            curStats.append([i, j, 'thompson', calculateActualReward(thompsonFileName, moocletPrefix + str(j+1), numActions, proportion), calculateProportionOfOptimalActions(simulatedDataFile,thompsonFileName, moocletPrefix + str(j+1), numActions, proportion)])
        # print("curStats: " + "".join(str(x) for x in curStats))
        
        statisticsForResults.extend(curStats)
    return pandas.DataFrame(statisticsForResults, columns=('iteration','mooclet', 'policy', 'reward','optimalActionProportion'))

        
def writePolicyStatistics(outFilePrefix, results):
    results.to_csv(outFilePrefix + 'All.csv',index=False)
    grouped = results.groupby(['mooclet','policy'],as_index=False)
    grouped.mean().to_csv(outFilePrefix + 'Grouped.csv',index=False)

def main():
  '''
  for now, simulated data file is arg 1, policy out file is arg 2, mooclet name is arg 3, and numActions is arg 4
  (TODO: should use an argument parser)
  Example:
  calculateRewardExpectations.py ../data/simulated_data_files_input.csv MOOClet 3 3 25 ../data/varianceTesting/ ../data/varianceOut .25
  '''
  if len(sys.argv) >= 8: # run a bunch and output to a file
      simDataFile = sys.argv[1]
      moocletPrefix =sys.argv[2]
      numMooclets = int(sys.argv[3])
      numActions = int(sys.argv[4])
      numIterations = int(sys.argv[5])
      directoryForFiles = sys.argv[6]
      proportion = 1
      if len(sys.argv) >= 9:
          proportion = float(sys.argv[8])
      writePolicyStatistics(sys.argv[7],runSimulations(simDataFile, moocletPrefix, numMooclets, numActions, numIterations, directoryForFiles, proportion))
  else:
      simDataFile = sys.argv[1]
      policyOutFile = sys.argv[2]
      moocletName =sys.argv[3]
      numActions = int(sys.argv[4])
      print('Expected reward for optimal actions: ' + str(calculateExpectedReward(simDataFile, moocletName, numActions)))
      print('Actual reward for optimal actions: ' + str(calculateRewardBasedOnOptimalActions(simDataFile, moocletName, numActions)))
      print('Actual reward for policy: ' + str(calculateActualReward(policyOutFile, moocletName, numActions)))
  

if __name__ == "__main__":
  main()
