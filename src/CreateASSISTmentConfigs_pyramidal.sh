#!/bin/bash

echo "Type path to your banditalgorithms folder below and hit Enter."
echo "For example, I would type '/home/yangz2/banditalgorithms' without quotation."
echo "[WARNING] Please make sure that configFiles/ASSISTmentsPyramidalConfigs is not already full."

# shellcheck disable=SC2162
read CODEBASE

echo "You typed ${CODEBASE}."

echo "GENERATING CONFIGS ......"

TEMPLATE_DIR="ASSISTmentsConfigsTemplates"
EXP_SPEC_DIR="ASSISTmentsPyramidalConfigs"  # must be a directory within configFiles
LAMBDA=1000

for SIMTYPE in "Params" "Outcomes"
do
    for REWARDSCHEME in "complete" "ProblemCount"
    do
        python3 ${CODEBASE}/src/CreateASSISTmentConfigs.py \
        --template_file=${CODEBASE}/configFiles/${TEMPLATE_DIR}/ASSISTments${SIMTYPE}Config226210_${REWARDSCHEME}.json \
        --config_out_directory=${CODEBASE}/configFiles/${EXP_SPEC_DIR}/${SIMTYPE}Configs \
        --simulation_subdir=lambda_${LAMBDA}_pyramidal \
        --lambdaNumber=${LAMBDA} \
        --online=0 \
        --dow_scheduler_name=pyramidal \
        --assistments_path=${CODEBASE}/configFiles/${TEMPLATE_DIR}/ASSISTmentsData.csv
    done
done

echo "FINISHED"