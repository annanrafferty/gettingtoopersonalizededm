﻿import sys
import random
import csv
import numpy
"""
Creates a data set where reward probabilities are based on a file of probabilities
(e.g. SimulatedDataRewardProbabilitiesByCondition.csv)
Example:
 python3 generateDataFromContextProbabilityFile.py ../data/SimulatedDataRewardProbabilitiesByConditionIncreased.csv ../data/simulated_data_files_input.csv ../data/simulatedDataCreatedWithProbs.csv
"""

def getMoocletColumnName(moocletName, action):
    characterToAppend = chr(ord('A') + int(moocletName[-1]) -1)
    return moocletName + characterToAppend + str(action) 
    

def generateIndependentBanditData(response_prob, source, dest, num_actions):
    '''
    Generate separate bandit files from source files.
    :param response_prob: Input file containing response probabilities for each context group.
    :param source: Input file containing simulated data for each MOOClet.
    :param dest: Output file prefix that will be appended with the bandit number to use as output path.
                 For example, prefix = "my_file" then output for bandit 1 = "my_file.bandit1.csv"
    :param num_actions: Number of actions/arms to generate files for.
    '''
    numMooclets = 3

    with open(source, newline='') as in_file:
        reader = csv.DictReader(in_file)
        
        # initialize csv writers for separate bandit files
        csv_writers = []

        for moocletIndex in range(1, numMooclets + 1):

            f = open('{}.bandit{}.csv'.format(dest, moocletIndex), 'w', newline='')

            # set column names for each bandit file
            names = ['SampleNumber']
            names.extend(reader.fieldnames[1:3]) # context variables
            
            # fields for action rewards
            for actionIndex in range(1, num_actions + 1):
                names.append(getMoocletColumnName('MOOClet{}'.format(moocletIndex), actionIndex))

            # fields for probabilities
            for actionIndex in range(1, num_actions + 1):
                names.append(getMoocletColumnName('MOOClet{}'.format(moocletIndex), actionIndex) + 'PROB')

            # write list of column names for each bandit file
            writer = csv.DictWriter(f, fieldnames=names)
            writer.writeheader()

            csv_writers.append(writer)

        sampleNumber = 0
        for row in reader:
            sampleNumber += 1

            for moocletIndex in range(1, numMooclets + 1):
                curRow = {}
                curRow['SampleNumber'] = sampleNumber
                contextVector = []
                for i in range(1, 3):
                    curRow[reader.fieldnames[i]] = row[reader.fieldnames[i]]
                    # get the user vars
                    contextVector.append(int(row[reader.fieldnames[i]]))

                # get response probabilities
                contextVector = tuple(contextVector)
                probabilities = response_prob.get(contextVector)

                # write for action rewards
                index = 0
                for actionIndex in range(1, num_actions + 1):
                    reward = 0
                    if random.random() < probabilities[index]:
                        reward = 1
                    curRow[getMoocletColumnName('MOOClet' + str(moocletIndex), actionIndex)] = reward
                    index += 1
                
                # write probabilities
                index = 0
                for actionIndex in range(1, num_actions + 1):
                    curRow[getMoocletColumnName('MOOClet' + str(moocletIndex), actionIndex) + 'PROB'] = probabilities[index]
                    index += 1

                # write out
                csv_writers[moocletIndex - 1].writerow(curRow)


#source is the input file for column headers and context vectors
#respprop is the dictionary of response probabilities by context
def generateSimulatedDataFile(respProb, source, dest):
    numMooclets = 3
    numActions = 3
    with open(source, newline='') as inf, open(dest, 'w', newline='') as outf:
        reader = csv.DictReader(inf)
        fieldnamesOut = ['SampleNumber']
        fieldnamesOut.extend(reader.fieldnames[1:3]) # context variables

        # fields for action rewards
        for moocletIndex in range(1,numMooclets+1):
            for actionIndex in range(1, numActions + 1):
                fieldnamesOut.append(getMoocletColumnName('MOOClet' + str(moocletIndex), actionIndex))
                
        # fields for probabilities
        for moocletIndex in range(1,numMooclets+1):
            for actionIndex in range(1, numActions + 1):
                fieldnamesOut.append(getMoocletColumnName('MOOClet' + str(moocletIndex), actionIndex) + 'PROB')

        writer = csv.DictWriter(outf, fieldnames=fieldnamesOut)
        writer.writeheader()
        sampleNumber = 0
        for row in reader:
            sampleNumber += 1
            curRow = {}
            curRow['SampleNumber'] = sampleNumber
            contextVector = []
            for i in range(1,3):
                curRow[reader.fieldnames[i]] = row[reader.fieldnames[i]]
                #get the user vars
                contextVector.append(int(row[reader.fieldnames[i]]))
            # get response probabilities
            contextVector = tuple(contextVector)
            probabilities = respProb.get(contextVector)
            index = 0
            # write for action rewards
            for moocletIndex in range(1,numMooclets+1):
                for actionIndex in range(1, numActions + 1):
                    reward = 0
                    if random.random() < probabilities[index]:
                        reward = 1
                    curRow[getMoocletColumnName('MOOClet' + str(moocletIndex), actionIndex)] = reward
                    index += 1

                
            # write probabilities
            index = 0
            for moocletIndex in range(1,numMooclets+1):
                for actionIndex in range(1, numActions + 1):
                    curRow[getMoocletColumnName('MOOClet' + str(moocletIndex), actionIndex) + 'PROB'] = probabilities[index]
                    index += 1

            #write out 
            writer.writerow(curRow)

def readResponseProbabilities(inFile):
    respProb = {}
    with open(inFile, newline='') as inf:
        reader = csv.DictReader(inf)
        for row in reader:
            # get context - key for the dictionary
            contextVector = []
            for i in range(0,2):
                #get the user vars
                contextVector.append(int(row[reader.fieldnames[i]]))
            contextVector = tuple(contextVector)
            
            # get probability values - need to find initial value and thenjust read to end
            indexOfFirstProbability = reader.fieldnames.index(getMoocletColumnName('MOOClet' + str(1), 1) + 'PROB')
            probabilities = []
            for i in range(indexOfFirstProbability, len(reader.fieldnames)):
                probabilities.append(float(row[reader.fieldnames[i]]))
            respProb[contextVector] = probabilities
    return respProb
            
    

def main():
    responseProbabilityFile = sys.argv[1]
    simulatedDataVectorFile = sys.argv[2]
    outfile = sys.argv[3]
    responseProb = readResponseProbabilities(responseProbabilityFile)
    #generateSimulatedDataFile(responseProb, simulatedDataVectorFile, outfile)
    generateIndependentBanditData(responseProb, simulatedDataVectorFile, outfile, num_actions=3)

if __name__ == "__main__":
    main()