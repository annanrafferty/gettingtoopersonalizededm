#!/bin/bash

# Automatically run src/CreateASSISTmentConfigs.py for all template files located in
# configFiles/ASSISTmentsConfigsTemplates for several lambda values. Freshly
# generated configs will be located in configFiles/ASSISTmentsRegConfigs. Afterwards,
# it runs src/CreateASSISTmentConfigs_reg_test.py to check if all configs are generated
# correctly.
#
# Caution: Running this script will overwrite existing configs in configFiles/ASSISTmentsRegConfigs.
#
# Zhihan Yang
# February 6, 2021

# In case of permission issues
#
# Run chmod +rx CreateASSISTmentConfigs_reg.sh before runnning CreateASSISTmentConfigs_reg.sh.

echo "Type path to your banditalgorithms folder below and hit Enter."
echo "For example, I would type '/home/yangz2/banditalgorithms' without quotation."
echo "[WARNING] Please make sure that configFiles/ASSISTmentsRegConfigs is not already full."

# shellcheck disable=SC2162
read CODEBASE

echo "You typed ${CODEBASE}."

echo "GENERATING CONFIGS ......"

# What is a EXP_SPEC_DIR?
#
# A EXP_SPEC_DIR is a directory containing several sub-directories (may even be nested) of configs.
# Each sub-directory corresponds to a combination of values of a few independent variables.
# For example, here we have SIMTYPE, REWARDSCHEME and LAMBDA as our three independent variables.
# Therefore, their container ASSISTmentsRegConfigs is called an Experiment Specification Directory (EXP_SPEC_DIR)
# because it specifies all the info necessary to run an experiment / answer a question of interest.
#
# You should also mkdir and pre-populate ${EXP_SPEC_DIR}Templates with relevant template files (this is done already).

TEMPLATE_DIR="ASSISTmentsConfigsTemplates"
EXP_SPEC_DIR="ASSISTmentsRegConfigs"  # must be a directory within configFiles

for SIMTYPE in "Params" "Outcomes"
do
    for REWARDSCHEME in "complete" "ProblemCount"
    do
        for LAMBDA in 1 1000 1000000
        do
            python3 ${CODEBASE}/src/CreateASSISTmentConfigs.py \
            --template_file=${CODEBASE}/configFiles/${TEMPLATE_DIR}/ASSISTments${SIMTYPE}Config226210_${REWARDSCHEME}.json \
            --config_out_directory=${CODEBASE}/configFiles/${EXP_SPEC_DIR}/${SIMTYPE}ConfigsLambda${LAMBDA} \
            --simulation_subdir=lambda_${LAMBDA} \
            --lambdaNumber=${LAMBDA}
        done
    done
done

echo "FINISHED"
echo "TESTING WHETHER GENERATED CONFIGS ARE CORRECT ......"

python3 ${CODEBASE}/src/CreateASSISTmentConfigs_reg_test.py \
--exp_spec_dir=${CODEBASE}/configFiles/${EXP_SPEC_DIR}

echo "ALL CORRECT IF YOU SEE NO ERROR MESSAGES"