﻿import sys
import csv
import random
import math
import numpy as np

#source = 'mailresults_week1_small.csv'
dest1 = 'test.csv'
dest2 = 'testThompson_simData.csv'

#Iterative update
#values=5x3x3 array of current values (average response rate) for this MOOClet
#counts=5x3x3 array of how many times we've seen each age x nDays x version tuple
#version=which version this student got
#nDaysAct, ageQuartile=this student's context
#starte=whether this student started the survey (0 or 1)

def updateValues(values, counts, version, nDaysAct, ageQuartile, started):
    oldVal = values[version][nDaysAct][ageQuartile];
    newVal = (oldVal*counts[version][nDaysAct][ageQuartile] + started)/(counts[version][nDaysAct][ageQuartile]+1);
    #print("new value is", newVal)
    values[version][nDaysAct][ageQuartile] = newVal;
    counts[version][nDaysAct][ageQuartile] += 1;
   # print(values)
    return [values, counts]


def compute_regret(src_truth, src_test):

    num_mooclets = 3
    num_actions = 3

    context_to_optimal_actions = {}

    with open(src_truth, newline='') as in_file:
        reader = csv.DictReader(in_file)

        # generate list of probs for each mooclet
        field_names = [['MOOClet{}{}{}PROB'.format(
            m + 1, chr(ord('A') + m), a + 1) for a in range(
            num_actions)] for m in range(num_mooclets)]

        # load dictionary mapping from context to optimal actions for each mooclet
        for row in reader:
            optimal_actions = []

            # context = age group & # of days active
            context = (max(0, int(row[reader.fieldnames[1]])), max(0, int(row[reader.fieldnames[2]])))

            # get optimal action
            for m in range(num_mooclets):
                optimal = np.argmax([row[field_names[m][a]] for a in range(num_actions)])
                optimal_actions.append(optimal)

            context_to_optimal_actions[context] = np.array(optimal_actions)

    with open(src_test, newline='') as in_file:
        reader = csv.DictReader(in_file)

        field_names = [ 'MOOClet{}'.format(m + 1) for m in range(num_mooclets) ]

        row_count = 0
        regret = 0
        regret_bandits = np.zeros(num_mooclets).astype(int)
        for row in reader:
            row_count += 1
            # context = age group & # of days active
            context = (max(0, int(row[reader.fieldnames[1]])), max(0, int(row[reader.fieldnames[2]])))

            actions = np.array( [ int(row[field_names[m]]) for m in range(num_mooclets) ] )

            optimal_actions = context_to_optimal_actions[context]

            # This regret treats all 3 bandits as one single bandit
            if np.array_equal(optimal_actions, actions) == False:
                regret += 1

            for m in range(num_mooclets):
                regret_bandits[m] += (1 if (optimal_actions[m] == actions[m]) else 0)

        print('Total regret for file {} is {} out of {}'.format(src_test, regret, row_count))
    
        for m in range(num_mooclets):
            print('Regret for bandit {} is {}'.format(field_names[m], regret_bandits[m]))


def calculateValues(source):
    '''source is a data file that gives the actual reward for each row given the
    chosen action. This function updates the values using the updateValues function,
    which seems to be using the softmax-ish policy.
    TODO: It seems like some refactoring should be done to put this function into a 
    module that's unrelated to Thompson sampling. Additionally, it might be helpful
    to take an OOP approach, defining a general class that has all the methods we
    want and then making each individual policy override those methods.
    '''

    #array to hold the expected values (3 version x 3 days act values x 5 age values)
    # possibly want this to actually a 4-d array instead
    values1 =[[[0 for col in range(5)] for col in range(3)] for cond in range(3)]
    values2 =[[[0 for col in range(5)] for col in range(3)] for cond in range(3)]
    values3 =[[[0 for col in range(5)] for col in range(3)] for cond in range(3)]

    #array to hold how many times we've seen each combo
    # possibly want this to actually a 4-d array instead
    counts1 =[[[0 for col in range(5)] for col in range(3)] for cond in range(3)]
    counts2 =[[[0 for col in range(5)] for col in range(3)] for cond in range(3)]
    counts3 =[[[0 for col in range(5)] for col in range(3)] for cond in range(3)]


    with open(source, newline='') as inf, open(dest1, 'w', newline='') as outf:
        reader = csv.DictReader(inf)
        fieldNamesOut = reader.fieldnames
        fieldNamesOut.append('MOOClet1a')
        fieldNamesOut.append('MOOClet1b')
        fieldNamesOut.append('MOOClet1c')
        fieldNamesOut.append('MOOClet2a')
        fieldNamesOut.append('MOOClet2b')
        fieldNamesOut.append('MOOClet2c')
        fieldNamesOut.append('MOOClet3a')
        fieldNamesOut.append('MOOClet3b')
        fieldNamesOut.append('MOOClet3c')
        writer = csv.DictWriter(outf, fieldnames=fieldNamesOut)
        writer.writeheader()
        sampleNumber = 1 # starting sample numbering at 1, not 0
        for row in reader:
            #get the user vars
            ageQuartile = int(row['agequartilesUSER']);
            #user 0 instead of -1 for age quartiles
            if ageQuartile==-1:
              ageQuartile=0;
            
            nDaysAct = int(row['ndaysactUSER']);
            
            #see which versions they got
            if (row['subjectCondition']=='survey'):
              version1 = 0;
            elif(row['subjectCondition']=='question'):
              version1=1;
            elif row['subjectCondition']=='returning':
              version1=2;
            
            if (row['introCondition']=='original'):
              version2 = 0;
            elif(row['introCondition']=='survey'):
              version2=1;
            elif (row['introCondition']=='brief'):
              version2=2;
            
            if (row['questionCondition']=='simplesurvey'):
              version3 = 0;
            elif(row['questionCondition']=='twochoice'):
              version3=1;
            elif(row['questionCondition']=='embeddedlinks'):
              version3=2;
            
            #check whether they started the survey (reward signal)
            started = int(row['started_survey']);
            
            #update the value of the received version for similar users
            [values1, counts1] = updateValues(values1, counts1, version1, nDaysAct, ageQuartile, started)
            [values2, counts2] = updateValues(values2, counts2, version2, nDaysAct, ageQuartile, started)
            [values3, counts3] = updateValues(values3, counts3, version3, nDaysAct, ageQuartile, started)

            writer.writerow({'SampleNumber' : sampleNumber, 'agequartilesUSER': ageQuartile, 'ndaysactUSER' : nDaysAct, 'subjectCondition' : version1, 'introCondition' : version2, 'questionCondition' : version3, 'started_survey' : started,
                             'MOOClet1a' : values1[0][nDaysAct][ageQuartile], 'MOOClet1b' : values1[1][nDaysAct][ageQuartile], 'MOOClet1c' : values1[2][nDaysAct][ageQuartile],
                             'MOOClet2a' : values2[0][nDaysAct][ageQuartile], 'MOOClet2b' : values2[1][nDaysAct][ageQuartile], 'MOOClet2c' : values2[2][nDaysAct][ageQuartile],
                             'MOOClet3a' : values3[0][nDaysAct][ageQuartile], 'MOOClet3b' : values3[1][nDaysAct][ageQuartile], 'MOOClet3c' : values3[2][nDaysAct][ageQuartile]})

    return [values1, values2, values3]

def exp(val):
    return pow(2.718, val)

#Use the values calculated in the calculate value function to weight the versions using a Boltzmann dist
def calculateWeightsBoltzmann(allValues):
  
  #separate the MOOClets
  values1 = allValues[0];
  values2 = allValues[1];
  values3 = allValues[2];
  
  #array to hold the weights
  weights = [[[[0 for col in range(5)] for col in range(3)] for cond in range(3)] for ver in range(3)]
  #hold the denominators
  denom = [[[0 for col in range(5)] for col in range(3)] for cond in range(3)]
  
  #temperature parameter, can be adjusted later
  temp = 0.5;

 #do we need these?   
  tDenom=0
  first=0
  sec=0
  third=0
  
  for m in range(0,len(allValues)):
    for k in range(0, len(allValues[0][0])):
      for i in range(0, len(allValues[0][0][0])):
        first = exp((allValues[m][0][k][i])/temp)
        sec = exp((allValues[m][1][k][i])/temp)
        third = exp((allValues[m][2][k][i])/temp)
        tDenom = first+sec+third
        denom[m][k][i] = tDenom
        for j in range(0,3):
          #calculate weights using Boltzmann
          weights[m][j][k][i] = exp(allValues[m][j][k][i]/temp)/denom[m][k][i]
        
  return weights;

def calculateThompsonValuesSecondReward(simulatedDataReward2, fileWithActions, numActionsToProcess, outputFile):
    '''Calculates the Thompson model values (parameters for the betas) based on following the
    policy specified in fileWithActions. Rewards are taken from the file simulatedDataReward2,
    and these rewards may or may not match the rewards that were received when the original actions
    were taken. simulatedDataReward2 may have more rows than fileWithActions; values are updated
    only through numActionsToProcess. This is to facilitate, e.g., switching to 
    active learning where we get to actually choose the actions again. outputFile rewards
    received rewards from simulatedDataReward2; likely, we want to append to this same file when
    switching to active learning. This function will overwrite any existing content in outputfile.'''
    numMooclets = 3
    successes = []
    failures = []
    for i in range(numMooclets):
        curSuccess, curFailure = initializeSuccessFailure(3, 3, 5)
        successes.append(curSuccess)
        failures.append(curFailure)
    
    with open(simulatedDataReward2, newline='') as simDataFile, open(fileWithActions, newline='') as actionFile, open(outputFile, 'w', newline='') as outf:
        simReader = csv.DictReader(simDataFile)
        actionReader = csv.DictReader(actionFile)
        fieldNamesOut = constructHeadersForPolicyOutFile(simReader.fieldnames)
        writer = csv.DictWriter(outf, fieldnames=fieldNamesOut)
        writer.writeheader()
        # Loop while we still have actions to process
        actionsProcessed = 0
        for actionLine in actionReader:
            simLine = next(simReader)
            
            contextTuple = getContextVars(actionLine)
            actions = []
            for i in range(numMooclets):
                actions.append(int(actionLine['MOOClet' + str(i+1)]))
                
            rewards = getRewardsFromSimDataRow(simLine, numMooclets, actions)
            
            for i in range(numMooclets):
                updateThompsonValues(rewards[i], actions[i], contextTuple[0], contextTuple[1], successes[i], failures[i])
            
            # write to outfile
            writer.writerow({'SampleNumber' : actionLine['SampleNumber'], 'agequartilesUSER': contextTuple[1], 
                'ndaysactUSER' : contextTuple[0], 'MOOClet1' : actions[0], 'MOOClet2' : actions[1], 'MOOClet3' : actions[2],
                'RewardMOOClet1' : rewards[0], 'RewardMOOClet2' : rewards[1], 'RewardMOOClet3' : rewards[2]})
            actionsProcessed += 1
            if actionsProcessed >= numActionsToProcess:
                break
    # At the end, return the successes and failures so these could be used for future action choices
    return (successes, failures)

def calculateActionChoices(successes, failures, simulatedData, sampleToStartChoosingActions, outFile):
    '''Initializes Thompson counts as the successes, failures lists. Skips the first sampleToStartChoosingActions - 1
    samples in simulatedData, and then acts as normal for Thompson sampling: chooses an action, observes reward,
    updates successes, failures. outFile is *appended* to starting with sampleToStartChoosingActions.'''
    numMooclets = 3
    with open(simulatedData, newline='') as simDataFile, open(outFile, 'a', newline='') as outf:
        simReader = csv.DictReader(simDataFile)
        fieldNamesOut = constructHeadersForPolicyOutFile(simReader.fieldnames)
        writer = csv.DictWriter(outf, fieldnames=fieldNamesOut)
        # Skip the first sampleToStartChoosingActions lines, then start choices
        sampleNumber = 0
        for line in simReader:
            sampleNumber += 1
            if sampleNumber >= sampleToStartChoosingActions:
                # Choose action, update counts, append to file
                contextTuple = getContextVars(line)
                ageQuartile = contextTuple[1] # TODO: generalize better
                nDaysAct = contextTuple[0]
                actions = []
                samples = []
                for i in range(numMooclets):
                    action, sample = chooseThompsonAction(successes[i], failures[i], contextTuple)
                    actions.append(action)
                    samples.append(sample)
                
                # Get reward.
                rewards = getRewardsFromSimDataRow(line, numMooclets, actions)

                #track success/failure 
                for i in range(numMooclets):
                    updateThompsonValues(rewards[i], actions[i], contextTuple[0], contextTuple[1], successes[i], failures[i])

    
                #write out some of the inputs, which versions we chose, samples #TODO: generalize better
                writer.writerow({'SampleNumber' : sampleNumber, 'agequartilesUSER': ageQuartile, 'ndaysactUSER' : nDaysAct, 
                                 'MOOClet1' : actions[0], 'MOOClet2' : actions[1], 'MOOClet3' : actions[2],
                                 'MOOClet1a' : samples[0][0], 'MOOClet1b' : samples[0][1], 'MOOClet1c' : samples[0][2],
                                 'MOOClet2a' : samples[1][0], 'MOOClet2b' : samples[1][1], 'MOOClet2c' : samples[1][2],
                                 'MOOClet3a' : samples[2][0], 'MOOClet3b' : samples[2][1], 'MOOClet3c' : samples[2][2],
                                 'RewardMOOClet1' : rewards[0], 'RewardMOOClet2' : rewards[1], 'RewardMOOClet3' : rewards[2]})


            
            
def getContextVars(row):
    '''Gets the values for context variables (num days active and age quartile,
    in that order). Extracted into a fn to try to make it easier in the future to 
    not have everything incredibly specific to this one type of data file.'''
    ageQuartile = int(row['agequartilesUSER']);
    #user 0 instead of -1 for age quartiles
    if ageQuartile==-1:
        ageQuartile=0;
    
    nDaysAct = int(row['ndaysactUSER']);
    return (nDaysAct, ageQuartile)
            

def initializeSuccessFailure(numActions, numValuesContextVar1, numValuesContextVar2):
    '''Returns empty success and failure multidimensional lists.
    '''
    # context var 2 x context var 1 x actions
    success =[[[1 for col in range(numValuesContextVar2)] for col in range(numValuesContextVar1)] for cond in range(numActions)]
    failure =[[[1 for col in range(numValuesContextVar2)] for col in range(numValuesContextVar1)] for cond in range(numActions)]
    return success, failure

def constructHeadersForPolicyOutFile(simulatedDateFieldNames):
    fieldNamesOut = simulatedDateFieldNames[0:3]
    #output the conditions chosen
    fieldNamesOut.append('MOOClet1') # Modified these names to match the naming scheme of other columns
    fieldNamesOut.append('MOOClet2')
    fieldNamesOut.append('MOOClet3')
    #output our samples drawn
    fieldNamesOut.append('RewardMOOClet1')
    fieldNamesOut.append('RewardMOOClet2')
    fieldNamesOut.append('RewardMOOClet3')
    fieldNamesOut.append('MOOClet1a')
    fieldNamesOut.append('MOOClet1b')
    fieldNamesOut.append('MOOClet1c')
    fieldNamesOut.append('MOOClet2a')
    fieldNamesOut.append('MOOClet2b')
    fieldNamesOut.append('MOOClet2c')
    fieldNamesOut.append('MOOClet3a')
    fieldNamesOut.append('MOOClet3b')
    fieldNamesOut.append('MOOClet3c')
    return fieldNamesOut;

def getRewardsFromSimDataRow(row, numMooclets, chosenActions):
    '''Returns a list of rewards read from this row, handling the column naming scheme.
    '''
    rewards = []
    for i in range(numMooclets):
        characterToAppend = chr(ord('A') + i)
        rewards.append(int(row['MOOClet' + str(i+1) + characterToAppend + str(chosenActions[i]+1)]))
    return rewards

def updateThompsonValues(reward, chosenAction, nDaysAct, ageQuartile, successCounts, failureCounts):
    '''Updates the success and failure counts based on a new sample.
    '''
    if(reward == 1):
        successCounts[chosenAction][nDaysAct][ageQuartile] += 1
    else:
        failureCounts[chosenAction][nDaysAct][ageQuartile] += 1

def chooseThompsonAction(successCounts, failureCounts, contextTuple):  
    '''Chooses an action using Thompson sampling based on the successs and failure
    counts with contextTuple. Also results the samples taken by Thompson.
    '''
    nDaysAct = contextTuple[0]
    ageQuartile = contextTuple[1]
    samples= []
    for i in range(0,3):
        samples.append(numpy.random.beta(successCounts[i][nDaysAct][ageQuartile], failureCounts[i][nDaysAct][ageQuartile]))
    action = samples.index(max(samples))
    return (action, samples)


def calculateThompson(source, dest):
    '''Calculates the thompson actions and weights. source is a simulated data file that
    records the default rewards for each action. dest is the outfile for printing the 
    chosen actions and received rewards.'''
    numMooclets = 3
    # 5 (age quartiles) x 3 (nDays) x 3 (versions) keeping track of sucessful pulls
    success1 =[[[1 for col in range(5)] for col in range(3)] for cond in range(3)]
    success2 =[[[1 for col in range(5)] for col in range(3)] for cond in range(3)]
    success3 =[[[1 for col in range(5)] for col in range(3)] for cond in range(3)]

    # 5 (age quartiles) x 3 (nDays) x 3 (versions) keeping track of failed pulls
    failure1 =[[[1 for col in range(5)] for col in range(3)] for cond in range(3)]
    failure2 =[[[1 for col in range(5)] for col in range(3)] for cond in range(3)]
    failure3 =[[[1 for col in range(5)] for col in range(3)] for cond in range(3)]

    
    with open(source, newline='') as inf, open(dest, 'w', newline='') as outf:
        reader = csv.DictReader(inf)
        fieldNamesOut = constructHeadersForPolicyOutFile(reader.fieldnames)

        writer = csv.DictWriter(outf, fieldnames=fieldNamesOut)
        writer.writeheader()
        sampleNumber = 0
        for row in reader:
            sampleNumber += 1
            #get the user vars
            ageQuartile = int(row['agequartilesUSER']);
            #user 0 instead of -1 for age quartiles
            if ageQuartile==-1:
                ageQuartile=0;
            
            nDaysAct = int(row['ndaysactUSER']);


            #first decide which arm we'd pull using Thompson (do the random sampling, the max is the one we'd choose)
            samples=[[1 for cond in range(3)] for mooclet in range(3)]
            for i in range(0,3):
                #print('nDays is ' + str(nDaysAct))
                #print('age q is ' + str(ageQuartile))
                #print('i is ' + str(i))
                samples[0][i]=np.random.beta(success1[i][nDaysAct][ageQuartile], failure1[i][nDaysAct][ageQuartile])
                samples[1][i]=np.random.beta(success2[i][nDaysAct][ageQuartile], failure2[i][nDaysAct][ageQuartile])
                samples[2][i]=np.random.beta(success3[i][nDaysAct][ageQuartile], failure3[i][nDaysAct][ageQuartile])
                
            
            #find the max of samples[0][i] etc and choose an arm
            actions = []
            for i in range(numMooclets):
                actions.append(samples[i].index(max(samples[i])))

            # get reward signals
            rewards = getRewardsFromSimDataRow(row, numMooclets, actions)


            #track success/failure # it would be nice if this were generalized to make it easier to scale to other numbers of actions/mooclets
            updateThompsonValues(rewards[0], actions[0], nDaysAct, ageQuartile, success1, failure1)
            updateThompsonValues(rewards[1], actions[1], nDaysAct, ageQuartile, success2, failure2)
            updateThompsonValues(rewards[2], actions[2], nDaysAct, ageQuartile, success3, failure3)


            #write out some of the inputs, which versions we chose, samples
            writer.writerow({'SampleNumber' : sampleNumber, 'agequartilesUSER': ageQuartile, 'ndaysactUSER' : nDaysAct, 'MOOClet1' : actions[0], 'MOOClet2' : actions[1], 'MOOClet3' : actions[2],
                             'MOOClet1a' : samples[0][0], 'MOOClet1b' : samples[0][1], 'MOOClet1c' : samples[0][2],
                             'MOOClet2a' : samples[1][0], 'MOOClet2b' : samples[1][1], 'MOOClet2c' : samples[1][2],
                             'MOOClet3a' : samples[2][0], 'MOOClet3b' : samples[2][1], 'MOOClet3c' : samples[2][2],
                             'RewardMOOClet1' : rewards[0], 'RewardMOOClet2' : rewards[1], 'RewardMOOClet3' : rewards[2]})


#calculateWeightsBoltzmann(calculateValues('mailresults_week1_small.csv'))
def main():
    if len(sys.argv) == 3: 
        calculateThompson(sys.argv[1], sys.argv[2])
    elif len(sys.argv) == 6:
        simDataReward1 = sys.argv[1]
        simDataReward2 = sys.argv[2]
        actionsOutReward1 = sys.argv[3]
        outFileReward2 = sys.argv[4]
        samplesForReward1 = int(sys.argv[5])
        
        # First, choose some actions wiht initial reward
        calculateThompson(simDataReward1, actionsOutReward1)
        # Next, update the successes/failures using reward 2 based on actions chosen
        successes, failures = calculateThompsonValuesSecondReward(simDataReward2, actionsOutReward1, samplesForReward1, outFileReward2)
        # Finally, choose remaining actions using policy for reward 2
        calculateActionChoices(successes, failures, simDataReward2, samplesForReward1 + 1, outFileReward2)
    else:
        calculateThompson('simulated_data_files_input.csv', 'testThompson_simData.csv')

if __name__ == "__main__":
    main()
