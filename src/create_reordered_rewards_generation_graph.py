'''
Created on Oct 27, 2017

Module for reading in already generated rewards and visualizing the average reward per quartile. 
Idea is that we want to show that reward reordering with softmax does lead to noticeable changes
in reward over time (and how big those changes are).

@author: rafferty
'''
import os
import pandas as pd
import re
import numpy as np
import csv

# Helper functions
def get_sampling_spec_from_filename(filename):
    # ignore case
    filename = filename.lower()
    if 'uniform' in filename: # this is strongest cue for non-bandit sampling
        return 'uniform'
    elif 'ng' in filename or 'bb' in filename: # if one of these appears and filename doesn't have uniform in it, should be bandit
        return 'bandit'
    elif 'nu' in filename or 'bu' in filename:
        return 'uniform'
    else:
        print('No sampling specification found: '  + filename)
        return ''
    
def get_reward_spec_from_filename(filename):
    # ignore case
    filename = filename.lower()
    if 'ng' in filename or 'nu' in filename:
        return 'normal'
    elif 'bb' in filename or 'bu' in filename:
        return 'binary'
    else:
        print('No reward specification found: '  + filename)
        return ''

def get_prior_spec_from_filename(filename):
    if 'LowArms' in filename or 'ArmsLow' in filename:
        return 'priorAbove'
    elif 'HighArms' in filename or 'ArmsHigh' in filename:
        return 'priorBelow'
    elif 'Mu' in filename:
        pattern = re.compile('Mu(?P<priorMean>.*)ES')
        search_result = pattern.search(filename)
        if search_result == None:
            print("No prior mean found in filename that had mu: " + filename)
            return '-1.0'# error value
        else:
            return search_result.group('priorMean')
    else:
        print("No prior spec found: "  + filename)
        return 'priorBetween'
    
def get_effect_size_from_filename(filename):
    pattern = re.compile('(NG|BB|NU|BU)(?P<es>.*)Df')
    search_result = pattern.search(filename)
    if search_result == None:
        if 'EqualMeans' in filename:
            return '0.0'
        print("No effect size found in filename: " + filename)
        return '0.0'
    else:
        return search_result.group('es')
    
def get_variance_from_filename(filename):
    '''Valid only for effect size 0 and normal reward type'''
    pattern = re.compile('[a-zA-Z](?P<var>[0-9]*?(\.)[0-9]*)(NG|NU)')
    search_result = pattern.search(filename)
    if search_result == None:
        print("No variance found in filename: " + filename)
        return '-1.0'# error value
    else:
        return search_result.group('var')

def get_preference_type_from_filename(filename):
    ''' Valid only for reordered rewards. Gets a string like "BetterAction1" or "BetterActions"
    '''
    if "PreferBetterActions" in filename:
        return "betterActions"
    elif "PreferWorseActions" in filename:
        return "worseActions"
    elif "WorseAction" in filename:
        endIndex = (filename.find("WorseAction") + len("WorseAction"))
        return "worseAction" + filename[endIndex:(endIndex + 1)]
    elif "BetterAction" in filename:
        endIndex = (filename.find("BetterAction") + len("BetterAction"))
        return "betterAction" + filename[endIndex:(endIndex + 1)]
    else:
        print("No preference type in filename: " + filename)
        return ""# error value
    
def get_softmax_from_filename(filename):
    pattern = re.compile('Softmax(?P<softmax>\d+\.?\d*)')
    search_result = pattern.search(filename)
    if search_result == None:
        print("No softmax found in filename: " + filename)
        return '-1.0'# error value
    else:
        return search_result.group('softmax')
    
def get_sim_num_from_filename(filename):
    pattern = re.compile('_(?P<simnum>\d+)\.csv')
    search_result = pattern.search(filename)
    if search_result == None:
        print("No simnum found in filename: " + filename)
        return '-1.0'# error value
    else:
        return search_result.group('simnum')
    
# Directories to look in for dataframes
# directories_to_combine = {'/Users/rafferty/banditalgorithms/data/effectSizeTuring/reorderedRewards/testData'}#{'sameArms23August2017', 'sameArms24August2017'}
directories_to_combine = {'repeatReorderedRewardsWithEffect9October2017'}#{'sameArms23August2017', 'sameArms24August2017'}

# Whether we'll be combining dfs from simulations with effect size = 0 or != 0.
zero_effect_size = False

# Which types of reordering we care about
reordering_preferences = {'betterActions', 'worseActions'}

# Suffix of relevant dataframe pickles

# number of steps to use for normal
num_steps_normal = 256

# number of steps to use for binary rewards
num_steps_binary = 88

#CSV to write out to
# out_csv = '/Users/rafferty/banditalgorithms/data/effectSizeTuring/reorderedRewards/testData/CombinedReorderedRewardsByTrialWithEffect.csv'
out_csv = 'RewardsByQuartile.csv'


# Assume quartiles
num_diff_weights = 4
# Add missing fields if necessary
add_unequal_variance_fields = True
columns = ['num_steps','sim','reward_type','quartile','softmax','reorderingPreference','avg_reward_quartile','std_dev_quartile','quartile_length']
# Walk through all dataframes to combine to one CSV
is_first = True # We'll mark the first one we write to use write rather than append and to write headers
with open(out_csv, 'w') as outf:
    writer = csv.DictWriter(outf, fieldnames=columns)
    writer.writeheader()
    for directory in directories_to_combine:
        for entry in os.scandir(directory):
            if entry.is_dir() and get_preference_type_from_filename(entry.name) in reordering_preferences:
                # This is a directory that has reward information we care about
                directory_reward_type = get_reward_spec_from_filename(entry.name)
    
                # Figure out what the filename will look like based on reward type, and where the quartile entries will
                # differ 
                if directory_reward_type == "normal":
                    prefix = "tng_rewards_reordered_" + str(num_steps_normal)
                    num_steps = num_steps_normal
    
                else:
                    prefix = "tbb_rewards_reordered_" + str(num_steps_binary)
                    num_steps = num_steps_binary
                    
                # quartile info
                quartile_start_points = [int(round(num_steps*i/num_diff_weights)) for i in range(num_diff_weights)]
                quartile_start_points.append(num_steps)
                
                # row with info for this directory
                row_info = {}
                row_info['num_steps'] = num_steps
                row_info['reward_type'] = directory_reward_type
                row_info['softmax'] = get_softmax_from_filename(entry.name)
                row_info['reorderingPreference'] = get_preference_type_from_filename(entry.name)
    
                for file in os.scandir(entry.path):
                    if file.name.startswith(prefix):
                        # Reward file to read
                        reward_df = pd.read_csv(file.path)
                        row_info['sim'] = get_sim_num_from_filename(file.name)
                        # Now find the average of each quartile
                        for i in range(1, len(quartile_start_points)):
                            row_info['quartile'] = i
                            quartile_start_point = quartile_start_points[i]
                            last_quartile_start_point = quartile_start_points[i-1]
                            cur_quartile_DF = reward_df.iloc[last_quartile_start_point:quartile_start_point]
                            total_reward_quartile = (cur_quartile_DF['Action1OracleActualReward'] + cur_quartile_DF['Action2OracleActualReward']).sum();
                            avg_reward_quartile = total_reward_quartile / (quartile_start_point - last_quartile_start_point)
                            row_info['avg_reward_quartile'] = avg_reward_quartile
                            row_info['std_dev_quartile'] = np.std(cur_quartile_DF['Action1OracleActualReward'] + cur_quartile_DF['Action2OracleActualReward']);
                            row_info['quartile_length'] = quartile_start_point - last_quartile_start_point
            
                            # Now write to csv in progress
                            writer.writerow(row_info)
                                
     
