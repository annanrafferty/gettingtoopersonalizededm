'''
Created on May 9, 2018

@author: rafferty
'''
import re
# Helper functions
def get_sampling_spec_from_filename(filename):
    # ignore case
    filename = filename.lower()
    if 'uniform' in filename: # this is strongest cue for non-bandit sampling
        return 'uniform'
    elif 'nu' in filename or 'bu' in filename: # if one of these appears, should be uniform
        return 'uniform'
    elif 'ng' in filename or 'bb' in filename: # if one of these appears and filename doesn't have uniform in it, should be bandit
        return 'bandit'
    else:
        print('No sampling specification found: '  + filename)
        return ''
    
def get_reward_spec_from_filename(filename):
    # ignore case
    filename = filename.lower()
    if 'ng' in filename or 'nu' in filename:
        return 'normal'
    elif 'bb' in filename or 'bu' in filename:
        return 'binary'
    else:
        print('No reward specification found: '  + filename)
        return ''

def get_prior_spec_from_filename(filename):
    if 'LowArms' in filename or 'ArmsLow' in filename:
        return 'priorAbove'
    elif 'HighArms' in filename or 'ArmsHigh' in filename:
        return 'priorBelow'
    elif 'Mu' in filename:
        pattern = re.compile('Mu(?P<priorMean>.*)ES')
        search_result = pattern.search(filename)
        if search_result == None:
            print("No prior mean found in filename that had mu: " + filename)
            return '-1.0'# error value
        else:
            return search_result.group('priorMean')
    else:
        print("No prior spec found: "  + filename)
        return 'priorBetween'
    
def get_effect_size_from_filename(filename, pklSuffix = 'Df'):
    pattern = re.compile('(NG|BB|NU|BU)(?P<es>.*)' + pklSuffix)
    search_result = pattern.search(filename)
    if search_result == None:
        if 'EqualMeans' in filename:
            return '0.0'
        print("No effect size found in filename: " + filename)
        return '0.0'
    else:
        return search_result.group('es')
    
def get_effect_size_from_directory(directoryname):
    pattern = re.compile('(ES|Uniform)(?P<es>.[^/]*)($|/)')
    search_result = pattern.search(directoryname)
    if search_result == None:
        if 'EqualMeans' in directoryname:
            return '0.0'
        print("No effect size found in filename: " + directoryname)
        return '0.0'
    else:
        return search_result.group('es')
    
def get_variance_from_filename(filename):
    '''Valid only for effect size 0 and normal reward type'''
    pattern = re.compile('[a-zA-Z](?P<var>[0-9]*?(\.)[0-9]*)(NG|NU)')
    search_result = pattern.search(filename)
    if search_result == None:
        print("No variance found in filename: " + filename)
        return '-1.0'# error value
    else:
        return search_result.group('var')
    
def get_variance_from_directoryname(directoryname):
    '''Valid only for effect size 0 and normal reward type'''
    pattern = re.compile('[a-zA-Z](?P<var>[0-9]*?(\.)[0-9]*)(/|$)')
    search_result = pattern.search(directoryname)
    if search_result == None:
        print("No variance found in directoryname: " + directoryname)
        return '-1.0'# error value
    else:
        return search_result.group('var')

def get_preference_type_from_filename(filename):
    ''' Valid only for reordered rewards. Gets a string like "BetterAction1" or "BetterActions"
    '''
    if "PreferBetterActions" in filename:
        return "betterActions"
    elif "PreferWorseActions" in filename:
        return "worseActions"
    elif "WorseAction" in filename:
        endIndex = (filename.find("WorseAction") + len("WorseAction"))
        return "worseAction" + filename[endIndex:(endIndex + 1)]
    elif "BetterAction" in filename:
        endIndex = (filename.find("BetterAction") + len("BetterAction"))
        return "betterAction" + filename[endIndex:(endIndex + 1)]
    else:
        print("No preference type in filename: " + filename)
        return ""# error value
    
def get_softmax_from_filename(filename):
    pattern = re.compile('Softmax(?P<softmax>\d+\.?\d*)')
    search_result = pattern.search(filename)
    if search_result == None:
        print("No softmax found in filename: " + filename)
        return '-1.0'# error value
    else:
        return search_result.group('softmax')

# empirical param: ngEmpirical377938ProblemCountNG0N239Var[6.1015, 5.3148]ArmStats.pdf
# real data: ngRealData377938ProblemCountShuffleTrueNG0PowerOverTime.pdf
def get_experiment_num_from_filename(filename):
    pattern = re.compile('(Empirical|RealData)(?P<experimentNum>\d+)')
    search_result = pattern.search(filename)
    if search_result == None:
        print("No experiment number found in filename: " + filename)
        return '-1.0'# error value
    else:
        return search_result.group('experimentNum')
    

valid_outcome_measures = {'problemcount','logcount', 'complete'}
def get_outcome_measure_from_filename(filename):
    filename = filename.lower()
    for outcome in valid_outcome_measures:
        if outcome.lower() in filename:
            return outcome
    print("No outcome measure found in filename: " + filename)
    return None

#bbRealData256027completeShuffleFalseBB256027completeArmStats.pdf
# real data: ngRealData377938ProblemCountShuffleTrueNG0PowerOverTime.pdf
def get_shuffle_boolean_from_real_data_filename(filename):
    pattern = re.compile('Shuffle(?P<shuffleBoolean>(True|False))')
    search_result = pattern.search(filename)
    if search_result == None:
        print("No shuffle boolean found in filename: " + filename)
        return None
    else:
        return search_result.group('shuffleBoolean') == 'True'