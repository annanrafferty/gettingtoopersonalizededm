"""
The purpose of this script is to make JSONs more readable, which is
very helpful for debugging problems in them.

Author: Zhihan Yang
"""

import os
import json
import argparse

parser = argparse.ArgumentParser(description="Prettify JSON files in a list of directories.")

parser.add_argument(
    "directories",  # name of the argument
    type=str,   
    nargs="+"  # collect all entered items into a list
)

args = parser.parse_args()

for d in args.directories:
    for filename in [filename for filename in os.listdir(d) if filename.split('.')[-1] == 'json']:
        
        path_to_json = os.path.join(d, filename)
        
        with open(path_to_json, 'r') as json_f:
            temp = json.load(json_f)

            # It's weird that toy configs in PosterConfigs do not have this field
            # yet the ones generated using python $CODE/master_comparison_data.py $CONFIG/MasterComparisonConfig_Poster_edited.json
            # do have it. Since it does not affect code, I've decided to simply remove it.

            if 'randConditions' in temp.keys(): 
                del temp['randConditions']
        
        with open(path_to_json, 'w') as json_f:
            json.dump(temp, json_f, indent=4, sort_keys=True)  # indentation and key sorting; this is where the "prettify-cation" magic happens!