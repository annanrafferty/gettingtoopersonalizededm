"""
Run the config files generated, prettified and verified by the other
three scripts: generate_reg_configs.py, prettify_json.py and verify_reg_configs.py.

This script requires the path of a so-called meta config as its command line
argument. See the "parsing" section for more details. For an example of a meta config,
see configFiles/MetaConfigs. If this example has been deleted or overwritten, there is a
perhaps outdated example in utility_scripts/archive called
C2_C5_C10_Crossover_Main_None_L1_L2_L5_ILoff_IL1.json.

If you want to use this script for your own purpose, you are most likely required
# to only change PARAM_NAME_PRIORITY_LIST and PARAM_NAME_TO_PREFIX in the "user
# settings" section.

This script is not designed for ASSISTments.

Author: Zhihan Yang
"""

# ===== imports =====

import os
import argparse
import json
import itertools
import numpy as np

# ===== user settings =====

# Here, a "parameter" (or "PARAM, or "param") refers to, for example, how many contextual
# variables (represented by C), type of underlying model (Crossover, Main or None),
# minority proportion (represented by G), lambda values (e.g., 1, 1K and 1M) and etc.

# Param names specified in a meta_json must be contained in here.
# Note that the order by which items appear in this list follows
# the order they appear in config names - this is crucial.
# Note that "Type" represents Crossover, Main or None.
PARAM_NAME_PRIORITY_LIST = ["C", "G", "Type", "L", "IL"]

# Smaller index corresponds to higher priority.
PARAM_NAME_TO_PRIORITY_DICT = {param_name : i for i, param_name in enumerate(PARAM_NAME_PRIORITY_LIST)}

# Param names specified in a meta_json must be contained in here as keys.
# This dictionary is created because all PARAMs must have a name, and yet
# sometimes a PARAM's name does actually show up, e.g., for "Type".
PARAM_NAME_TO_PREFIX = { 
	"C" : "C",
	"G" : "G",
	"Type" : "",
	"L" : "L",
	"IL" : "IL"
}

# ===== parsing =====

# A combination of values of parameters specify an "experiment".

parser = argparse.ArgumentParser()
parser.add_argument('path_to_meta_json', type=str, help="the path to the meta JSON file containing the settings for all experiments to run")
parser.add_argument('testing', type=int, help="if true, then prints out the complete path to all configs that will be run but does not actually run them")
args = parser.parse_args()
with open(args.path_to_meta_json, "r") as json_f:
	meta_json = json.load(json_f)

param_ranges_dict = meta_json['param_ranges']
run_settings_dict = meta_json['run_settings']

for key in param_ranges_dict.keys():
	assert key in PARAM_NAME_PRIORITY_LIST
	assert key in PARAM_NAME_TO_PREFIX.keys()

# ===== obtain experiments from param_ranges_dict =====

# Here, "experiments" is an iterator equivalent to the cartesian product of all ranges, one per parameter specified.

param_names = list(param_ranges_dict.keys())
param_names_sorted = sorted(param_names, key=PARAM_NAME_TO_PRIORITY_DICT.get, reverse=False)  # param_names with high priority get sorted to the left

param_ranges = []  # now a list rather than a dictionary
for param_name in param_names_sorted:
	param_ranges.append(param_ranges_dict[param_name])

experiments = itertools.product(*param_ranges)

# ===== run all experiments specified in experiments =====

num_experiments = np.prod([len(param_range) for param_range in param_ranges])
for i, exp in enumerate(experiments):

	# construct config_name from scratch
	config_name = ""
	for param_name, param_val in zip(param_names_sorted, exp):
		if not (param_name == 'IL' and param_val == -1):
			config_name += f"{PARAM_NAME_TO_PREFIX[param_name]}{param_val}_"
	config_name = config_name[:-1]
	config_name += '.json'

	# construct the command line code from scratch
	path_to_config = os.path.join(run_settings_dict['config_directory'], config_name)
	command_line_code = f"python3 {run_settings_dict['path_to_generate_comparison_data']} {path_to_config} {run_settings_dict['num_cores']}"

	if args.testing:
		print(command_line_code)
	else:
		os.system(f"echo Running the {i+1} / {num_experiments} experiment.")
		os.system(command_line_code)




