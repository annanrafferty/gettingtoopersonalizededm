"""
The purpose of this script is to, using toy dataset configs 
in configsFiles/PosterConfigsComplete as templates, generate configs
with additional regularization settings in configsFiles/RegConfigs.

This script requires no command line arguments. Please change the
value of source_dir and target_dir in the "user settings" section to
use this script in your own account.

This script is not designed for ASSISTments.

Author: Zhihan Yang
"""

# ========== imports ==========

# First, we import three modules.

import os
import json
import itertools

# ========== user settings ==========

"""
There are a few things the script needs to know before it can do its work:

- source_dir: the directory containing toy configs
- target_dir: the directory for emitting toy configs with additional regularization settings

- C: a list containing all the numbers of contextual variables to use
- G: a list containing all the minority percentages to use
- TYPE: a list containing all the optimal-arm variations to use

- DATA_DIR: simulation data will be stored in /disk2/contextualBanditSimsNoEffect/DATA_DIR
- LAMBDA: a list containing all the lambda values to use
- INTERCEPT_LAMBDA: a list containing all the intercept lambda values to use

"""

# please change the directory accordingly
source_dir = '/Users/zhaobinli/PycharmProjects/banditalgorithms/configFiles/PosterConfigsComplete'
target_dir = '/Users/zhaobinli/PycharmProjects/banditalgorithms/configFiles/PyramidalC2'

C = [2, 10]
G = [50]
TYPE = ['Crossover', 'Main', 'None']

DATA_DIR = 'PyramidalC2'
LAMBDA = [1, 50, 1000]
INTERCEPT_LAMBDA = [-1]  # -1 represents None
ONLINE = [0]
DOW_SCHEDULER_NAME = ['pyramidal']
# ========== utility functions ===========

"""
Defining some utility functions makes the code later on a bit cleaner.
"""

def load_json(path_to_config:str) -> dict:
    """Read in a JSON file using its complete path."""
    with open(path_to_config, 'r') as json_f:
        config = json.load(json_f)
    return config

def save_json(dictionary:dict, path_to_config:str) -> None:
    """Save a dictionary in a JSON file located at some path."""
    with open(path_to_config, 'w+', encoding='windows-1252') as json_f:
        json.dump(dictionary, json_f)

def use_config(config_name:str) -> bool:
    """Check whether we should use a given config as template."""

    # We will follow through an example.
    # config_name: "C5_G50_Crossover_offline_L1000_pyramidal.json"

    C_and_val, G_and_val, TYPE_val_and_posfix = config_name.split('_')

    # C_and_val: "C5" 
    # G_and_val: "G50"
    # TYPE_val_and_postfix: "Crossover.json"
    
    C_val = int(C_and_val[1:])  # C_val: 5
    G_val = int(G_and_val[1:])  # G_val: 50
    TYPE_val = TYPE_val_and_posfix.split('.')[0]  # TYPE_val: "Crossover"
    
    if (C_val in C) and (G_val in G) and (TYPE_val in TYPE):
        return True
    else:
        return False

def json_only(filenames:list) -> list:
    return [fn for fn in filenames if fn.split('.')[-1] == 'json']

# ========== main code ===========

for config_name_with_postfix in json_only(os.listdir(source_dir)):
    os.makedirs(target_dir, exist_ok=True)

    if use_config(config_name_with_postfix):
        for lamb, intercept_lamb, onl, dsn in itertools.product(LAMBDA, INTERCEPT_LAMBDA, ONLINE, DOW_SCHEDULER_NAME):

            config = load_json(os.path.join(source_dir, config_name_with_postfix))  # this line must be inside the loop; otherwise changes get carried over

            # === add fields ====

            # add lambda
            config['lambda'] = int(lamb)

            # add online
            config['online'] = int(onl)

            # add dsn
            config['dow_scheduler_name'] = str(dsn)

            # add intercept_lambda if it's not None
            if intercept_lamb != -1:
                config['intercept_lambda'] = int(intercept_lamb)
            
            # === change config file name ===

            config_name_without_postfix = config_name_with_postfix.split('.')[0]

            if intercept_lamb != -1:  # intercept has its own degree of regularization
                # IL stands for "intercept lambda"
                new_config_name_without_postfix = config_name_without_postfix + f'_L{int(lamb)}' + f'_IL{int(intercept_lamb)}' 
            else:                     # intercept uses the same regularization as everyone else
                new_config_name_without_postfix = config_name_without_postfix + f'_L{int(lamb)}'
            
            new_config_name_with_postfix = new_config_name_without_postfix + '.json'

            # === change where simulated data will be stored ===

            config["fileName"] = new_config_name_with_postfix
            config["conditionsToActionsFile"] = \
                f"/disk2/contextualBanditSimsNoEffect/{DATA_DIR}/{new_config_name_without_postfix}/CompareBanditConditionsToActions_NNUMSTUDENTS_TNUMTRIALS"
            config['outfilePrefix'] = \
                f"/disk2/contextualBanditSimsNoEffect/{DATA_DIR}/{new_config_name_without_postfix}/CompareBanditActionsOut_TYPE_NNUMSTUDENTS_TNUMTRIALS"
            config['rewardFile'] = \
                f"/disk2/contextualBanditSimsNoEffect/{DATA_DIR}/{new_config_name_without_postfix}/CompareBanditRewardFile_NNUMSTUDENTS_TNUMTRIALS"

            # === finally, save the config file as a JSON in target_dir ===

            new_config_path = os.path.join(target_dir, new_config_name_with_postfix)

            save_json(config, new_config_path)

