#!/bin/bash
# make sure you always put $f in double quotes to avoid any nasty surprises i.e. "$f"
# to run in utility_scripts directory
# e.g. bash run_toy_in_dir.sh ../configFiles/PyramidalC2/*.json
for f in $@
do
  echo "Processing $f file..."
  python3 ../src/louie_experiments/generate_comparison_data.py "$f"
done