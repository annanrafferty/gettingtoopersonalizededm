% Model specfication
modelspec = 'linear';% one way interactions
%modelspec = 'interactions';% Two way interactions
%modelspec = 'outcomes ~ 1 + samplingType + rewardType + effectSize + sampleSize + rewardType_normal*samplingType_bandit';

% Read in a data table with information about power
% Assumes columns: samplingType, rewardType, effectSize, sampleSize, totalSims, propSig
% sampling type, and reward type should have two unique values - will be converted to 0/1
% effect size should be 0.2, 0.5, 0.8 or 0.1, 0.3, 0.5 -> these will be converted to 1/2/3
% sample size should be as a coefficient of m, the number of samples expected to get .8 power
% with equally balanced conditions
powerTable = importPowerTableNoPriors('PowerTableMiddleArmSims.txt');
lastPredictorColumn = 4;

% Not clear that we need to expand to a full data table, but can't determine how to
% set the outcome variable to be success counts with a data table otherwise
totalSamples = sum(powerTable.totalSims);
fullSampleTable = powerTable(1:1,1:lastPredictorColumn);
outcomes = zeros(totalSamples,1);
simIndex = 1;
for i = 1:size(powerTable,1)
    simsForRow = powerTable{i,{'totalSims'}};
    curRow = powerTable(i,1:lastPredictorColumn);
    curRow.effectSize = convertEffectSize(curRow.effectSize, curRow.rewardType);
    curPredictors = repmat(curRow, simsForRow, 1);
    fullSampleTable(simIndex:(simIndex+simsForRow-1),:) = curPredictors;
    outcomes(simIndex:(simIndex+simsForRow-1)) = [ones(powerTable{i,{'propSig'}}*simsForRow,1) ;
        zeros(int16((1-powerTable{i,{'propSig'}})*simsForRow),1)];
    simIndex = simIndex + simsForRow;
end
% logisticTable = powerTable(:,1:lastPredictorColumn);
% outcomeCounts = powerTable.propSig .* powerTable.totalSims;
% logisticTable.outcomes = [outcomeCounts, powerTable.totalSims];
% % mdl = fitglm(logisticTable,'Distribution','binomial')
fullSampleTable.outcomes = outcomes;
mdl = fitglm(fullSampleTable, modelspec, 'Distribution','binomial')

% Should do the same type of thing with modeling type s errors and overall reward; also should consider prior
% as a predictor, and look at 0 effect size

function[convertedEffectSize] = convertEffectSize(effectSize, rewardType)
    convertedEffectSize = 0;
    if rewardType == 'binary'
       switch effectSize
       case .1
           convertedEffectSize = 1;
       case .3
          convertedEffectSize = 2;
       case .5
          convertedEffectSize = 3;
       otherwise
           disp(['Unknown effect size ' num2str(effectSize) ' for reward type ' rewardType]);
       end
   elseif rewardType == 'normal'
       switch effectSize
       case .2
           convertedEffectSize = 1;
       case .5
          convertedEffectSize = 2;
       case .8
          convertedEffectSize = 3;
       otherwise
           disp(['Unknown effect size ' num2str(effectSize) ' for reward type ' rewardType]);
       end
   else
      disp(['Unknown reward type: ' rewardType]) 
   end
end