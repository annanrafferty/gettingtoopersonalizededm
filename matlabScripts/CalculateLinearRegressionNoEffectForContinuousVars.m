% Assumes we're doing stat tests in a case where there is no actual effect (true effect size = 0)
% Model specification
modelspec = 'linear';% one way interactions
%modelspec = 'interactions';% Two way interactions


% Read in a fully rolled out data table with one simulation per row

data = importOneSimPerRowTable('CombinedZeroEffectSizeDF.csv');

% Print some sanity checks for what we care about
disp(['Total simulations: ' num2str(size(data,1))])
disp(['Total bandit simulations: '  num2str(sum(data.sampling_type == 'bandit'))])
disp(['Total uniform simulations: '  num2str(sum(data.sampling_type == 'uniform'))])
disp(['Total prior between simulations: '  num2str(sum(data.prior_type == 'priorBetween'))])
disp(['Total prior higher simulations: '  num2str(sum(data.prior_type == 'priorAbove'))])
disp(['Total prior lower simulations: '  num2str(sum(data.prior_type == 'priorBelow'))])
disp(['Total binary reward simulations: '  num2str(sum(data.reward_type == 'binary'))])
disp(['Total normal reward simulations: '  num2str(sum(data.reward_type == 'normal'))])


% TODO: Try analyses below with 4 categories per sim (combining sampling type and prior), as well as restricting to just normal case to test variance
useFourCategoriesPerSim = 1;
testVariance = 0;
compareJustUniformAndPriorBetween = 1;
runInitialTests = 0;


if(useFourCategoriesPerSim == 1)
    runFourCategoriesPerSimTests(data, modelspec)
end

if(testVariance == 1)
    testEffectOfVariance(data, modelspec, 1)
end

if(runInitialTests == 1)
    % Variables we care about - outcome should be final row
    % possibilities: num_steps,sim,sample_size_1,sample_size_2,mean_1,mean_2,total_reward,ratio,power,actual_es,stat,pvalue,df,prior_type,sampling_type,actual_arm_variance
    actualESVars = {'sampling_type', 'reward_type','num_steps','prior_type','actual_es'};
    disp('Model for Estimating Effect Size')
    mdl = fitglm(data(:,actualESVars), modelspec)
    
    % TODO: modify this to focus on average reward per step and to subtract off the mean of the two arms
    meanPriorAboveNormal = (-0.5 + -1) / 2;
    meanPriorBelowNormal = (0.5 + 1) / 2;
    data.avg_reward_per_step = data.total_reward ./ data.num_steps;
    data{data.prior_type == 'priorAbove' & data.reward_type == 'normal','avg_reward_per_step'} = data{data.prior_type == 'priorAbove' & data.reward_type == 'normal','avg_reward_per_step'} - meanPriorAboveNormal;
    data{data.prior_type == 'priorBelow' & data.reward_type == 'normal','avg_reward_per_step'} = data{data.prior_type == 'priorBelow' & data.reward_type == 'normal','avg_reward_per_step'} - meanPriorBelowNormal;
    
    rewardVars = {'sampling_type', 'reward_type','num_steps','prior_type','avg_reward_per_step'};
    disp('Model for Estimating Reward')
    mdl = fitglm(data(:,rewardVars), modelspec)
    
    % Modify to be error on mean (subtract off true mean)
    mean1Vars = {'sampling_type', 'reward_type','num_steps','prior_type','mean_1'};
    disp('Model for Estimating Mean of Condition 1')
    mdl = fitglm(data(:,mean1Vars), modelspec)
    
    % Modify to be error on mean (subtract off true mean)
    mean1Vars = {'sampling_type', 'reward_type','num_steps','prior_type','mean_1'};
    disp('Model for Estimating Mean of Condition 2')
    mdl = fitglm(data(:,mean1Vars), modelspec)
end
    
% Should do the same type of thing with modeling type s errors and overall reward; also should consider prior
% as a predictor, and look at 0 effect size

function[] = runFourCategoriesPerSimTests(data, modelspec)
    data = getSamplingTypeFourCats(data);
    actualESVars = {'sampling_type', 'reward_type','num_steps','actual_es'};
    runLinearGLMTest(data, modelspec,'Estimate actual es with 4 category predictor', actualESVars)
    
    testEffectOnFalseSigRate(data, modelspec)
end

function[data] = getSamplingTypeFourCats(data)
    data{data.sampling_type == 'bandit', 'sampling_type'} = data{data.sampling_type == 'bandit', 'sampling_type'} .* data{data.sampling_type == 'bandit', 'prior_type'};% concatenate the two
    data.sampling_type = removecats(data.sampling_type);  
end

function[] = testEffectOnFalseSigRate(data, modelspec)
    data.is_sig = (data.pvalue <= .05);
    falseSigVars = {'sampling_type','reward_type','num_steps','is_sig'};
    disp('Model testing false significance rates')
    mdl = fitglm(data(:,falseSigVars), modelspec, 'Distribution','binomial')
    
end
    

function[] = testEffectOfVariance(data, modelspec, useFourCats)
    if(useFourCats)
        data = getSamplingTypeFourCats(data);
    end
    actualESVars = {'sampling_type', 'actual_arm_variance','num_steps','actual_es'};
    runLinearGLMTest(data(data.reward_type == 'normal',:), modelspec,['Estimate actual es with arm variance and 4 category predictor ' num2str(useFourCats)], actualESVars)  
end

function[] = runLinearGLMTest(data, modelspec, description, variables)
    disp(description)
    mdl = fitglm(data(:,variables), modelspec) 
end


function[convertedEffectSize] = convertEffectSize(effectSize, rewardType)
    convertedEffectSize = 0;
    if rewardType == 'binary'
       switch effectSize
       case .1
           convertedEffectSize = 1;
       case .3
          convertedEffectSize = 2;
       case .5
          convertedEffectSize = 3;
       otherwise
           disp(['Unknown effect size ' num2str(effectSize) ' for reward type ' rewardType]);
       end
   elseif rewardType == 'normal'
       switch effectSize
       case .2
           convertedEffectSize = 1;
       case .5
          convertedEffectSize = 2;
       case .8
          convertedEffectSize = 3;
       otherwise
           disp(['Unknown effect size ' num2str(effectSize) ' for reward type ' rewardType]);
       end
   else
      disp(['Unknown reward type: ' rewardType]) 
   end
end