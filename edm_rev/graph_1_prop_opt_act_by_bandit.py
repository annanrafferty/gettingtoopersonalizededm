import os

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

pd.options.mode.chained_assignment = None
import string

save_dir = '/disk2/contextualBanditSimsNoEffect/edm_stats/figs'

save_name = os.path.join(save_dir, 'AllEff250Correct.parquet.gzip')
df = pd.read_parquet(save_name)

df = df.loc[(df['MinGrpSize'] == 50)]
df = df.loc[(df['NumConVars'].isin([1, 3, 5, 10]))]
df = df.loc[(df['EffType'].isin(['Baseline', 'Universal1', 'Personalized']))]

# 1000 trials * 2 bandits * 4 contextual variables * 3 effect types
assert len(df) == (1000 * 2 * 4 * 3)

df['Type'].replace(['ModContextual', 'NonContextual'], ['Contextual', 'Non Contextual'], inplace=True)

df['EffType'].replace(['Baseline', 'Universal1', 'Personalized'],
                      ['Baseline', 'Universal Optimal Action (1)', 'Personalized Optimal Action'], inplace=True)

bandit_ord = ['Non Contextual', 'Contextual']
eff_ord = ['Baseline', 'Universal Optimal Action (1)', 'Personalized Optimal Action']

sns.set()
sns.set(context='paper', style="whitegrid", font='Times', font_scale=5, palette="colorblind")
g = sns.catplot(data=df, y='AvgCorrect', x='Type', hue='NumConVars', col='EffType',
                kind="swarm", dodge=True, height=12, aspect=1.125, col_order=eff_ord, order=bandit_ord)

for i, label in enumerate(['Baseline', 'Universal Optimal Action (1)', 'Personalized Optimal Action']):
    g.axes[0, i].set_title(label)

for i, label in enumerate(['', 'Bandit Type', '']):
    g.axes[0, i].set_xlabel(label, weight='bold')

for i in range(len(g.axes[0])):
    g.axes[0, i].text(0, 1, string.ascii_lowercase[i] + ')', transform=g.axes[0, i].transAxes,
                      weight='bold', va='bottom', ha='left')

g.set_ylabels("Proportion of Optimal Actions", weight='bold') \
    .set(ylim=(0, 1)) \
    .despine(left=True)

g._legend.set_title("No. of\nContextual\nVariables")

plt.show()

g.savefig(os.path.join(save_dir, 'NumConVars.pdf'))
