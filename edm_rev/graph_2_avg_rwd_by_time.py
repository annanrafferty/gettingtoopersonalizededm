import os

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

pd.options.mode.chained_assignment = None
import matplotlib.patches as mpatches
import matplotlib.ticker as ticker
import string

save_dir = '/disk2/contextualBanditSimsNoEffect/edm_stats/figs'

save_name = os.path.join(save_dir, 'AllEffRangesRew.parquet.gzip')
df = pd.read_parquet(save_name)

df = df.loc[(df['NumConVars'].isin([1, 3, 5, 10]))]
df = df.loc[(df['MinGrpSize'] == 50)]
df = df.loc[(df['EffType'] == 'Baseline')]

# 1000 trials * 2 bandits * 4 contextual variables * 4 ranges
assert len(df) == (1000 * 2 * 4 * 4)

df_noncon_means = df.loc[df['Type'] == 'NonContextual'].groupby('Range')['AvgReward'].mean()

df = df.loc[df['Type'] == 'ModContextual']

bandit_ord = ['NonContextual', 'ModContextual']
eff_ord = ['Baseline', 'Universal1', 'Personalized']
range_ord = ['1-50', '1-250', '1-1000', '951-1000']
noncon_color = sns.color_palette()[0]
con_color = sns.color_palette()[1]

scale = 1.5

sns.set()
sns.set(context='paper', style="whitegrid", font='Times', font_scale=5 * scale, palette="colorblind",
        rc={"lines.linewidth": 10 * scale})

g = sns.catplot(data=df, y='AvgReward', x='NumConVars', col='Range',
                kind="bar", ci=68, capsize=0.1 * scale, errwidth=3 * scale, height=15, aspect=1 / scale,
                col_order=range_ord, palette=[con_color])

for i, label in enumerate(range_ord):
    g.axes[0, i].set_title(label)
    line = g.axes[0, i].axhline(df_noncon_means.loc[label], color=noncon_color, label='Non Contextual')

for i, label in enumerate(['', 'No. of Contextual Variables', '', '']):
    g.axes[0, i].set_xlabel(label, weight='bold')

for i in range(len(g.axes[0])):
    g.axes[0, i].text(0, 1, string.ascii_lowercase[i] + ')', transform=g.axes[0, i].transAxes,
                      weight='bold', va='bottom', ha='left')

g.axes[0, 1].xaxis.set_label_coords(1.05, -0.12)

g.set_ylabels("Average Reward\nper Student", weight='bold') \
    .set(ylim=(.5, .61)) \
    .despine(left=True)

g.axes[0, 0].yaxis.set_major_locator(ticker.MultipleLocator(0.02))

patch = mpatches.Patch(color=con_color, label='Contextual')
g.fig.legend(handles=[line, patch], bbox_to_anchor=(0.25, -.02,), loc='lower left', ncol=2, borderaxespad=0.,
             frameon=False)

g.fig.subplots_adjust(top=0.8)
g.fig.suptitle('Baseline')

g.fig.subplots_adjust(wspace=.02, hspace=.02)

plt.show()

g.savefig(os.path.join(save_dir, 'NumConVarsRanges.pdf'))
