import os
import sys

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

pd.options.mode.chained_assignment = None
import string

save_dir = '/disk2/contextualBanditSimsNoEffect/edm_stats/figs'

save_name = os.path.join(save_dir, 'AllEff250Correct.parquet.gzip')
df = pd.read_parquet(save_name)

df = df.loc[df['NumConVars'].isin([1, 5])]
df = df.loc[df['Type'] == 'ModContextual']
#df = df.loc[(df['EffType'].isin(['Baseline', 'Universal1', 'Personalized']))]

df["Balanced"] = (df['Grp0AvgCorrect'] + df['Grp1AvgCorrect']) / 2

df = df.melt(id_vars=['Type', 'NumConVars', 'MinGrpSize', 'EffType'],
             value_vars=['AvgCorrect', 'Balanced', 'Grp0AvgCorrect', 'Grp1AvgCorrect'], var_name='Group',
             value_name='Correct')
df.to_csv(os.path.join(save_dir, 'AllEff250CorrectDataTable.csv'))
sys.exit()
# 1000 trials * 2 contextual variables * 5 group sizes * 3 effect types * 4 correct statistics
assert len(df) == (1000 * 2 * 5 * 3 * 4)

eff_ord = ['Baseline', 'Universal1', 'Personalized']
group_ord = ['AvgCorrect', 'Balanced', 'Grp1AvgCorrect', 'Grp0AvgCorrect']

sns.set()
sns.set(context='paper', style="whitegrid", font='Times', font_scale=5, palette="colorblind",
        rc={"lines.linewidth": 10, 'lines.markersize': 30, 'legend.markerscale': 1.2, 'legend.handlelength': 3})
g = sns.relplot(data=df, x="MinGrpSize", y="Correct", hue="Group", hue_order=group_ord, style="NumConVars",
                kind="line", ci=68, markers=True, sizes=20, height=17, aspect=1 / 1.125, col='EffType',
                col_order=eff_ord)

for i, label in enumerate(['Baseline', 'Universal Optimal Action (1)', 'Personalized Optimal Action']):
    g.axes[0, i].set_title(label)

for i, label in enumerate(['', 'Minority Group Size', '']):
    g.axes[0, i].set_xlabel(label, weight='bold')

for i in range(len(g.axes[0])):
    g.axes[0, i].text(0, 1, string.ascii_lowercase[i] + ')', transform=g.axes[0, i].transAxes,
                      weight='bold', va='bottom', ha='left')

new_legends = ['Success Rate', 'Overall', 'Balanced', 'Majority', 'Minority', '\nNo. of\nContextual\nVariables']
for t, l in zip(g._legend.texts, new_legends):
    t.set_text(l)

g.set_ylabels("Proportion of Optimal Actions", weight='bold') \
    .set(ylim=(0.45, .90)) \
    .set_xticklabels([0, 10, 20, 30, 40, 50]) \
    .despine(left=True)

plt.show()

g.savefig(os.path.join(save_dir, 'MinGrpSize1v5.pdf'))
