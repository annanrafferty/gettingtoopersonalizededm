import os

import numpy as np
import pandas as pd
from statsmodels.formula.api import ols
from statsmodels.stats.anova import anova_lm

pd.options.mode.chained_assignment = None


# Maths Ref: https://en.wikipedia.org/wiki/Effect_size#Cohen's_d
# Code Ref: https://machinelearningmastery.com/effect-size-measures-in-python/

# function to calculate Cohen's d for independent samples
def cohend(d1, d2):
    # calculate the means of the samples
    u1, u2 = np.mean(d1), np.mean(d2)

    # calculate the size of samples
    n1, n2 = len(d1), len(d2)
    # calculate the variance of the samples
    s1, s2 = np.var(d1, ddof=1), np.var(d2, ddof=1)
    # calculate the pooled standard deviation
    s = np.sqrt(((n1 - 1) * s1 + (n2 - 1) * s2) / (n1 + n2 - 2))

    # calculate the effect size
    return (u1 - u2) / s


def get_statistics(eff_type, df):
    df = df.loc[df['EffType'] == eff_type]
    assert len(df) == (1000 * 2 * 7)

    def_ref = 'Non Contextual'
    alt_ref = 'Contextual'

    # def_ref - alt_ref
    eff_size = cohend(df.loc[df['Type'] == def_ref]['AvgCorrect'], df.loc[df['Type'] == alt_ref]['AvgCorrect'])
    d["Cohen's $d$"].append(abs(eff_size))

    d['Superior bandit'].append(def_ref if eff_size > 0 else alt_ref)

    # inferior ref as base
    model = ols(
        "AvgCorrect ~ C(Type, Treatment(reference='{}')) * NumConVars".format(def_ref if eff_size < 0 else alt_ref),
        df).fit()

    coef = model.params[1]
    d["$|b|$"].append(coef)

    df_ci = model.conf_int().round(3)
    lower_bound, upper_bound = df_ci[0][1], df_ci[1][1]
    d['95\% CI'].append('[{}, {}]'.format(lower_bound, upper_bound))

    df_anova = anova_lm(model).round()
    f = df_anova['F'][0]
    d['$F(1, 13996)$'].append(f)

    p = df_anova['PR(>F)'][0]
    if p < 0.001:
        d['$p$'].append('$< .001$')
    else:
        d['$p$'].append(p)


save_dir = '/disk2/contextualBanditSimsNoEffect/edm_stats/figs'

save_name = os.path.join(save_dir, 'AllEff250Correct.parquet.gzip')
df = pd.read_parquet(save_name)

df = df.loc[(df['MinGrpSize'] == 50)]

# 1000 trials * 2 bandits * 7 contextual variables * 6 effect types
assert len(df) == (1000 * 2 * 7 * 6)

df['Type'].replace(['ModContextual', 'NonContextual'], ['Contextual', 'Non Contextual'], inplace=True)

eff_types = ['Baseline', 'Universal1', 'Universal2',
             'Universal3', 'Universal4', 'Personalized']

columns = ['Superior bandit', '$|b|$', '95\% CI', '$F(1, 13996)$', '$p$', "Cohen's $d$"]
d = {k: [] for k in columns}
index = ['Baseline',
         'Universal Optimal Action (1)',
         'Universal Optimal Action (2)',
         'Universal Optimal Action (3)',
         'Universal Optimal Action (4)',
         'Personalized Optimal Action']

for eff_type in eff_types:
    get_statistics(eff_type, df)

stats_table = pd.DataFrame(data=d, index=index, columns=columns)

print(stats_table)

print(stats_table.to_latex(float_format="%.3f", escape=False))
