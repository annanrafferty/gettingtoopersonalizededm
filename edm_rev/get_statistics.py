import glob as glob
import math
import os
import sys
import time
from functools import partial
from multiprocessing import Pool

import numpy as np
import pandas as pd

pd.options.mode.chained_assignment = None


def sigmoid(x):
    return 1 / (1 + math.exp(-x))


# Maths Ref: https://en.wikipedia.org/wiki/Effect_size#Cohen's_d
# Code Ref: https://machinelearningmastery.com/effect-size-measures-in-python/

# function to calculate Cohen's d for independent samples
def cohend(d1, d2):
    # calculate the means of the samples
    u1, u2 = np.mean(d1), np.mean(d2)

    # calculate the size of samples
    n1, n2 = len(d1), len(d2)
    # calculate the variance of the samples
    s1, s2 = np.var(d1, ddof=1), np.var(d2, ddof=1)
    # calculate the pooled standard deviation
    s = np.sqrt(((n1 - 1) * s1 + (n2 - 1) * s2) / (n1 + n2 - 2))

    # calculate the effect size
    return (u1 - u2) / s


def calc_df_dir(directory, calc_func, num_students=None, search_str_list=["ModContextual", "NonContextual"]):
    '''Combine contextual and noncontextual dataframes, calculating the value
    (indicated by calc_func) in the process'''

    print(f"========== Running {directory} ==========")
    start = time.time()

    num_con_vars, min_grp_size, eff_type = os.path.basename(directory).split('_', 2)

    # restrict simulations
    if num_con_vars not in ['C1', 'C2', 'C3', 'C5', 'C7', 'C8', 'C10']:
        return
    if min_grp_size not in ['G10', 'G20', 'G30', 'G40', 'G50']:
        return
    if eff_type not in ['Baseline', 'Personalized', 'Universal1', 'Universal2', 'Universal3', 'Universal4']:
        return

    dir_df_list = []

    for search_str in search_str_list:
        f = glob.glob(os.path.join(directory, '{}*.parquet.gzip'.format(search_str)))

        df = pd.read_parquet(f[0], columns=['Trial', 'n', 'contextualVariable0', 'ObservedRewardofAction',
                                            'MatchesOptimalExpectedAction'])
        gb = df.groupby('Trial')
        df_list = [grp for _, grp in gb]

        if num_students is None:
            df_list = [calc_func(df) for df in df_list]
        else:
            df_list = [calc_func(df[:num_students]) for df in df_list]

        df = pd.concat(df_list)
        df["Type"] = search_str

        dir_df_list.append(df)

    df = pd.concat(dir_df_list)

    df['NumConVars'] = int(num_con_vars[1:])
    df['MinGrpSize'] = int(min_grp_size[1:])
    df['EffType'] = eff_type

    df = df.astype({'NumConVars': 'int32', 'MinGrpSize': 'int32', 'Type': 'category', 'EffType': 'category'})

    end = time.time()
    print(f'Duration: {round((end - start) / 60, 2)} minutes')

    return df


def calc_avg_statistic(df, statistic, balanced=False):
    """
    statistic (str): either 'reward' (average reward by student)
    or 'correct' (average Proportion of Optimal Actions by student)
    balanced (boolean): balanced success rate (both groups are weighed equally) or not
    """

    statistic = statistic.lower()

    if statistic not in ['reward', 'correct']:
        print('{} is not a valid statistic')
        return
    elif statistic == 'reward':
        col_name = 'ObservedRewardofAction'
    else:
        col_name = 'MatchesOptimalExpectedAction'

    data = {}
    df = df[['contextualVariable0', col_name]]

    # average by group
    df_group_means = df.groupby(['contextualVariable0'], sort=True)[col_name].mean()
    data['Grp0Avg{}'.format(statistic.capitalize())] = df_group_means[0]
    data['Grp1Avg{}'.format(statistic.capitalize())] = df_group_means[1]

    # average overall
    if balanced:
        data['Avg{}'.format(statistic.capitalize())] = df_group_means.mean()

    else:
        data['Avg{}'.format(statistic.capitalize())] = df[col_name].mean()

    return pd.DataFrame(data=[data])


def calc_avg_reward(df, balanced=False):
    return calc_avg_statistic(df, 'reward', balanced)


def calc_avg_correct(df, balanced=False):
    return calc_avg_statistic(df, 'correct', balanced)


def calc_avg_statistic_by_range(df, statistic, student_range=[(1, 50), (1, 250), (1, 1000), (951, 1000)]):
    df_list = []
    for start, end in student_range:
        try:
            df_slice = calc_avg_statistic(df[start - 1: end - 1], statistic)
            df_slice['Range'] = '{}-{}'.format(start, end)
            df_list.append(df_slice)
        except KeyError:
            continue

    return pd.concat(df_list)


def calc_avg_reward_by_range(df, student_range=[(1, 50), (1, 250), (1, 1000), (951, 1000)]):
    return calc_avg_statistic_by_range(df, 'reward', student_range)


def calc_avg_correct_by_range(df, student_range=[(1, 50), (1, 250), (201, 250), (1, 1000), (951, 1000)]):
    return calc_avg_statistic_by_range(df, 'correct', student_range)


def calc_rolling_reward(df, window=50):
    return pd.DataFrame(data={'RollingReward': df["ObservedRewardofAction"].rolling(window).mean()})


def calc_cum_reward(df):
    df = df[['n', 'contextualVariable0', "ObservedRewardofAction"]]
    df['CumReward'] = df.groupby(['contextualVariable0'])["ObservedRewardofAction"].cumsum()
    return df


def main(parent_dir, calc_func, save_name, num_students=None, num_cores=20):
    '''get statistics for all child directories in parent_dir'''

    directories = []
    for child in next(os.walk(parent_dir))[1]:
        directories.append(os.path.join(parent_dir, child))

    # df_list = []
    # for directory in directories:
    #     df_list.append(calc_df_dir(directory, calc_func=calc_func, num_students=num_students))

    with Pool(num_cores) as pool:
        df_list = pool.map(partial(calc_df_dir, calc_func=calc_func, num_students=num_students), directories)

    df_list = [df for df in df_list if df is not None]

    df = pd.concat(df_list)

    df.to_parquet(save_name, compression='gzip')


if __name__ == "__main__":
    parent_dir = '/disk2/contextualBanditSimsNoEffect/edm_rev'

    save_dir = '/disk2/contextualBanditSimsNoEffect/edm_stats/figs'
    os.makedirs(save_dir, exist_ok=True)
    
    calc_func = calc_avg_correct_by_range
    save_name = os.path.join(save_dir, 'AllEffRangesCorrect.parquet.gzip')
    main(parent_dir, calc_func, save_name, num_students=None, num_cores=30)
    #sys.exit()
    
    calc_func = calc_avg_correct
    save_name = os.path.join(save_dir, 'AllEff250Correct.parquet.gzip')
    main(parent_dir, calc_func, save_name, num_students=250)

    calc_func = calc_avg_reward_by_range
    save_name = os.path.join(save_dir, 'AllEffRangesRew.parquet.gzip')
    main(parent_dir, calc_func, save_name, num_students=None, num_cores=30)
