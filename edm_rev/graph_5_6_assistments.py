# Overhead

# mpl.use('Agg')
import seaborn as sns
from matplotlib import pyplot as plt

sns.set(context='paper', style="whitegrid")
import pandas as pd

pd.options.mode.chained_assignment = None

from functools import partial
import time
# mpl.use('Agg')
import seaborn as sns
sns.set(context='paper', style="whitegrid")
import numpy as np
import pandas as pd

import glob as glob
import os
import sys
import itertools
from scipy.special import expit
from IPython.display import display
pd.options.mode.chained_assignment = None
import concurrent

# seaborn swarmplot prints a lot of warnings, hinders readability
import warnings
warnings.filterwarnings("ignore")

# Notebook constants

PROBLEM_SET_LIST = ["293151", "263057"]
REWARD_SCHEMES = ["complete", "ProblemCount"]
SIM_TYPE_LIST = ['Outcomes', 'Parameters']
TREATMENTS = ['Contextual', 'Non Contextual']

EXPERIMENT_ID_HEADER = 'problem_set'
CONDITION_HEADER = 'Condition'
ARM_IDENTIFIERS = ['C', 'E']
EXPERIENCED_CONDITION_HEADER = 'ExperiencedCondition'
VIDEO_HEADER = 'Could See Video'
REWARD_HEADER = 'Reward'
COMPLETE_HEADER = 'complete'
PROBLEM_COUNT_HEADER = 'ProblemCount'
SOURCE = '/disk2/contextualBanditSimsNoEffect/edm_rev_assist/ASSISTmentsData.csv'


# adapted from get_assistments_contextual_rewards
class AssistmentsData:
    def __init__(self, problem_set, reward_scheme, auto=False):
        """
        EXPECTS (in config):
            "source" - Location of the ASSISTments data, as csv
            "experimentId" - Which experiment to draw from
            "contextHeaders" - Headers of data to use as contextual variables
            ("qcutBehavior") - Dict of {header: num} where each header's values will be cut into num quantiles
            ("rewardScheme") - Uses COMPLETE_HEADER as reward if no value, or uses specific scheme e.g. "ProblemCount"

        :param config: dict with at least the above information
        :param auto: whether to automatically compute; default False; can call .retrieve_data() to compute
        """
        # items from config
        self.data_file = SOURCE
        self.problem_set = problem_set
        self.reward_scheme = reward_scheme
        self.qcut_behavior = {"Prior Percent Correct": 4}
        self.context_headers = ["Prior Percent Correct"]
        # items to be set in retrieveData
        self.df = None
        self.context_dict = None
        self.context_vectors = None
        # set values for the three above
        if auto:
            self.retrieve_data()

    def retrieve_data(self):
        # Read pandas dataframe from file
        df = pd.read_csv(self.data_file)
        df = df[df[EXPERIMENT_ID_HEADER] == int(self.problem_set)]

        # Qcut values of marked headers into n bins according to dict value (int)
        for header in self.qcut_behavior.keys():
            # Drop n/a fields
            df = df.dropna(subset=[header])
            # Quantile-cut marked fields
            df[header] = pd.qcut(df[header], self.qcut_behavior[header], labels=False)

        # Create dict of unique values of context fields
        self.context_dict = dict()
        for header in self.context_headers:
            # Drop n/a fields
            df = df.dropna(subset=[header])
            # Mark values in dict
            # TODO: no guarantee that these are 0-based indices; may not actually break anything
            self.context_dict[header] = sorted(df[header].unique())

        # Create list of all possible unique context vectors
        self.context_vectors = list(itertools.product(*[range(len(self.context_dict[header])) for header in
                                                        self.context_headers]))

        # Drop students that didn't experience condition
        df = df[df[EXPERIENCED_CONDITION_HEADER] == True]
        df = df[~((df[VIDEO_HEADER] == 0) & (df[CONDITION_HEADER] == 'E'))]

        # Generate reward column
        df = AssistmentsData.append_reward(df, self.reward_scheme)

        # Calculate descriptive statistics
        mean_stats = df.groupby(
            [CONDITION_HEADER, self.context_headers[0]]).Reward.mean()  # TODO: terrible; only suited for singlevar
        self.feature_vectors = list(
            range(len(self.context_dict[self.context_headers[0]])))  # TODO: terrible; only suited for singlevar
        self.arms_list = list(range(1, len(ARM_IDENTIFIERS) + 1))
        # calc means
        stats_dict = dict()
        for arm in self.arms_list:
            stats_dict[arm] = dict()
            for feature_vector in self.feature_vectors:
                stats_dict[arm][feature_vector] = mean_stats.values.item(
                    (arm - 1) * (len(self.feature_vectors)) + feature_vector)
        # calc opt actions
        correct_arm = []
        for feature_vector in self.feature_vectors:
            arm_probs = [stats_dict[arm][feature_vector] for arm in self.arms_list]
            correct_arm.append([i + 1 for i, x in enumerate(arm_probs) if x == max(arm_probs)])
        # save
        self.stats_dict = stats_dict
        self.correct_arm = correct_arm
        self.mean_stats = mean_stats

        # Save finalized df
        self.num_students = len(df)
        self.df = df

    def get_context_dict(self):
        if self.context_dict is None:
            self.retrieve_data()
        return self.context_dict

    def get_context_headers(self):
        return self.context_headers

    def get_context_vectors(self):
        if self.context_vectors is None:
            self.retrieve_data()
        return self.context_vectors

    def get_df(self):
        if self.df is None:
            self.retrieve_data()
        return self.df

    def get_arm_rewards(self):
        if self.stats_dict is None:
            self.retrieve_data()
        return tuple([[self.stats_dict[arm][feature_vector] for feature_vector in self.feature_vectors] for arm in
                      self.arms_list])

    def get_group_sizes(self):
        if self.num_students is None:
            self.retrieve_data()
        return [(len(self.df.loc[self.df[self.context_headers[0]] == self.feature_vectors[i]]),
                 len(self.df.loc[self.df[self.context_headers[0]] == self.feature_vectors[i]]) / self.num_students) for
                i in range(len(self.feature_vectors))]  # also only singlevar

    def __str__(self, show_means=True):
        desc = 'ASSISTments problem set {self.problem_set} with reward scheme "{self.reward_scheme}" ({self.num_students} students)'
        if show_means:
            desc += ':\n{self.mean_stats}'
        return desc.format(self=self)

    @staticmethod
    def append_reward(df, reward_scheme):
        if reward_scheme is not None and reward_scheme.lower() == 'problemcount':
            df = AssistmentsData.append_problemcount_reward(df)
        else:
            df = AssistmentsData.append_complete_reward(df)
        # reward should not be None
        return df

    @staticmethod
    def append_complete_reward(df):
        # assign directly from complete
        df[REWARD_HEADER] = df[COMPLETE_HEADER]
        return df

    @staticmethod
    def append_problemcount_reward(df):
        # first, assign those who did complete the assignment by relation to median
        completed = df[COMPLETE_HEADER] == 1
        # quantile into binary across median problems to complete
        df[REWARD_HEADER] = pd.qcut(df[completed][PROBLEM_COUNT_HEADER], 2, labels=False, duplicates='drop')
        # flip: those with median or lower problem count get 1 reward; higher than median get 0
        df[REWARD_HEADER] = df[REWARD_HEADER].replace({1: 0, 0: 1})

        # then, assign those who did not complete the assignment to 0 reward
        df[REWARD_HEADER] = df[REWARD_HEADER].fillna(0)

        return df


experiment_stats = dict()
df = None
for problem_set in PROBLEM_SET_LIST:
    experiment_stats[problem_set] = dict()
    for reward_scheme in REWARD_SCHEMES:
        assistments_data = AssistmentsData(problem_set, reward_scheme, auto=True)
        # partial_df = assistments_data.df
        # partial_df['Experiment'] = problem_set + "," + reward_scheme
        # temp_df = partial_df.copy()
        # temp_df['Prior Percent Correct'] = 4
        # partial_df = pd.concat((partial_df, temp_df))
        # df = partial_df if df is None else pd.concat((df, partial_df))
        print(assistments_data)
        # print(assistments_data.get_group_sizes())
        # print(assistments_data.correct_arm)
        # print()
        experiment_stats[problem_set][reward_scheme] = assistments_data


class SimulationResults:
    """ Fetches dataframes from pickles """
    pickle_location = '/disk2/contextualBanditSimsNoEffect/edm_rev_assist/'
    pickle_name_template = 'ASSISTmentsOutput_{reward_scheme}-{sim_type}-{problem_set}_{treatment}.pkl.gz'

    def __init__(self, problem_set_list, reward_scheme_list, sim_type_list, treatment_list):
        self.treatment_list = treatment_list
        # Initialize empty frame
        self.sim_result_dict = {
            ps: {rs: {st: {tr: None for tr in treatment_list} for st in sim_type_list} for rs in reward_scheme_list} for
            ps in problem_set_list}

    def get_sim(self, problem_set, reward_scheme, sim_type, treatment):
        if self.sim_result_dict[problem_set][reward_scheme][sim_type][treatment] is not None:
            return self.sim_result_dict[problem_set][reward_scheme][sim_type][treatment]
        else:
            df = SimulationResults.load_from_pickle(problem_set, reward_scheme, sim_type, treatment)
            self.sim_result_dict[problem_set][reward_scheme][sim_type][treatment] = df
            return df

    def get_treatments_of(self, problem_set, reward_scheme, sim_type):
        return [self.get_sim(problem_set, reward_scheme, sim_type, treatment) for treatment in self.treatment_list]

    def get_master(self, include_experiment=True):
        df = None
        for ps in self.sim_result_dict.keys():
            for rs in self.sim_result_dict[ps].keys():
                for st in self.sim_result_dict[ps][rs].keys():
                    for tr in self.sim_result_dict[ps][rs][st].keys():
                        partial_df = self.get_sim(ps, rs, st, tr)
                        partial_df['Problem Set'] = ps
                        partial_df['Reward Scheme'] = rs
                        partial_df['Sim Type'] = st
                        partial_df['Treatment'] = tr
                        if include_experiment:
                            partial_df['Experiment'] = ps + ',' + rs
                        if df is None:
                            df = partial_df
                        else:
                            df = pd.concat((df, partial_df))
        return df

    @staticmethod
    def save_to_pickle(df, problem_set, reward_scheme, sim_type, treatment):
        save_location = SimulationResults.pickle_location
        save_location += SimulationResults.pickle_name_template.format(reward_scheme=reward_scheme, sim_type=sim_type,
                                                                       problem_set=problem_set, treatment=treatment)
        df.to_pickle(save_location)
        print('Saved df at "' + save_location + '" successfully!')

    @staticmethod
    def load_from_pickle(problem_set, reward_scheme, sim_type, treatment):
        save_location = SimulationResults.pickle_location
        save_location += SimulationResults.pickle_name_template.format(reward_scheme=reward_scheme, sim_type=sim_type,
                                                                       problem_set=problem_set, treatment=treatment)
        return pd.read_pickle(save_location)

"""# Data Visualization"""

simulation_results = SimulationResults(PROBLEM_SET_LIST, REWARD_SCHEMES, SIM_TYPE_LIST, TREATMENTS)

df = None
for ps in experiment_stats.keys():
    for rs in experiment_stats[ps].keys():
        partial_df = experiment_stats[ps][rs].get_df()
        partial_df['Problem Set'] = ps
        partial_df['Reward Scheme'] = rs
        partial_df['Experiment'] = ps + ',' + rs
        if df is None:
            df = partial_df
        else:
            df = pd.concat((df, partial_df))

df['Condition'] = df['Condition'].replace({'C': 'Control', 'E': 'Experimental'})

df2 = df.copy()
df2['Prior Percent Correct'] = 4
df = pd.concat((df, df2))

df_filtered = df.groupby(["Experiment", "Prior Percent Correct", "Condition"]).mean()
present = df_filtered.drop(
    labels=["problem_set", "User ID", "ExperiencedCondition", "Could See Video", "Prior Problem Count",
            "Prior Correct Count", "Class ID", "Class Section ID", "Teacher ID", "school_id", "District ID", "State ID",
            "Prior Assignments Assigned", "Prior Assignment Count", "Prior Completion Count",
            "Prior Percent Completion", "Prior Class Percent Completion", "Z-Scored Mastery Speed",
            "Prior Homework Assigned", "Prior Homework Count", "Prior Homework Completion Count",
            "Prior Homework Percent Completion", "Prior Class Homework Percent Completion", "Z-Scored HW Mastery Speed",
            "complete", "ProblemCount", "log(count)"], axis=1)

present.to_csv('/disk2/contextualBanditSimsNoEffect/edm_rev_assist/' + 'RealWorld_Parameters.csv')

sns.set()
sns.set(context='paper', style="whitegrid", font='Times', font_scale=5)

# print(sns.color_palette("colorblind"))
palette = np.roll(np.array(sns.color_palette("colorblind")),
                  -2 * 3)  # Skip first two colors (*3 adjusts for hsl sublists)
# print(palette)

g = sns.catplot(y='Reward', x='Prior Percent Correct', hue='Condition', data=df,
                hue_order=['Control', 'Experimental'], kind='bar',
                height=12,
                aspect=1.125,
                ci=None,
                # cut=0,
                col='Experiment',
                col_order=['293151,complete', '293151,ProblemCount', '263057,complete', '263057,ProblemCount'],
                palette=palette)

for i, label in enumerate(
        ['Uneven Student Distribution,\nCompleted HW', 'Uneven Student Distribution,\nCompleted Quickly',
         'Even Student Distribution,\nCompleted HW', 'Even Student Distribution,\nCompleted Quickly']):
    g.axes[0, i].set_title(label)

for i, label in enumerate(['', 'Group of Prior Percent Correct', '', '']):
    g.axes[0, i].set_xlabel(label, weight='bold')
g.axes[0, 1].xaxis.set_label_coords(1.033, -0.1)

g.set_xticklabels(["Q1", "Q2", "Q3", "Q4", r"$\mathit{All}$"])

g.set_ylabels("Average Reward per Student", weight='bold').set(ylim=(0, 1)).despine(left=True)

g._legend.set_title("Condition")

plt.show()
g.savefig('/disk2/contextualBanditSimsNoEffect/edm_rev_assist/' + 'RealWorld_OriginalProbs.pdf')



def display_stats(assistments_data, df_contextual, df_noncontextual):
    arm_1_reward, arm_2_reward = assistments_data.get_arm_rewards()

    df_con = df_contextual.groupby(level=0).mean()
    df_con['Arm 1 % Picked'] = df_con['Arm 1 Times Picked'] / df_con['Times Occured'] * 100
    df_con['Arm 2 % Picked'] = df_con['Arm 2 Times Picked'] / df_con['Times Occured'] * 100
    df_con['Arm 1 Actual P(Reward)'] = arm_1_reward
    df_con['Arm 2 Actual P(Reward)'] = arm_2_reward

    df_ncon = df_noncontextual.groupby(level=0).mean()
    df_ncon['Arm 1 % Picked'] = df_ncon['Arm 1 Times Picked'] / df_ncon['Times Occured'] * 100
    df_ncon['Arm 2 % Picked'] = df_ncon['Arm 2 Times Picked'] / df_ncon['Times Occured'] * 100
    df_ncon['Arm 1 Actual P(Reward)'] = arm_1_reward
    df_ncon['Arm 2 Actual P(Reward)'] = arm_2_reward

    print("Summary of descriptive statistics for:", assistments_data)
    print()
    print("Contextual bandit")
    display(df_con.head())
    print()
    print("Non-contextual bandit")
    display(df_ncon.head())


for problem_set, reward_scheme, sim_type in itertools.product(PROBLEM_SET_LIST, REWARD_SCHEMES, SIM_TYPE_LIST):
    assistments_data = experiment_stats[problem_set][reward_scheme]
    df_contextual, df_noncontextual = simulation_results.get_treatments_of(problem_set, reward_scheme, sim_type)
    print('simType: ', sim_type)
    display_stats(assistments_data, df_contextual, df_noncontextual)
    print()
    print('---------------------------------------------------------------------------------------------------------')
    print()

df = simulation_results.get_master()
df = df.loc[df['Sim Type'] == 'Outcomes']
df = df.reset_index()

this_sim = [None, None, None, None]
df2 = df.loc[df['index'] == 0]
for i in range(len(df)):
    if i % 4 == 0 and i != 0:
        percent_corr = [this_sim[j]['Correct Assignment'] for j in range(4)]
        times_occured = [this_sim[j]['Times Occured'] for j in range(4)]
        num_corr = [percent * times for percent, times in zip(percent_corr, times_occured)]
        total_occured = sum(times_occured)
        weighted_percent = sum(num_corr) / total_occured
        df2.at[i - 4, 'Correct Assignment'] = weighted_percent
        df2.at[i - 4, 'Times Occured'] = total_occured
    this_sim[i % 4] = df.iloc[i]
df2['index'] = 4
# display(df2.head())
df = pd.concat((df, df2))

sns.set()
sns.set(context='paper', style="whitegrid", font='Times', font_scale=5)
g = sns.catplot(y='Correct Assignment', x='index', hue='Treatment', data=df,
                hue_order=['Non Contextual', 'Contextual'], kind='bar',
                height=12,
                aspect=1.125,
                # cut=0,
                ci=68,
                capsize=0.1,
                errwidth=3,
                col='Experiment',
                col_order=['293151,complete', '293151,ProblemCount', '263057,complete', '263057,ProblemCount'],
                palette="colorblind")

for i, label in enumerate(
        ['Uneven Student Distribution,\nCompleted HW', 'Uneven Student Distribution,\nCompleted Quickly',
         'Even Student Distribution,\nCompleted HW', 'Even Student Distribution,\nCompleted Quickly']):
    g.axes[0, i].set_title(label)

for i, label in enumerate(['', 'Group of Prior Percent Correct', '', '']):
    g.axes[0, i].set_xlabel(label, weight='bold')
g.axes[0, 1].xaxis.set_label_coords(1.033, -0.1)

g.set_xticklabels(["Q1", "Q2", "Q3", "Q4", r"$\mathit{All}$"])

g.set_ylabels("Proportion of Optimal Actions", weight='bold').set(ylim=(0, 1)).despine(left=True)

g._legend.set_title("Bandit Type")

plt.show()
g.savefig('/disk2/contextualBanditSimsNoEffect/edm_rev_assist/' + 'RealWorld_PropOptimal.pdf')

df = simulation_results.get_master()
df = df.loc[df['Sim Type'] == 'Outcomes']
df = df.reset_index()

this_sim = [None, None, None, None]
df2 = df.loc[df['index'] == 0]
for i in range(len(df)):
    if i % 4 == 0 and i != 0:
        percent_corr = [this_sim[j]['Correct Assignment'] for j in range(4)]
        times_occured = [this_sim[j]['Times Occured'] for j in range(4)]
        num_corr = [percent * times for percent, times in zip(percent_corr, times_occured)]
        total_occured = sum(times_occured)
        weighted_percent = sum(num_corr) / total_occured
        df2.at[i - 4, 'Correct Assignment'] = weighted_percent
        df2.at[i - 4, 'Times Occured'] = total_occured
    this_sim[i % 4] = df.iloc[i]
df2['index'] = 4
# display(df2.head())
df = pd.concat((df, df2))

df_noncon_means = df.loc[df['Treatment'] == 'Non Contextual'].groupby(['Experiment', 'index'])[
    'Correct Assignment'].mean()
df_con_means = df.loc[df['Treatment'] == 'Contextual'].groupby(['Experiment', 'index'])['Correct Assignment'].mean()

df_noncon_means.loc['293151,complete'].loc[0]

sns.set()
sns.set(context='paper', style="whitegrid", font='Times', font_scale=5)
g = sns.catplot(y='Correct Assignment', x='index', hue='Treatment', data=df,
                hue_order=['Non Contextual', 'Contextual'], kind='swarm',
                height=13,
                aspect=1,
                # cut=0,
                # linewidth=5,
                # showmeans=True,
                dodge=True,
                col='Experiment',
                col_order=['293151,complete', '293151,ProblemCount', '263057,complete', '263057,ProblemCount'],
                palette="colorblind")

for i, experiment in enumerate(['293151,complete', '293151,ProblemCount', '263057,complete', '263057,ProblemCount']):
    for j, index in enumerate([0, 1, 2, 3, 4]):
        # g.axes[0, i].add_patch(mpl.patches.Polygon([[j - .2, df_noncon_means.loc[experiment].loc[index] - .03], [j - .275, df_noncon_means.loc[experiment].loc[index]], [j - .2, df_noncon_means.loc[experiment].loc[index] + .03], [j - .125, df_noncon_means.loc[experiment].loc[index]]], closed=True, zorder=3, fc='white', ec=sns.color_palette('colorblind')[0], linewidth=5))
        # g.axes[0, i].add_patch(mpl.patches.Polygon([[j + .2, df_con_means.loc[experiment].loc[index] - .03], [j + .275, df_con_means.loc[experiment].loc[index]], [j + .2, df_con_means.loc[experiment].loc[index] + .03], [j + .125, df_con_means.loc[experiment].loc[index]]], closed=True, zorder=3, fc='white', ec=sns.color_palette('colorblind')[1], linewidth=5))
        # g.axes[0, i].add_patch(mpl.patches.Ellipse([j + .2, df_con_means.loc[experiment].loc[index]], .1, .025, zorder=3, fc='white', ec='black', linewidth=3))
        line_noncon = g.axes[0, i].hlines(df_noncon_means.loc[experiment].loc[index], j - .4, j + 0, linestyles='solid',
                                          zorder=3, label='Non Contextual', linewidth=10)
        line_con = g.axes[0, i].hlines(df_con_means.loc[experiment].loc[index], j + 0, j + .4, linestyles='solid',
                                       zorder=3, label='Contextual', linewidth=10)
        # line_mid = g.axes[0,i].vlines(j + 0, max(df_con_means.loc[experiment].loc[index], df_noncon_means.loc[experiment].loc[index]), 0, linestyles='solid', zorder=3, linewidth=10)
        line_right = g.axes[0, i].vlines(j + .385, df_con_means.loc[experiment].loc[index], 0, linestyles='solid',
                                         zorder=3, linewidth=5)
        line_left = g.axes[0, i].vlines(j - .385, df_noncon_means.loc[experiment].loc[index], 0, linestyles='solid',
                                        zorder=3, linewidth=5)
        line_rightmid = g.axes[0, i].vlines(j + .015, df_con_means.loc[experiment].loc[index], 0, linestyles='solid',
                                            zorder=3, linewidth=5)
        line_leftmid = g.axes[0, i].vlines(j - .015, df_noncon_means.loc[experiment].loc[index], 0, linestyles='solid',
                                           zorder=3, linewidth=5)

for i, label in enumerate(
        ['Uneven Student Distribution,\nCompleted HW', 'Uneven Student Distribution,\nCompleted Quickly',
         'Even Student Distribution,\nCompleted HW', 'Even Student Distribution,\nCompleted Quickly']):
    g.axes[0, i].set_title(label)
    # line = g.axes[0,i].hlines(0.9, -.4, 0, linestyles='dashed', color=sns.color_palette()[2], label='Non Contextual', linewidth=3)

for i, label in enumerate(['', 'Group of Prior Percent Correct', '', '']):
    g.axes[0, i].set_xlabel(label, weight='bold')
g.axes[0, 1].xaxis.set_label_coords(1.033, -0.1)

g.set_xticklabels(["Q1", "Q2", "Q3", "Q4", r"$\mathit{All}$"])

g.set_ylabels("Proportion of Optimal Actions", weight='bold').set(ylim=(0, 1)).despine(left=True)

g._legend.set_title("Bandit Type")

# g.fig.subplots_adjust(wspace=.04, hspace=.04)

plt.show()
g.savefig('/disk2/contextualBanditSimsNoEffect/edm_rev_assist/' + 'RealWorld_SwarmHybrid.pdf')

df = simulation_results.get_master()
df = df.loc[df['Sim Type'] == 'Outcomes']
df = df.reset_index()

this_sim = [None, None, None, None]
df2 = df.loc[df['index'] == 0]
for i in range(len(df)):
    if i % 4 == 0 and i != 0:
        arm_1_rew = [this_sim[j]['Arm 1 Total Reward'] for j in range(4)]
        arm_2_rew = [this_sim[j]['Arm 2 Total Reward'] for j in range(4)]
        times_occured = [this_sim[j]['Times Occured'] for j in range(4)]
        total_arm_1 = sum(arm_1_rew)
        total_arm_2 = sum(arm_2_rew)
        total_occured = sum(times_occured)
        df2.at[i - 4, 'Arm 1 Total Reward'] = total_arm_1
        df2.at[i - 4, 'Arm 2 Total Reward'] = total_arm_2
        df2.at[i - 4, 'Times Occured'] = total_occured
    this_sim[i % 4] = df.iloc[i]
df2['index'] = 4
df = pd.concat((df, df2))

df['Average Reward per Student'] = (df['Arm 1 Total Reward'] + df['Arm 2 Total Reward']) / df['Times Occured']
# display(df.loc[df['index'] == 4].head())

df_filtered = df.drop(labels=["Times Occured", "Correct Assignment", "Arm 1 Times Picked", "Arm 1 Total Reward",
                              "Arm 1 Expected P(Reward)", "Arm 2 Times Picked", "Arm 2 Total Reward",
                              "Arm 2 Expected P(Reward)"], axis=1)
present = df_filtered.groupby(["index", "Experiment", "Treatment"]).mean()
present.to_csv('/disk2/contextualBanditSimsNoEffect/edm_rev_assist/' + 'RealWorld_AvgReward.csv')

sns.set()
sns.set(context='paper', style="whitegrid", font='Times', font_scale=5)
g = sns.catplot(y='Average Reward per Student', x='index', hue='Treatment', data=df,
                hue_order=['Non Contextual', 'Contextual'], kind='bar',
                height=15,
                aspect=1,
                # cut=0,
                ci=68,
                capsize=0.1,
                errwidth=3,
                col='Experiment',
                col_order=['293151,complete', '293151,ProblemCount', '263057,complete', '263057,ProblemCount'],
                palette="colorblind")

for i, label in enumerate(
        ['Uneven Student Distribution,\nCompleted HW', 'Uneven Student Distribution,\nCompleted Quickly',
         'Even Student Distribution,\nCompleted HW', 'Even Student Distribution,\nCompleted Quickly']):
    g.axes[0, i].set_title(label)

for i, label in enumerate(['', 'Group of Prior Percent Correct', '', '']):
    g.axes[0, i].set_xlabel(label, weight='bold')
g.axes[0, 1].xaxis.set_label_coords(1.033, -0.1)

g.set_xticklabels(["Q1", "Q2", "Q3", "Q4", "All"])

g.set_ylabels("Average Reward per Student", weight='bold').set(ylim=(0, 1)).despine(left=True)

g._legend.set_title("Bandit Type")

plt.show()
g.savefig('/disk2/contextualBanditSimsNoEffect/edm_rev_assist/' + 'RealWorld_AvgReward.pdf')

for experiment in ['293151,complete', '263057,complete', '293151,ProblemCount', '263057,ProblemCount']:
    df = simulation_results.get_master()
    df = df.reset_index()
    df = df.loc[df['Sim Type'] == 'Outcomes']
    df = df.loc[df['Experiment'] == experiment]
    df = df.loc[df['Treatment'] == 'Non Contextual']
    df_groups = dict()
    for i in range(4):
        df_groups[i] = df.loc[df['index'] == i]
        df_groups[i]['Arm 1 Total Expected Reward'] = df_groups[i]['Times Occured'] * df_groups[i][
            'Arm 1 Expected P(Reward)']
        df_groups[i]['Arm 2 Total Expected Reward'] = df_groups[i]['Times Occured'] * df_groups[i][
            'Arm 2 Expected P(Reward)']
    arm_1_total = sum([df_groups[i]['Arm 1 Total Expected Reward'].mean() for i in
                       range(4)])  # / sum([df_groups[i]['Times Occured'].mean() for i in range(4)])
    arm_2_total = sum([df_groups[i]['Arm 2 Total Expected Reward'].mean() for i in
                       range(4)])  # / sum([df_groups[i]['Times Occured'].mean() for i in range(4)])
    print(experiment, ':', arm_1_total, arm_2_total)
