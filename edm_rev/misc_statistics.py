import os

import pandas as pd
from statsmodels.formula.api import ols

pd.options.mode.chained_assignment = None

# For section 4.2 Baseline
save_dir = '/disk2/contextualBanditSimsNoEffect/edm_stats/figs'

save_name = os.path.join(save_dir, 'AllEff250Correct.parquet.gzip')
df = pd.read_parquet(save_name)

df = df.loc[df['EffType'] == 'Baseline']
df = df.loc[(df['MinGrpSize'] == 50)]

# 1000 trials * 2 bandits * 7 contextual variables
assert len(df) == (1000 * 2 * 7)

model = ols("AvgCorrect ~ C(Type, Treatment(reference='ModContextual')) * NumConVars", df).fit()
print(model.summary())

index = 2
data = {'df': model.df_resid, 't': model.tvalues.iloc[index], 'b': model.params.iloc[index],
        'p': max(model.pvalues.iloc[index], 0.001),
        'ul': model.conf_int().iloc[index, 0], 'll': model.conf_int().iloc[index, 1]}
print('$t({df:.0f})={t:.3f}$, $p<{p:.3f}$, $b={b:.3f}$, 95\% CI = $[{ul:.3f}, {ll:.3f}]$'.format(**data))

# For section 5.2 Contextual Bandit Minority Group results
save_dir = '/disk2/contextualBanditSimsNoEffect/edm_stats/figs'

save_name = os.path.join(save_dir, 'AllEff250Correct.parquet.gzip')
df = pd.read_parquet(save_name)

df = df.loc[(df['Type'] == 'ModContextual')]
df = df.loc[(df['NumConVars'] == 1)]

# 1000 trials * 5 group sizes * 6 scenarios
assert len(df) == (1000 * 5 * 6)

df['MinGrpSize'] = df['MinGrpSize'] / 100

model = ols("Grp0AvgCorrect ~ MinGrpSize * EffType", df).fit()
print(model.summary())

# Interested in trend as MinGrpSize decreases, will add negative sign
index = 6
data = {'df': model.df_resid, 't': -model.tvalues.iloc[index], 'b': -model.params.iloc[index],
        'p': max(model.pvalues.iloc[index], 0.001),
        'ul': -model.conf_int().iloc[index, 1], 'll': -model.conf_int().iloc[index, 0]}
print('$t({df:.0f})={t:.3f}$, $p<{p:.3f}$, $b={b:.3f}$, 95\% CI = $[{ul:.3f}, {ll:.3f}]$'.format(**data))

# For section 5.2 Contextual Bandit Overall results
save_dir = '/disk2/contextualBanditSimsNoEffect/edm_stats/figs'

save_name = os.path.join(save_dir, 'AllEff250Correct.parquet.gzip')
df = pd.read_parquet(save_name)

df = df.loc[(df['Type'] == 'ModContextual')]
df = df.loc[(df['NumConVars'] == 1)]

# 1000 trials * 5 group sizes * 6 scenarios
assert len(df) == (1000 * 5 * 6)

df['MinGrpSize'] = df['MinGrpSize'] / 100

model = ols("AvgCorrect ~ MinGrpSize * EffType", df).fit()
print(model.summary())

# Interested in trend as MinGrpSize decreases, will add negative sign
index = 6
data = {'df': model.df_resid, 't': -model.tvalues.iloc[index], 'b': -model.params.iloc[index],
        'p': max(model.pvalues.iloc[index], 0.001),
        'ul': -model.conf_int().iloc[index, 1], 'll': -model.conf_int().iloc[index, 0]}
print('$t({df:.0f})={t:.3f}$, $p<{p:.3f}$, $b={b:.3f}$, 95\% CI = $[{ul:.3f}, {ll:.3f}]$'.format(**data))

# For section 5.2 Non Contextual Bandit Baseline
save_dir = '/disk2/contextualBanditSimsNoEffect/edm_stats/figs'

save_name = os.path.join(save_dir, 'AllEff250Correct.parquet.gzip')
df = pd.read_parquet(save_name)

df = df.loc[df['EffType'] == 'Baseline']
df = df.loc[(df['NumConVars'] == 1)]

# 1000 trials * 2 bandits * 5 group sizes
assert len(df) == (1000 * 2 * 5)

df['MinGrpSize'] = df['MinGrpSize'] / 100

model = ols("AvgCorrect ~ C(Type, Treatment(reference='NonContextual')) * MinGrpSize", df).fit()
print(model.summary())

index = 2
data = {'df': model.df_resid, 't': model.tvalues.iloc[index], 'b': model.params.iloc[index],
        'p': max(model.pvalues.iloc[index], 0.001),
        'ul': model.conf_int().iloc[index, 0], 'll': model.conf_int().iloc[index, 1]}
print('$t({df:.0f})={t:.3f}$, $p={p:.3f}$, $b={b:.3f}$, 95\% CI = $[{ul:.3f}, {ll:.3f}]$'.format(**data))

# For section 5.2 Non Contextual Bandit Universal Optimal Action
save_dir = '/disk2/contextualBanditSimsNoEffect/edm_stats/figs'

save_name = os.path.join(save_dir, 'AllEff250Correct.parquet.gzip')
df = pd.read_parquet(save_name)

df = df.loc[df['EffType'].isin(['Universal1', 'Universal2', 'Universal3', 'Universal4'])]
df = df.loc[(df['NumConVars'] == 1)]

# 1000 trials * 2 bandits * 5 group sizes * 4 scenarios
assert len(df) == (1000 * 2 * 5 * 4)

df['MinGrpSize'] = df['MinGrpSize'] / 100

model = ols("AvgCorrect ~ C(Type, Treatment(reference='NonContextual')) * MinGrpSize", df).fit()
print(model.summary())

index = 2
data = {'df': model.df_resid, 't': model.tvalues.iloc[index], 'b': model.params.iloc[index],
        'p': max(model.pvalues.iloc[index], 0.001),
        'ul': model.conf_int().iloc[index, 0], 'll': model.conf_int().iloc[index, 1]}
print('$t({df:.0f})={t:.3f}$, $p={p:.3f}$, $b={b:.3f}$, 95\% CI = $[{ul:.3f}, {ll:.3f}]$'.format(**data))
