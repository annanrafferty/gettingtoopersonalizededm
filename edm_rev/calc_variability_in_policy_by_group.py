import sys
import pandas as pd
import glob as glob
import numpy as np
import scipy.special as scisp
from scipy.special import expit
import os
import time

import math
import random
import itertools
from multiprocessing import Pool
from functools import partial


def getArmChoiceGivenContext(context, means, stdevs,num_samples):
    ''' Returns proportion of choices that are for the second arm '''
    all_samples = []
    for i in range(len(means)):
        cur_samples = np.array([sampleOneArm(context, means[i],stdevs[i], 1) for _ in range(num_samples)])
        
#         print("cur_samples\n",cur_samples)

        all_samples.append(cur_samples)
    samples_array = np.array(all_samples).T
#     print("samples_array\n",samples_array)
    proportionArm2 =  sum(np.argmax(samples_array, axis=1)) / num_samples 
#     print("proportionArm2: ",proportionArm2)
    return proportionArm2

def sampleOneArm(x, mean, std, num_samples):# x is context
    if num_samples <= 1:
        # draw samples from approximate posterior distribution
         w = np.random.normal(mean, std)

         # compute expected value
         u = np.dot(w, x)
    
         return expit(u) #- expit(-u)
         # return expit(u)

    else:
        # generate many samples from same distribution
        mean_tile = np.tile(mean, [num_samples, 1])
        std_tile  = np.tile(std , [num_samples, 1])
        W = np.random.normal(mean_tile, std_tile)
        U = np.dot(W, x)

        return expit(U) - expit(-U)
        # return expit(U)
        


def get_needed_col_names(numActions):
    varianceSuffix = "EstimatedVariance"
    meanSuffix = "EstimatedMu"
    actionPrefix = "Action"
    col_names = ['Trial', 'n']
    for actionNum in range(numActions):
        actionLabel = actionPrefix + str(actionNum + 1)
        curMeanHeader =  actionLabel + meanSuffix
        curVarianceHeader =  actionLabel + varianceSuffix
        col_names.append(curMeanHeader)
        col_names.append(curVarianceHeader)
    return col_names

def processSingleTrial(row, idx=None, num_variables = 1, num_samples=10, index_for_groups=0):
    varianceSuffix = "EstimatedVariance"
    meanSuffix = "EstimatedMu"
    actionPrefix = "Action"
    numActions = 2
    means = []
    stdevs = []
    for actionNum in range(numActions):
        actionLabel = actionPrefix + str(actionNum + 1)
        curMeanHeader =  actionLabel + meanSuffix
        curVarianceHeader =  actionLabel + varianceSuffix
        meanKey = curMeanHeader
        varKey = curVarianceHeader
        if idx is not None:
            meanKey = idx[curMeanHeader]
            varKey = idx[curVarianceHeader]
        means.append(np.array([float(item) for item in row[meanKey].split(",")]))
#         print("means:",means)
        var = np.array([float(item) for item in row[varKey].split(",")])
#         print("var:", var)
        stdevs.append(np.sqrt(var))
#         print("stdevs:", stdevs)


    if (num_variables*2+1) != len(means[0]):
        print("Error!: ", num_variables, "variables was the file but the means length is", len(means[0]))
        return None
    # now repeatedly sample with different values of the contextual variables
    # make all possible combinations of binary strings with the right number of variables
    var_values = list(itertools.product([0, 1], repeat=num_variables))
    all_probs = []
    probs_by_group = {0 : [], 1 : []}
    for var_value in var_values:
        context = [1] + list(itertools.chain.from_iterable((x, 1-x) for x in var_value))
#             print("context",context)
        proportionArm2 = getArmChoiceGivenContext(context, means, stdevs, num_samples)
        all_probs.append(proportionArm2)
        probs_by_group[var_value[index_for_groups]].append(proportionArm2)
#         print("probs_by_group", probs_by_group)
    results = {}
    for i in range(2):
        results["Largest Difference " + str(i)] = max(probs_by_group[i]) - min(probs_by_group[i])
        results["Stdev " + str(i)] = np.std(probs_by_group[i])
    results["Largest Difference All"] = max(all_probs) - min(all_probs)
    results["Stdev All"] = np.std(all_probs)
    results['Trial'] = row['Trial'] if idx is None else row[idx['Trial']]
    return results

def processDfMultiThreaded(df, num_students, num_samples, index_for_groups, num_variables, num_cores = 20, max_trials = 20):
    # get the correct row and means and variances for that row and both arms

    trials = df[df['n'] == num_students]
    all_results = []
    idx = {name: i for i, name in enumerate(list(trials), start=1)}
    all_trials = []
#     for row in itertools.islice(trials.itertuples(), 0, max_trials):
#         all_trials.append(dict(row))
    all_trials = trials.to_dict('records')
    all_trials = all_trials[:max_trials]
    with Pool(num_cores) as pool:
        df_list = pool.map(partial(processSingleTrial, idx=None, num_variables=num_variables, 
                                   num_samples=num_samples, index_for_groups=index_for_groups), 
                           all_trials)

    
#     for row in trials.itertuples():    
#         results = processSingleTrial(row, idx)
#         all_results.append(results)
#         if results['Trial'] >= maxTrials:
#             break;
        
    df = pd.DataFrame.from_records(df_list)
    return df




def calc_df_dir(directory, num_students = 249, num_samples = 10, num_cores = 20, max_trials = 20, search_str = 'ModContextual'):
    '''Combine contextual and noncontextual dataframes, calculating the value
    (indicated by calc_func) in the process'''

    print(f"========== Running {directory} ==========")
    start = time.time()

    num_con_vars, min_grp_size, eff_type = os.path.basename(directory).split('_', 2)
    
    dir_df_list = []
    col_names = get_needed_col_names(2)
    f = glob.glob(os.path.join(directory, '{}*.parquet.gzip'.format(search_str)))

    df = pd.read_parquet(f[0], columns=col_names)
    gb = df.groupby('Trial')
    df_list = [grp for _, grp in gb]

#         if num_students is None:
#             df_list = [calc_func(df) for df in df_list]
#         else:
#             df_list = [calc_func(df[:num_students]) for df in df_list]

    df = pd.concat(df_list)




    index_for_groups = 0
    results_df = processDfMultiThreaded(df, num_students, num_samples, index_for_groups, int(num_con_vars[1:]), num_cores, max_trials)    
    results_df['NumConVars'] = int(num_con_vars[1:])
    results_df['MinGrpSize'] = int(min_grp_size[1:])
    results_df['EffType'] = eff_type
    results_df["Type"] = search_str

    results_df = results_df.astype({'NumConVars': 'int32', 'MinGrpSize': 'int32', 'Type': 'category', 'EffType': 'category'})

    end = time.time()
    print(f'Duration: {round((end - start) / 60, 2)} minutes')

    return results_df

def main(parent_dir, save_name, intermediate_save_dir = None):
    num_students = 249
    num_samples = 500
    index_for_groups = 0
    num_cores = 30
    directories = []
    max_trials = 1000
    for child in next(os.walk(parent_dir))[1]:
        directories.append(os.path.join(parent_dir, child))

    directories_to_include  = []
    df_list = []
    for directory in directories:
        num_con_vars, min_grp_size, eff_type = os.path.basename(directory).split('_', 2)

        # restrict simulations
        if num_con_vars in ['C2', 'C10'] and \
           min_grp_size in ['G50'] and \
           eff_type in ['Baseline', 'Personalized', 'Universal1', 'Universal2', 'Universal3', 'Universal4']:
            directories_to_include.append(directory)
            results_df = calc_df_dir(directory, num_students = num_students, num_samples = num_samples, 
                                     num_cores = num_cores, max_trials = max_trials)
            if intermediate_save_dir is not None:
                intermediate_save_name = os.path.join(intermediate_save_dir, os.path.basename(directory) + "PolicyVarIntermediate.parquet.gzip")
                results_df.to_parquet(intermediate_save_name, compression='gzip')
            df_list.append(results_df)
    final_df = pd.concat(df_list)

    final_df.to_parquet(save_name, compression='gzip')


if __name__ == "__main__":
    parent_dir = '/disk2/contextualBanditSimsNoEffect/edm_rev'
    save_dir = '/disk2/contextualBanditSimsNoEffect/edm_stats/figs'
    os.makedirs(save_dir, exist_ok=True)
    save_name = os.path.join(save_dir, 'AllVariabilityInPolicies.parquet.gzip')

    main(parent_dir, save_name, intermediate_save_dir=save_dir)