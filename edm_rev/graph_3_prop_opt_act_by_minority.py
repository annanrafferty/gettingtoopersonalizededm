import os

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

pd.options.mode.chained_assignment = None
import string

save_dir = '/disk2/contextualBanditSimsNoEffect/edm_stats/figs'

save_name = os.path.join(save_dir, 'AllEff250Correct.parquet.gzip')
df = pd.read_parquet(save_name)

df = df.loc[df['NumConVars'] == 1]

bandit_ord = ['NonContextual', 'ModContextual']
eff_ord = ['Baseline', 'Universal1', 'Personalized']

scale = 1.25

sns.set()
sns.set(context='paper', style="whitegrid", font='Times', font_scale=5 * scale, palette="colorblind",
        rc={"lines.linewidth": 10 * scale, 'lines.markersize': 30 * scale})
g = sns.relplot(data=df, x="MinGrpSize", y="Grp0AvgCorrect", hue='EffType', hue_order=eff_ord, style='EffType',
                style_order=eff_ord,
                kind="line", ci=68, markers=True, sizes=20 * scale, height=15, aspect=1, col='Type',
                col_order=bandit_ord)

handles, _ = g.axes[0][0].get_legend_handles_labels()

g._legend.remove()

g.fig.legend(handles=handles,
             labels=['Model:', 'Baseline', 'Universal Optimal Action (1)', 'Personalized Optimal Action'],
             bbox_to_anchor=(0.1, 0), loc='lower left', ncol=2, borderaxespad=0., frameon=False)

for i, label in enumerate(['Non Contextual', 'Contextual']):
    g.axes[0, i].set_title(label)

for i, label in enumerate(['Percentage of Minority Group', '']):
    g.axes[0, i].set_xlabel(label, weight='bold')

for i in range(len(g.axes[0])):
    g.axes[0, i].text(0, 1, string.ascii_lowercase[i] + ')', transform=g.axes[0, i].transAxes,
                      weight='bold', va='bottom', ha='left')

g.axes[0, 0].xaxis.set_label_coords(1.05, -0.1)

g.set_ylabels("Proportion of Optimal Actions\nfor Minority Group", weight='bold') \
    .set(ylim=(0, 1)) \
    .set_xticklabels([0, 10, 20, 30, 40, 50]) \
    .despine(left=True)

g.fig.subplots_adjust(bottom=.3, top=1)

plt.show()

g.savefig(os.path.join(save_dir, 'MinGrpSize.pdf'))
