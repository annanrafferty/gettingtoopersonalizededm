# Overhead

# mpl.use('Agg')
import seaborn as sns

sns.set(context='paper', style="whitegrid")
import pandas as pd

pd.options.mode.chained_assignment = None

from functools import partial
import time
# mpl.use('Agg')
import seaborn as sns
sns.set(context='paper', style="whitegrid")
import numpy as np
import pandas as pd

import glob as glob
import os
import sys
import itertools
from scipy.special import expit
from IPython.display import display
pd.options.mode.chained_assignment = None
import concurrent

# seaborn swarmplot prints a lot of warnings, hinders readability
import warnings
warnings.filterwarnings("ignore")

# Notebook constants

PROBLEM_SET_LIST = ["293151", "263057"]
REWARD_SCHEMES = ["complete", "ProblemCount"]
SIM_TYPE_LIST = ['Outcomes', 'Parameters']
TREATMENTS = ['Contextual', 'Non Contextual']

EXPERIMENT_ID_HEADER = 'problem_set'
CONDITION_HEADER = 'Condition'
ARM_IDENTIFIERS = ['C', 'E']
EXPERIENCED_CONDITION_HEADER = 'ExperiencedCondition'
VIDEO_HEADER = 'Could See Video'
REWARD_HEADER = 'Reward'
COMPLETE_HEADER = 'complete'
PROBLEM_COUNT_HEADER = 'ProblemCount'
SOURCE = '/disk2/contextualBanditSimsNoEffect/edm_rev_assist/ASSISTmentsData.csv'


# adapted from get_assistments_contextual_rewards
class AssistmentsData:
    def __init__(self, problem_set, reward_scheme, auto=False):
        """
        EXPECTS (in config):
            "source" - Location of the ASSISTments data, as csv
            "experimentId" - Which experiment to draw from
            "contextHeaders" - Headers of data to use as contextual variables
            ("qcutBehavior") - Dict of {header: num} where each header's values will be cut into num quantiles
            ("rewardScheme") - Uses COMPLETE_HEADER as reward if no value, or uses specific scheme e.g. "ProblemCount"

        :param config: dict with at least the above information
        :param auto: whether to automatically compute; default False; can call .retrieve_data() to compute
        """
        # items from config
        self.data_file = SOURCE
        self.problem_set = problem_set
        self.reward_scheme = reward_scheme
        self.qcut_behavior = {"Prior Percent Correct": 4}
        self.context_headers = ["Prior Percent Correct"]
        # items to be set in retrieveData
        self.df = None
        self.context_dict = None
        self.context_vectors = None
        # set values for the three above
        if auto:
            self.retrieve_data()

    def retrieve_data(self):
        # Read pandas dataframe from file
        df = pd.read_csv(self.data_file)
        df = df[df[EXPERIMENT_ID_HEADER] == int(self.problem_set)]

        # Qcut values of marked headers into n bins according to dict value (int)
        for header in self.qcut_behavior.keys():
            # Drop n/a fields
            df = df.dropna(subset=[header])
            # Quantile-cut marked fields
            df[header] = pd.qcut(df[header], self.qcut_behavior[header], labels=False)

        # Create dict of unique values of context fields
        self.context_dict = dict()
        for header in self.context_headers:
            # Drop n/a fields
            df = df.dropna(subset=[header])
            # Mark values in dict
            # TODO: no guarantee that these are 0-based indices; may not actually break anything
            self.context_dict[header] = sorted(df[header].unique())

        # Create list of all possible unique context vectors
        self.context_vectors = list(itertools.product(*[range(len(self.context_dict[header])) for header in
                                                        self.context_headers]))

        # Drop students that didn't experience condition
        df = df[df[EXPERIENCED_CONDITION_HEADER] == True]
        df = df[~((df[VIDEO_HEADER] == 0) & (df[CONDITION_HEADER] == 'E'))]

        # Generate reward column
        df = AssistmentsData.append_reward(df, self.reward_scheme)

        # Calculate descriptive statistics
        mean_stats = df.groupby(
            [CONDITION_HEADER, self.context_headers[0]]).Reward.mean()  # TODO: terrible; only suited for singlevar
        self.feature_vectors = list(
            range(len(self.context_dict[self.context_headers[0]])))  # TODO: terrible; only suited for singlevar
        self.arms_list = list(range(1, len(ARM_IDENTIFIERS) + 1))
        # calc means
        stats_dict = dict()
        for arm in self.arms_list:
            stats_dict[arm] = dict()
            for feature_vector in self.feature_vectors:
                stats_dict[arm][feature_vector] = mean_stats.values.item(
                    (arm - 1) * (len(self.feature_vectors)) + feature_vector)
        # calc opt actions
        correct_arm = []
        for feature_vector in self.feature_vectors:
            arm_probs = [stats_dict[arm][feature_vector] for arm in self.arms_list]
            correct_arm.append([i + 1 for i, x in enumerate(arm_probs) if x == max(arm_probs)])
        # save
        self.stats_dict = stats_dict
        self.correct_arm = correct_arm
        self.mean_stats = mean_stats

        # Save finalized df
        self.num_students = len(df)
        self.df = df

    def get_context_dict(self):
        if self.context_dict is None:
            self.retrieve_data()
        return self.context_dict

    def get_context_headers(self):
        return self.context_headers

    def get_context_vectors(self):
        if self.context_vectors is None:
            self.retrieve_data()
        return self.context_vectors

    def get_df(self):
        if self.df is None:
            self.retrieve_data()
        return self.df

    def get_arm_rewards(self):
        if self.stats_dict is None:
            self.retrieve_data()
        return tuple([[self.stats_dict[arm][feature_vector] for feature_vector in self.feature_vectors] for arm in
                      self.arms_list])

    def get_group_sizes(self):
        if self.num_students is None:
            self.retrieve_data()
        return [(len(self.df.loc[self.df[self.context_headers[0]] == self.feature_vectors[i]]),
                 len(self.df.loc[self.df[self.context_headers[0]] == self.feature_vectors[i]]) / self.num_students) for
                i in range(len(self.feature_vectors))]  # also only singlevar

    def __str__(self, show_means=True):
        desc = 'ASSISTments problem set {self.problem_set} with reward scheme "{self.reward_scheme}" ({self.num_students} students)'
        if show_means:
            desc += ':\n{self.mean_stats}'
        return desc.format(self=self)

    @staticmethod
    def append_reward(df, reward_scheme):
        if reward_scheme is not None and reward_scheme.lower() == 'problemcount':
            df = AssistmentsData.append_problemcount_reward(df)
        else:
            df = AssistmentsData.append_complete_reward(df)
        # reward should not be None
        return df

    @staticmethod
    def append_complete_reward(df):
        # assign directly from complete
        df[REWARD_HEADER] = df[COMPLETE_HEADER]
        return df

    @staticmethod
    def append_problemcount_reward(df):
        # first, assign those who did complete the assignment by relation to median
        completed = df[COMPLETE_HEADER] == 1
        # quantile into binary across median problems to complete
        df[REWARD_HEADER] = pd.qcut(df[completed][PROBLEM_COUNT_HEADER], 2, labels=False, duplicates='drop')
        # flip: those with median or lower problem count get 1 reward; higher than median get 0
        df[REWARD_HEADER] = df[REWARD_HEADER].replace({1: 0, 0: 1})

        # then, assign those who did not complete the assignment to 0 reward
        df[REWARD_HEADER] = df[REWARD_HEADER].fillna(0)

        return df


experiment_stats = dict()
df = None
for problem_set in PROBLEM_SET_LIST:
    experiment_stats[problem_set] = dict()
    for reward_scheme in REWARD_SCHEMES:
        assistments_data = AssistmentsData(problem_set, reward_scheme, auto=True)
        # partial_df = assistments_data.df
        # partial_df['Experiment'] = problem_set + "," + reward_scheme
        # temp_df = partial_df.copy()
        # temp_df['Prior Percent Correct'] = 4
        # partial_df = pd.concat((partial_df, temp_df))
        # df = partial_df if df is None else pd.concat((df, partial_df))
        print(assistments_data)
        # print(assistments_data.get_group_sizes())
        # print(assistments_data.correct_arm)
        # print()
        experiment_stats[problem_set][reward_scheme] = assistments_data


class SimulationResults:
    """ Fetches dataframes from pickles """
    pickle_location = '/disk2/contextualBanditSimsNoEffect/edm_rev_assist/'
    pickle_name_template = 'ASSISTmentsOutput_{reward_scheme}-{sim_type}-{problem_set}_{treatment}.pkl.gz'

    def __init__(self, problem_set_list, reward_scheme_list, sim_type_list, treatment_list):
        self.treatment_list = treatment_list
        # Initialize empty frame
        self.sim_result_dict = {
            ps: {rs: {st: {tr: None for tr in treatment_list} for st in sim_type_list} for rs in reward_scheme_list} for
            ps in problem_set_list}

    def get_sim(self, problem_set, reward_scheme, sim_type, treatment):
        if self.sim_result_dict[problem_set][reward_scheme][sim_type][treatment] is not None:
            return self.sim_result_dict[problem_set][reward_scheme][sim_type][treatment]
        else:
            df = SimulationResults.load_from_pickle(problem_set, reward_scheme, sim_type, treatment)
            self.sim_result_dict[problem_set][reward_scheme][sim_type][treatment] = df
            return df

    def get_treatments_of(self, problem_set, reward_scheme, sim_type):
        return [self.get_sim(problem_set, reward_scheme, sim_type, treatment) for treatment in self.treatment_list]

    def get_master(self, include_experiment=True):
        df = None
        for ps in self.sim_result_dict.keys():
            for rs in self.sim_result_dict[ps].keys():
                for st in self.sim_result_dict[ps][rs].keys():
                    for tr in self.sim_result_dict[ps][rs][st].keys():
                        partial_df = self.get_sim(ps, rs, st, tr)
                        partial_df['Problem Set'] = ps
                        partial_df['Reward Scheme'] = rs
                        partial_df['Sim Type'] = st
                        partial_df['Treatment'] = tr
                        if include_experiment:
                            partial_df['Experiment'] = ps + ',' + rs
                        if df is None:
                            df = partial_df
                        else:
                            df = pd.concat((df, partial_df))
        return df

    @staticmethod
    def save_to_pickle(df, problem_set, reward_scheme, sim_type, treatment):
        save_location = SimulationResults.pickle_location
        save_location += SimulationResults.pickle_name_template.format(reward_scheme=reward_scheme, sim_type=sim_type,
                                                                       problem_set=problem_set, treatment=treatment)
        df.to_pickle(save_location)
        print('Saved df at "' + save_location + '" successfully!')

    @staticmethod
    def load_from_pickle(problem_set, reward_scheme, sim_type, treatment):
        save_location = SimulationResults.pickle_location
        save_location += SimulationResults.pickle_name_template.format(reward_scheme=reward_scheme, sim_type=sim_type,
                                                                       problem_set=problem_set, treatment=treatment)
        return pd.read_pickle(save_location)


"""# Pickle Statistics"""


def calc_df(directory, calc_func, assistments_data):
    '''Combine contextual and noncontextual dataframes, calculating the value
    (indicated by calc_func) in the process'''
    sample_size = sys.maxsize

    df_contextual_list = [pd.read_csv(f, header=1) for i, f in
                          enumerate(glob.glob(os.path.join(directory, 'CompareBanditActionsOut_ModContextual*.csv'))) if
                          i < sample_size]
    df_noncontextual_list = [pd.read_csv(f, header=1) for i, f in
                             enumerate(glob.glob(os.path.join(directory, 'CompareBanditActionsOut_NonContextual*.csv')))
                             if i < sample_size]

    if len(df_contextual_list) == 0 or len(df_noncontextual_list) == 0:
        return None
    else:
        print(directory)

    df_contextual_list = [calc_func(df, True, assistments_data) for df in df_contextual_list]
    df_contextual = pd.concat(df_contextual_list)

    df_noncontextual_list = [calc_func(df, False, assistments_data) for df in df_noncontextual_list]
    df_noncontextual = pd.concat(df_noncontextual_list)

    return df_contextual, df_noncontextual


def read_list_of_trial_dfs_from_one_gzip(gzip_path):
    # from Zhaobin's "parquet demo.ipynb"
    df = pd.read_parquet(gzip_path)
    return [grp for _, grp in df.groupby('Trial')]


def calc_df_from_gzip(directory, calc_func, assistments_data):
    '''Combine contextual and noncontextual dataframes, calculating the value
    (indicated by calc_func) in the process'''
    sample_size = sys.maxsize

    bandit_type_to_gzip_name = {
        'contextual': None,
        'noncontextual': None
    }

    for name in os.listdir(directory):
        if '.parquet.gzip' in name:
            if 'ModContextual' in name:
                bandit_type_to_gzip_name['contextual'] = name
            elif 'NonContextual' in name:
                bandit_type_to_gzip_name['noncontextual'] = name

    print(f"[From calc_df_from_gzip] Loading CSVs from {directory} ...")

    start_time = time.perf_counter()

    df_contextual_list = read_list_of_trial_dfs_from_one_gzip(
        os.path.join(directory, bandit_type_to_gzip_name['contextual']))

    df_noncontextual_list = read_list_of_trial_dfs_from_one_gzip(
        os.path.join(directory, bandit_type_to_gzip_name['noncontextual']))

    stop_time = time.perf_counter()

    print(
        f"[From calc_df_from_gzip] Loaded {len(df_contextual_list)} contextual CSVs and {len(df_noncontextual_list)} non-contextual CSVs in {round(stop_time - start_time, 2)} seconds.")

    print(f"[From calc_df_from_gzip] Applying calc_func to each df with parallelism ...")

    start_time = time.perf_counter()

    with concurrent.futures.ProcessPoolExecutor(max_workers=5) as executor:
        df_contextual_list = executor.map(partial(calc_func, contextual=True, assistments_data=assistments_data),
                                          df_contextual_list)
    df_contextual = pd.concat(df_contextual_list)

    with concurrent.futures.ProcessPoolExecutor(max_workers=5) as executor:
        df_noncontextual_list = executor.map(partial(calc_func, contextual=False, assistments_data=assistments_data),
                                             df_noncontextual_list)
    df_noncontextual = pd.concat(df_noncontextual_list)

    stop_time = time.perf_counter()

    print(f"[From calc_df_from_gzip] Applied calc_func to each df in {round(stop_time - start_time, 2)} seconds.")

    return df_contextual, df_noncontextual


def calc_summary(df, contextual, assistments_data):
    num_students = assistments_data.num_students
    feature_vectors = assistments_data.feature_vectors
    arms_list = assistments_data.arms_list
    correct_arm = assistments_data.correct_arm

    columns = ['Correct Assignment', 'Times Occured']
    for arm in arms_list:
        columns.append('Arm {} Times Picked'.format(arm))
        columns.append('Arm {} Total Reward'.format(arm))
        columns.append('Arm {} Expected P(Reward)'.format(arm))
    df_table = pd.DataFrame(index=feature_vectors, columns=columns, dtype=np.float32)
    df_table = df_table.fillna(0)

    if num_students is not None:
        df = df[:num_students]

    for i, feature_vector in enumerate(feature_vectors):
        df_feature_vector = df[df['contextualVariable0'] == feature_vector]
        df_table.at[feature_vector, 'Times Occured'] = len(df_feature_vector)

        # get reward statistics for the feature
        for arm in arms_list:
            df_feature_vector_arm = df_feature_vector[df_feature_vector['AlgorithmAction'] == arm]
            df_table.at[feature_vector, 'Arm {} Times Picked'.format(arm)] = len(df_feature_vector_arm)
            df_table.at[feature_vector, 'Arm {} Total Reward'.format(arm)] = sum(
                df_feature_vector_arm['ObservedRewardofAction'])

            if contextual:
                weights = [float(weight) for weight in df.iloc[-1]['Action{}EstimatedMu'.format(arm)].split(',')]
                df_table.at[feature_vector, 'Arm {} Expected P(Reward)'.format(arm)] = expit(
                    weights[0] + weights[i + 1])
            else:
                df_table.at[feature_vector, 'Arm {} Expected P(Reward)'.format(arm)] = df.iloc[-1][
                    'Action{}EstimatedProb'.format(arm)]

        df_table.at[feature_vector, 'Correct Assignment'] = sum(
            [df_table.at[feature_vector, 'Arm {} Times Picked'.format(correct_arm_option)] for correct_arm_option in
             correct_arm[i]]) / df_table.at[feature_vector, 'Times Occured']

    return df_table


DIRECTORY_TEMPLATE = '/disk2/contextualBanditSimsNoEffect/edm_rev_assist/{reward_scheme}/{sim_type}/ASSISTments_{problem_set}_C1_N1k/'
SIM_TYPE_LIST = ['Outcomes', 'Parameters']

for problem_set, reward_scheme, sim_type in itertools.product(PROBLEM_SET_LIST, REWARD_SCHEMES, SIM_TYPE_LIST):
    print(problem_set, reward_scheme, sim_type)
    directory = DIRECTORY_TEMPLATE.format(reward_scheme=reward_scheme, sim_type=sim_type, problem_set=problem_set)
    df_contextual, df_noncontextual = calc_df_from_gzip(directory, calc_summary,
                                                        experiment_stats[problem_set][reward_scheme])
    SimulationResults.save_to_pickle(df_contextual, problem_set, reward_scheme, sim_type, 'Contextual')
    SimulationResults.save_to_pickle(df_noncontextual, problem_set, reward_scheme, sim_type, 'Non Contextual')
